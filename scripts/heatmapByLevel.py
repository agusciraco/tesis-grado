import plotly.graph_objects as go
import plotly.figure_factory as ff

import numpy as np
from scipy.spatial.distance import pdist, squareform
import sys

# get data
dataFile = sys.argv[1]
print('getting data from' + dataFile)
colorScale = 'default'
if(len(sys.argv)> 2):
	colorScale = sys.argv[2]
# get data
# ~ data = np.genfromtxt("Resultados-1era-corrida-debugged - cantNodesMetricForPython.csv",
                     # ~ names=True,dtype=float, delimiter=",")
data = np.genfromtxt(dataFile, names=True, dtype=float, delimiter=",")
labels = data.dtype.names



# Create Heatmap
if(colorScale!='default'):
	heatmap = go.Heatmap(
                   z=data,
                   x=labels,
                   y=labels,
                   colorscale=colorScale,
                   hoverongaps = False
                   )
else:
	heatmap = go.Heatmap(
                   z=data,
                   x=labels,
                   # y=labels,
                   colorbar=dict(title='distanciaDeTries'),
                   y=['Nivel 1','Nivel 2','Nivel 3','Nivel 4','Nivel 5','Nivel 6','Nivel 7','Nivel 8','Nivel 9','Nivel 10','Nivel 11','Nivel 12','Nivel 13','Nivel 14'],
                   # y=['Nivel 1','Nivel 2','Nivel 3','Nivel 4','Nivel 5','Nivel 6','Nivel 7','Nivel 8','Nivel 9','Nivel 10','Nivel 11'],
                   hoverongaps = False
                   )
# ~ layout = go.Layout(
            # ~ hovermode='closest',
            # ~ autosize=True,
            # ~ xaxis=dict(zeroline=False),
            # ~ yaxis=dict(zeroline=False, autorange='reversed', scaleanchor="x", scaleratio=1)
         # ~ )
# Create layout
layout = go.Layout(width=1500, height=500,
# layout = go.Layout(width=1500, height=1500,
            hovermode='closest',
            autosize=True,
            xaxis=dict(zeroline=False),
            yaxis=dict(zeroline=False, autorange='reversed'),
         )
# Create figure and show
fig = go.Figure(data=heatmap, layout=layout)
fig.show()

