import plotly.graph_objects as go
import plotly.figure_factory as ff

import numpy as np
from scipy.spatial.distance import pdist, squareform
import sys

# get data
dataFile = sys.argv[1]
print('getting data from' + dataFile)
colorScale = 'default'
if(len(sys.argv)> 2):
	colorScale = sys.argv[2]
	
data = np.genfromtxt(dataFile, names=True, dtype=float, delimiter=",")
data_array = data.view((np.float, len(data.dtype.names)))
labels = data.dtype.names

# Initialize figure by creating upper dendrogram
# fig = ff.create_dendrogram(data_array, orientation='bottom', labels=labels, color_threshold=0.2)
fig = ff.create_dendrogram(data_array, orientation='bottom', labels=labels)

# Edit Layout
fig.update_layout({'width':800, 'height':400,
                         'showlegend':False, 'hovermode': 'closest',
                         })
fig.update_xaxes(title_text='Familias')
fig.update_yaxes(title_text='Distancia entre clusters')
# Plot!
fig.show()
