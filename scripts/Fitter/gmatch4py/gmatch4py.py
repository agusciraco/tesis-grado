# Gmatch4py use networkx graph
import networkx as nx
# import the GED using the munkres algorithm
import gmatch4py as gm

G1a = nx.read_edgelist('testGlobinGraph40nodos.edgelist', data=(('weight',int),))
G1aa = nx.read_edgelist('testNebulinGraph40nodos.edgelist', data=(('weight',int),))
list(G1a)
list(G1a.edges(data=True))
f = open('salida.out', 'a')
f.write("diferencia de sets ini\n")
for elem in list(set(list(G1aa)) - set(list(G1a))):
        f.write(elem)
        f.write(" , ")
f.write("\n")
f.write("diferencia de sets fin\n")
f.close()

f = open('salida.out', 'a')
ged=gm.GraphEditDistance(1,1,1,1)

result = ged.compare([G1a, G1aa], None)

for elem in result:
        f.write("[")
        for subelem in elem:
                f.write(str(subelem))
                f.write(" , ")
        f.write("]\n")
f.write("]\n")

ged.similarity(result)

f.write("[")

for elem in ged.similarity(result):
        f.write("[")
        for subelem in elem:
                f.write(str(subelem))
                f.write(" , ")
        f.write("]\n")
f.write("]\n")
f.close()


