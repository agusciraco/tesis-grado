import sys


def file_generator(filename, cant_nodes):
    f = open(filename, 'w')
    j = 1
    for i in range(1, cant_nodes+1, 1):
        f.write(f'{str(j)} {str(i+1)} 1\n')
        if divmod(i+1, 10)[1] is 0:
            j += 1
    f.close()


if __name__ == '__main__':

    if len(sys.argv) < 3:
        print("missing file and nodes, use: trie_generator.py file.txt 200")
        exit(1)

    filename = sys.argv[1]
    cant_nodes = int(sys.argv[2])
    file_generator(filename, cant_nodes)

