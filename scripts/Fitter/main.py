# Polynomial Regression

import pandas as pd
import matplotlib.pyplot as plt
import numpy
from sklearn.linear_model import LinearRegression
from scipy.optimize import curve_fit
from deltacon.deltacon import GenAdjacentMatrix, DeltaCon
from trie_generator.trie_generator import file_generator
import time
from decimal import Decimal


def generate_tests_files():
    for i in range(100, 3100, 100):
        filename = f'/home/naciraa/Documents/tesis/tesiskapow/codigo/testComplejidadDeltacon/ejemplo-{str(i)}-nodes.txt'
        print(filename)
        file_generator(filename, i)


def run_deltacon():
    results_file = '/home/naciraa/Documents/tesis/tesiskapow/codigo/testComplejidadDeltacon/results.txt'
    results_file_csv = '/home/naciraa/Documents/tesis/tesiskapow/codigo/testComplejidadDeltacon/results.csv'
    for i in range(100, 3100, 100):
        f = open(results_file, 'a')
        r = open(results_file_csv, 'a')
        filename = f'/home/naciraa/Documents/tesis/tesiskapow/codigo/testComplejidadDeltacon/ejemplo-{str(i)}-nodes.txt'
        print(filename)
        f.write(f'case: {filename}\n')
        start = time.time()
        A1 = GenAdjacentMatrix(filename)
        A2 = GenAdjacentMatrix(filename)
        sim = DeltaCon(A1, A2, 1)
        print(f'result sim: {str(sim)}')
        f.write(f'result sim: {str(sim)}\n')
        end = time.time()
        print(f'time: {str(end - start)}')
        f.write(f'time: {str(end - start)}\n')
        r.write(f'{i},{str(end - start)}\n')
        f.close()
        r.close()


def fitter(file, x_label, y_label, label):
    # file = '/home/naciraa/Documents/tesis/tesiskapow/codigo/testComplejidadDeltacon/results.csv'
    df = pd.read_csv(file, header=0)
    df.columns = ['x', 'y']
    x_real = df['x'].to_numpy()
    y_real = df['y'].to_numpy()
    # x_real = np.linspace(2., 10., num=100)
    # y_real = 2 ** x_real + np.random.normal(0, 50, x_real.shape[0])

    # reg = LinearRegression().fit((2 ** x_real).reshape(-1, 1), y_real)
    reg = LinearRegression().fit((x_real ** 2).reshape(-1, 1), y_real)
    # print(reg.score((2 ** x_real).reshape(-1, 1), y_real))
    print(reg.score((x_real ** 2).reshape(-1, 1), y_real))
    print(reg.coef_)
    # y_predicted = reg.predict((2 ** x_real).reshape(-1, 1))
    y_predicted = reg.predict((x_real ** 2).reshape(-1, 1))
    plt.scatter(x_real, y_real, label=label)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.plot(x_real, y_predicted, 'r-', label=('aX^2'))
    plt.legend(loc=(0.2, 0.8))
    yticks, yticks_label = plt.yticks()
    yticks_l_new = []
    for y in yticks:
        yticks_l_new.append(format(Decimal(y), ',f'))
    plt.yticks(yticks, yticks_l_new)
    xticks, xticks_label = plt.xticks()
    xticks_l_new = []
    for x in xticks:
        xticks_l_new.append(format(Decimal(x), ',f'))
    plt.xticks(xticks, xticks_l_new)
    plt.show()


def f(x, a, b, n):
    return a * x ** n / (x ** n + b)


def fitter_asymptotic(file):
    df = pd.read_csv(file, header=0)
    df.columns = ['x', 'y']
    x_real = df['x'].to_numpy()
    y_real = df['y'].to_numpy()
    # https://stackoverflow.com/questions/19189362/getting-the-r-squared-value-using-curve-fit
    # https://stackoverflow.com/questions/45554107/asymptotic-regression-in-python
    popt, pcov = curve_fit(f, x_real, y_real, p0=[1, 0.5, 0])
    residuals = y_real - f(x_real, *popt)
    ss_res = numpy.sum(residuals ** 2)
    ss_tot = numpy.sum((y_real - numpy.mean(y_real)) ** 2)
    r_squared = 1 - (ss_res / ss_tot)
    print(r_squared)
    plt.scatter(x_real, y_real, label=('Elementos en intersección', 'distanciaDeTries'))
    plt.xlabel('Elementos en intersección')
    plt.ylabel('distanciaDeTries')
    plt.plot(x_real, f(x_real, *popt), 'r-', label='a*x^n / (x^n + b)')
    plt.legend(loc=(0.2, 0.7))
    # yticks, yticks_label = plt.yticks()
    # yticks_l_new = []
    # for y in yticks:
    #     yticks_l_new.append(format(Decimal(y), ',f'))
    # plt.yticks(yticks, yticks_l_new)
    xticks, xticks_label = plt.xticks()
    xticks_l_new = []
    for x in xticks:
        xticks_l_new.append(format(Decimal(x), ',f'))
    plt.xticks(xticks, xticks_l_new)
    plt.show()


def fitter_asymptotic2(file):
    df = pd.read_csv(file, header=0)
    df.columns = ['x', 'y']
    x_real = df['x'].to_numpy()
    y_real = df['y'].to_numpy()

    reg = LinearRegression().fit((1 / x_real).reshape(-1, 1), y_real)
    print(reg.score((1 / x_real).reshape(-1, 1), y_real))
    print(reg.coef_)
    y_predicted = reg.predict((1 / x_real).reshape(-1, 1))
    plt.scatter(x_real, y_real, label=('Elementos fuera de intersección', 'distanciaDeTries'))
    plt.xlabel('Elementos fuera de intersección')
    plt.ylabel('distanciaDeTries')
    plt.plot(x_real, y_predicted, 'r-', label='1/x')
    plt.legend(loc=(0.2, 0.2))
    xticks, xticks_label = plt.xticks()
    xticks_l_new = []
    for x in xticks:
        xticks_l_new.append(format(Decimal(x), ',f'))
    plt.xticks(xticks, xticks_l_new)
    plt.show()


def plot(file, x_label='x', y_label='y'):
    df = pd.read_csv(file, header=0)
    header=list(df.columns.values)
    df.columns = ['x', 'y1', 'y2']
    x = df['x'].to_numpy()
    y1 = df['y1'].to_numpy()
    y2 = df['y2'].to_numpy()

    plt.scatter(x, y1, label=(header[1]))
    plt.scatter(x, y2, label=(header[2]))
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.plot()
    plt.legend(loc=(0.4, 0.3))
    xticks, xticks_label = plt.xticks()
    xticks_l_new = []
    xticks_new = []
    for x in xticks:
        if x > 0:
            xticks_l_new.append(format(Decimal(x), ',f'))
            xticks_new.append(x)
    plt.xticks(xticks_new, xticks_l_new)
    yticks, yticks_label = plt.yticks()
    yticks_l_new = []
    yticks_new = []
    for y in yticks:
        print(y)
        if y > 0:
            yticks_l_new.append(format(Decimal(y), ',f'))
            yticks_new.append(y)
    plt.yticks(yticks_new, yticks_l_new)
    plt.show()


import csv


def order_csv(filename):
    with open(filename + '.csv', 'r') as infile, open(filename + '-reordered.csv', 'a') as outfile:
        # output dict needs a list for new column ordering
        fieldnames = ['Protein Family', 'Annexin', 'Arm',	'CWbinding1', 'Collagen', 'Fer4', 'Filamin', 'HEAT', 'HemolysinCabind', 'Hexapep', 'Kelch1',
                      'LRR1', 'Ldlrecepta', 'Ldlreceptb', 'MORN', 'Mitocarr', 'NEWAnk', 'NEWAnk_WithoutP25963', 'NEWWD40', 'Nebulin',
                      'PD40', 'PPR', 'PUF', 'Pentapeptide', 'Sel1', 'TPR1', 'TSP1', 'YadAhead', 'ABCtran', 'CorA', 'GDCP', 'Globin',
                      'Glycohydro19', 'GreAGreB', 'HelicaseC', 'MIP', 'MgtE', 'PBP', 'PFLlike', 'PIN', 'PPbinding', 'PUD',
                      'PeptidaseC25', 'Pkinase', 'Rhomboid', 'TerB', 'Thaumatin', 'ValtRNAsyntC', 'ABCtran_SCRAMBLED',
                      'ABCtran_UNIFORM', 'NEWAnk_SCRAMBLED', 'NEWAnk_UNIFORM', 'mix', 'mixScrambled', 'mixUniformDistributed']
        writer = csv.DictWriter(outfile, fieldnames=fieldnames)
        # reorder the header first
        writer.writeheader()
        for row in csv.DictReader(infile):
            # writes the reordered rows to the new file
            writer.writerow(row)

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # See PyCharm help at https://www.jetbrains.com/help/pycharm/
    # Generate testcases
    # generate_tests_files()
    # Run deltacon cases:
    # run_deltacon()
    # Plot results
    # fitter('/home/naciraa/Documents/tesis/tesiskapow/codigo/testComplejidadDeltacon/results.csv', 'cantidad de nodos', 'tiempo(s)', 'Ejecución Deltacon')
    # fitter('/home/naciraa/Documents/tesis/tesiskapow/codigo/test-complejidad-gmat4py/results.csv', 'cantidad de nodos', 'tiempo(s)', 'Ejecución GMatch4py')
    # fitter('/home/naciraa/Documents/tesis/tesiskapow/codigo/test-complejidad-networkx/results.csv', 'cantidad de nodos', 'tiempo(s)', 'Ejecución Networkx')
    # fitter('/home/naciraa/Documents/tesis/tesiskapow/codigo/test-complejidad-networkx/results_mem.csv', 'cantidad de nodos', 'memoria(MB)', 'Ejecución Networkx')
    # fitter('/home/naciraa/Documents/tesis/tesiskapow/codigo/custom-cases/results-increase-diff.csv')
    # fitter_asymptotic('/home/naciraa/Documents/tesis/tesiskapow/codigo/custom-cases/A-parecido-B.csv')
    # fitter_asymptotic2('/home/naciraa/Documents/tesis/tesiskapow/codigo/custom-cases/A-incluido-en-B.csv')
    # fitter('/home/naciraa/Documents/tesis/tesiskapow/codigo/ABCtran-scrambled-vs-globin-bylevel.csv', 'globin', 'nivel', 'abctran-scrambled-vs-globin')
    # fitter('/home/naciraa/Documents/tesis/tesiskapow/codigo/ABCtran-uniform-vs-globin-bylevel.csv', 'globin', 'nivel', 'abctran-uniform-vs-globin')
    # plot('/home/naciraa/Documents/tesis/tesiskapow/codigo/aporte-por-familia/Aporte por árbol familia-CN-HelicaseC vs ABCtran_Scrambled.csv', 'Nivel de Trie', 'Cantidad de nodos')
    # plot('/home/naciraa/Documents/tesis/tesiskapow/codigo/aporte-por-familia/Aporte por árbol familia-CN-ABCtran-vs-ABCtran_scambled.csv', 'Nivel de Trie', 'Cantidad de nodos')
    # plot('/home/naciraa/Documents/tesis/tesiskapow/codigo/aporte-por-familia/Aporte por árbol familia-FP-HelicaseC vs ABCtran_Scrambled.csv', 'Nivel de Trie', 'Cantidad de nodos')
    # plot('/home/naciraa/Documents/tesis/tesiskapow/codigo/aporte-por-familia/Aporte por árbol familia-FP-ABCtran-vs-ABCtran_scambled.csv', 'Nivel de Trie', 'Cantidad de nodos')
    # plot('/home/naciraa/Documents/tesis/tesiskapow/codigo/corridaActual/aporte-por-familia/ABCtran_SCRAMBLED-vs-HelicaseC-CN.csv', 'Nivel de Trie', 'Cantidad de nodos')
    # plot('/home/naciraa/Documents/tesis/tesiskapow/codigo/corridaActual/aporte-por-familia/ABCtran_SCRAMBLED-vs-HelicaseC-TFP.csv', 'Nivel de Trie', 'Suma del valor de diferencia')
    plot('/home/naciraa/Documents/tesis/tesiskapow/codigo/corridaActual/aporte-por-familia/ABCtran_SCRAMBLED-vs-ABCtran-CN.csv', 'Nivel de Trie', 'Cantidad de nodos')
    plot('/home/naciraa/Documents/tesis/tesiskapow/codigo/corridaActual/aporte-por-familia/ABCtran_SCRAMBLED-vs-ABCtran-TFP.csv', 'Nivel de Trie', 'Suma del valor de diferencia')
    # order_csv('/home/naciraa/Documents/tesis/tesiskapow/codigo/corridaActual/ByLevel-2020-10-22/cantNodesMetricResult-L3')
    # order_csv('/home/naciraa/Documents/tesis/tesiskapow/codigo/corridaActual/ByLevel-2020-10-22/totalFootPrintMetricResult-L0')
    # order_csv('/home/naciraa/Documents/tesis/tesiskapow/codigo/corridaActual/cantInst-2020-10-29/totalFootPrintMetricResult-ini-fini')
    # order_csv('/home/naciraa/Documents/tesis/tesiskapow/codigo/corridaActual/AP-2020-10-29/totalFootPrintMetricResult-AP')