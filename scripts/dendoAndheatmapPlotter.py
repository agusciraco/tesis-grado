import plotly.graph_objects as go
import plotly.figure_factory as ff

import numpy as np
from scipy.spatial.distance import pdist, squareform


# get data
# ~ data = np.genfromtxt("../informe/datos/Resultados sin prefix by level by family - tFPM-Ldlrecepta.csv",
# ~ data = np.genfromtxt("../informe/datos/Resultados sin prefix by level - L11totalFootPrintMetric.csv",
# ~ data = np.genfromtxt("../informe/datos/Resultados sin prefix by level - L11totalFootPrintMetric.csv",
data = np.genfromtxt("totalAffectedProteinsMetricResultLevel100.csv",
# ~ data = np.genfromtxt("totalFootPrintMetricAPResultLevel100ini-fini.csv",
# ~ data = np.genfromtxt("../informe/datos/cantNodesMetricResultLevel11_prueba_dendo.csv",
                     names=True,dtype=float, delimiter=",")
data_array = data.view((np.float, len(data.dtype.names)))
data_array = data_array.transpose()
labels = data.dtype.names

# Initialize figure by creating upper dendrogram
fig = ff.create_dendrogram(data_array, orientation='bottom', labels=labels)
for i in range(len(fig['data'])):
    fig['data'][i]['yaxis'] = 'y2'

# Create Side Dendrogram
dendro_side = ff.create_dendrogram(data_array, orientation='right')
# ~ print(dendro_side)
for i in range(len(dendro_side['data'])):
    dendro_side['data'][i]['xaxis'] = 'x2'

# Add Side Dendrogram Data to Figure
# ~ stack1 = []
for data in dendro_side['data']:
	fig.add_trace(data)
	# ~ stack1.append(data)


# ~ dataCant = len(stack1);
# ~ for i in range(dataCant):
	# ~ fig.add_trace(stack1.pop())


# Create Heatmap
dendro_leaves = dendro_side['layout']['yaxis']['ticktext']
dendro_leaves = list(map(int, dendro_leaves))
heat_data = data_array
heat_data = heat_data[dendro_leaves,:]
heat_data = heat_data[:,dendro_leaves]

heatmap = [
    go.Heatmap(
        x = dendro_leaves,
        y = dendro_leaves,
        z = heat_data
    )
]
heatmap[0]['x'] = fig['layout']['xaxis']['tickvals']
heatmap[0]['y'] = dendro_side['layout']['yaxis']['tickvals']

# Add Heatmap Data to Figure
for data in heatmap:
    fig.add_trace(data)


# Edit Layout
fig.update_layout({'width':800, 'height':800,
                         'showlegend':False, 'hovermode': 'closest',
                         })
# Edit xaxis
fig.update_layout(xaxis={'domain': [.15, 1],
                                  'mirror': False,
                                  'showgrid': False,
                                  'showline': False,
                                  'zeroline': False,
                                  'ticks':""})
# Edit xaxis2
fig.update_layout(xaxis2={'domain': [0, .15],
                                   'mirror': False,
                                   'showgrid': False,
                                   'showline': False,
                                   'zeroline': False,
                                   'showticklabels': False,
                                   'ticks':""})

# Edit yaxis
fig.update_layout(yaxis={'domain': [0, .85],
                                  'mirror': False,
                                  'showgrid': False,
                                  'showline': False,
                                  'zeroline': False,
                                  'showticklabels': False,
                                  'ticks': ""
                        })
# Edit yaxis2
fig.update_layout(yaxis2={'domain':[.825, .975],
                                   'mirror': False,
                                   'showgrid': False,
                                   'showline': False,
                                   'zeroline': False,
                                   'showticklabels': False,
                                   'ticks':""})
# ~ print(fig)
fig['layout']['yaxis']['ticktext'] = fig['layout']['xaxis']['ticktext']
fig['layout']['yaxis']['tickvals'] = fig['layout']['xaxis']['tickvals']

# Plot!
fig.show()
