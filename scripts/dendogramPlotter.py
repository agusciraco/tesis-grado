import plotly.graph_objects as go
import plotly.figure_factory as ff

import numpy as np
from scipy.spatial.distance import pdist, squareform


# get data
# ~ data = np.genfromtxt("../informe/datos/Resultados sin prefix by level by family - tFPM-Ldlrecepta.csv",
# ~ data = np.genfromtxt("../informe/datos/Resultados sin prefix by level - L11totalFootPrintMetric.csv",
# data = np.genfromtxt("../informe/datos/Resultados sin prefix by level - L11totalFootPrintMetric.csv",
data = np.genfromtxt("../informe/datos/cantNodesMetricResultLevel11_prueba_dendo.csv",
# ~ data = np.genfromtxt("../informe/datos/cantNodesMetricResultLevel11_prueba_dendo.csv",
                     names=True,dtype=float, delimiter=",")
data_array = data.view((np.float, len(data.dtype.names)))
labels = data.dtype.names

# Initialize figure by creating upper dendrogram
# fig = ff.create_dendrogram(data_array, orientation='bottom', labels=labels, color_threshold=0.2)
fig = ff.create_dendrogram(data_array, orientation='bottom', labels=labels)

# Edit Layout
fig.update_layout({'width':800, 'height':800,
                         'showlegend':False, 'hovermode': 'closest',
                         })

# Plot!
fig.show()

#create heatmap
heatmap = [go.Heatmap(
                   z=data_array,
                   x=list(labels),
                   y=list(labels),
                   hoverongaps = False)
                   ]
                
fig2 = go.Figure(heatmap)

# Plot!
# fig2.show()
