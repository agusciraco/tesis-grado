#ifndef __TRIE_H_
#define __TRIE_H_

#include <iostream>
#include <fstream>
#include <string>
#include <set>
#include <vector>
#include <map>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <stack>
#include <queue>
#include <utility>
#include <cstring>
#include <sstream>


using namespace std;


struct nodoTrie {
	map< char, nodoTrie * > info;
	bool isFinal;
	int footPrint;
};

struct trie {
	nodoTrie * ini;
	int maxFootPrint;
	int totalFootPrint;
};

void insertString (trie * aTrie, string s);
void insertStringWithFP (trie * aTrie, pair<string, int> pattern);
void calculateTotalFootPrint(trie * aTrie);
int calculateTotalFootPrintAux(nodoTrie * node);
bool searchString (trie * aTrie, string s);
int searchStringCov (trie * aTrie, string &s);
void stringCoverage (trie * aTrie, string &s);
void printFootPrint (trie * aTrie, int minDepth, int maxDepth);
void printTrieAsGraph (trie * aTrie, int minDepth, int maxDepth);
void printTrieAsGraphStr (trie * aTrie, int maxDepth);
void printTrieLine(nodoTrie * tree, trie * aTrie, int maxDepth);
void printTree(nodoTrie * tree, trie * aTrie, int maxDepth);
void printTreeWord(nodoTrie * tree, trie * aTrie, string parent, int maxDepth);
void printTreeWord2(nodoTrie * tree, trie * aTrie, string parent, int minDepth, int maxDepth);
int printTreeWord2Sim(nodoTrie * tree, trie * aTrie, int parent, int nodeNumber, int minDepth, int maxDepth);
string printTreeWord2SimStr(nodoTrie * tree, trie * aTrie, string parentLabel, int maxDepth);
int countTree(nodoTrie * tree);
void writeToFile(string filename, string line);
void writeAdjancencyMatrixToFile(string filename, string line);


pair<int,int> compare2TreeMetric(nodoTrie * aTrie1, nodoTrie * aTrie2);
pair<int,int> getMetricOfBranch(nodoTrie * aTrie1);

pair<int,int> compare2TreeMetricByLevel(nodoTrie * aTrie1, nodoTrie * aTrie2, int level, int maxLevel);
pair<int, int> getMetricOfBranchByLevel(nodoTrie * aTrie1, int level, int maxLevel);

map<string, int> compare2TreeMetricByLevelMap(nodoTrie * aTrie1, nodoTrie * aTrie2, int level, int maxLevel);

int compare2Tries(trie * myTrie1, trie * myTrie2);
pair<int,int> compare2TriesWithFP(trie * myTrie1, trie * myTrie2);
pair<int,int> compare2TriesByLevel(trie * myTrie1, trie * myTrie2, int baseLevel, int maxLevel);

void createSetWithSymbolsNoOcurrencies(set<string> &patronsSet, string symbols, int maxdepth, int seed);

void createTrieWithSetAndOcurrencies(set<string> &smr, trie * smrTrie, int seed);


void deleteNodoTrie(nodoTrie * node);
void deleteTrie(trie * aTrie);

int validateTotalFootPrintAux(nodoTrie * node);
void validateTotalFootPrint(trie * aTrie);
#endif
