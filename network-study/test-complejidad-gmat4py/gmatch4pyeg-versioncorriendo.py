# Gmatch4py use networkx graph 
import networkx as nx
# import the GED using the munkres algorithm
import gmatch4py as gm
import time
from memory_profiler import profile


@profile
def run_compare(ged, G1a, G1aa):
    start = time.time()
    result = ged.compare([G1a, G1aa], None)
    end = time.time()
    return start, result, end


def run_gmatch4py():
    path = '/home/naciraa/Documents/tesis/tesiskapow/codigo/test-complejidad-gmat4py'
    results_file = f'{path}/results.txt'
    # results_file_sim = f'{path}/results_sim.txt'
    # results_file_csv = f'{path}/results.csv'
    results_file_mem_csv = f'{path}/results_mem.csv'
    # results_file_sim_csv = f'{path}/results_sim.csv'
    for i in range(2600, 3100, 100):
        f = open(results_file, 'a')
        r = open(results_file_mem_csv, 'a')
        filename = f'{path}/ejemplo-{str(i)}-nodes.txt'
        print(filename)
        G1a = nx.read_edgelist(filename, data=(('weight', int),))
        G1aa = nx.read_edgelist(filename, data=(('weight', int),))
        f.write(f'case: {filename}\n')
        ged = gm.GraphEditDistance(1, 1, 1, 1)
        start, result, end = run_compare(ged, G1a, G1aa)
        print(f'result sim: {str(result)}')
        f.write(f'result sim: {str(result)}\n')
        print(f'time: {str(end - start)}')
        f.write(f'time: {str(end - start)}\n')
        r.write(f'{i},{str(end - start)}\n')
        f.close()
        r.close()
        # f = open(results_file_sim, 'a')
        # r = open(results_file_sim_csv, 'a')
        # f.write(f'case: {filename}\n')
        # start = time.time()
        # sim = ged.similarity(result)
        # end = time.time()
        # print(f'result sim: {str(sim)}')
        # f.write(f'result sim: {str(sim)}\n')
        # print(f'time: {str(end - start)}')
        # f.write(f'time: {str(end - start)}\n')
        # r.write(f'{i},{str(end - start)}\n')
        # f.close()
        # r.close()


if __name__ == '__main__':
    run_gmatch4py()