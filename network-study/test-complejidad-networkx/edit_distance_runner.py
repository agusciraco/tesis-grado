import networkx as nx
import time
from memory_profiler import profile


@profile
def run_compare(G1a, G1aa):
    start = time.time()
    sim = nx.graph_edit_distance(G1a, G1aa)
    end = time.time()
    return sim, start, end


def run_nx_edit_distance_cases():
    path = '/home/naciraa/Documents/tesis/tesiskapow/codigo/test-complejidad-networkx'
    results_file = f'{path}/results.txt'
    results_file_csv = f'{path}/results.csv'
    for i in range(700, 800, 100):
        f = open(results_file, 'a')
        r = open(results_file_csv, 'a')
        filename = f'{path}/ejemplo-{str(i)}-nodes.txt'
        print(filename)
        f.write(f'case: {filename}\n')
        G1a = nx.read_edgelist(filename, data=(('weight', int),))
        G1aa = nx.read_edgelist(filename, data=(('weight', int),))
        sim, start, end = run_compare(G1a, G1aa)
        print(f'result sim: {str(sim)}')
        f.write(f'result sim: {str(sim)}\n')
        print(f'time: {str(end - start)}')
        f.write(f'time: {str(end - start)}\n')
        r.write(f'{i},{str(end - start)}\n')
        f.close()
        r.close()


if __name__ == '__main__':
    run_nx_edit_distance_cases()
