import networkx as nx 

G1a = nx.read_edgelist('testGlobinGraph40nodos.edgelist', data=(('weight',int),))
G1aa = nx.read_edgelist('testNebulinGraph40nodos.edgelist', data=(('weight',int),))

f = open('salida.out', 'a')
f.write("40 nodos\n")
f.write("diferencia de sets ini\n")
for elem in list(set(list(G1aa)) - set(list(G1a))):
        f.write(elem)
        f.write(" , ")
f.write("\n")
f.write("diferencia de sets fin\n")
f.close()

f = open('salida.out', 'a')
from datetime import datetime

# current date and time
before = datetime.now()

f.close()
result=nx.graph_edit_distance(G1a,G1aa) 
print(result)
f = open('salida.out', 'a')
f.write("result: ")
# ~ f.write(str(result))
f.write(str(result))

# current date and time
after = datetime.now()
beforeTs= datetime.timestamp(before)
afterTs= datetime.timestamp(after)
diffTs= afterTs-beforeTs
f.write("40 nodos\n")
f.write("time\n")
f.write(str(before))
f.write("\n")
f.write(str(after))
f.write("\n")
f.write(str(diffTs))
f.write("\n")
f.write("--------------------------------------\n")
f.close()



G1a = nx.read_edgelist('testGlobinGraph70nodos.edgelist', data=(('weight',int),))
G1aa = nx.read_edgelist('testNebulinGraph70nodos.edgelist', data=(('weight',int),))

f = open('salida.out', 'a')
f.write("70 nodos\n")
f.write("diferencia de sets ini\n")
for elem in list(set(list(G1aa)) - set(list(G1a))):
        f.write(elem)
        f.write(" , ")
f.write("\n")
f.write("diferencia de sets fin\n")
f.close()

f = open('salida.out', 'a')
from datetime import datetime

# current date and time
before = datetime.now()
f.close()

result=nx.graph_edit_distance(G1a,G1aa) 
f = open('salida.out', 'a')
print(result)
f.write("result: ")
f.write(str(result))

# current date and time
after = datetime.now()
beforeTs= datetime.timestamp(before)
afterTs= datetime.timestamp(after)
diffTs= afterTs-beforeTs
f.write("70 nodos\n")
f.write("time\n")
f.write(str(before))
f.write("\n")
f.write(str(after))
f.write("\n")
f.write(str(diffTs))
f.write("\n")
f.write("--------------------------------------\n")
f.close()

G1a = nx.read_edgelist('testGlobinGraph100nodos.edgelist', data=(('weight',int),))
G1aa = nx.read_edgelist('testNebulinGraph100nodos.edgelist', data=(('weight',int),))

f = open('salida.out', 'a')
f.write("100 nodos\n")
f.write("diferencia de sets ini\n")
for elem in list(set(list(G1aa)) - set(list(G1a))):
        f.write(elem)
        f.write(" , ")
f.write("\n")
f.write("diferencia de sets fin\n")
f.close()

f = open('salida.out', 'a')
from datetime import datetime

# current date and time
before = datetime.now()
f.close()

result=nx.graph_edit_distance(G1a,G1aa) 
f = open('salida.out', 'a')
print(result)
f.write("result: ")
f.write(str(result))

# current date and time
after = datetime.now()
beforeTs= datetime.timestamp(before)
afterTs= datetime.timestamp(after)
diffTs= afterTs-beforeTs
f.write("100 nodos\n")
f.write("time\n")
f.write(str(before))
f.write("\n")
f.write(str(after))
f.write("\n")
f.write(str(diffTs))
f.write("\n")
f.write("--------------------------------------\n")
f.close()

# ~ G1a = nx.read_edgelist('testGlobinGraph130nodos.edgelist', data=(('weight',int),))
# ~ G1aa = nx.read_edgelist('testNebulinGraph130nodos.edgelist', data=(('weight',int),))

# ~ f = open('salida.out', 'a')
# ~ f.write("130 nodos\n")
# ~ f.write("diferencia de sets ini\n")
# ~ for elem in list(set(list(G1aa)) - set(list(G1a))):
        # ~ f.write(elem)
        # ~ f.write(" , ")
# ~ f.write("\n")
# ~ f.write("diferencia de sets fin\n")
# ~ f.close()

# ~ f = open('salida.out', 'a')
# ~ from datetime import datetime

# ~ # current date and time
# ~ before = datetime.now()
# ~ f.close()

# ~ result=nx.graph_edit_distance(G1a,G1aa) 
# ~ f = open('salida.out', 'a')
# ~ print(result)
# ~ f.write("result: ")
# ~ f.write(str(result))

# ~ # current date and time
# ~ after = datetime.now()
# ~ beforeTs= datetime.timestamp(before)
# ~ afterTs= datetime.timestamp(after)
# ~ diffTs= afterTs-beforeTs
# ~ f.write("130 nodos\n")
# ~ f.write("time\n")
# ~ f.write(str(before))
# ~ f.write("\n")
# ~ f.write(str(after))
# ~ f.write("\n")
# ~ f.write(str(diffTs))
# ~ f.write("\n")
# ~ f.write("--------------------------------------\n")
# ~ f.close()

G1a = nx.read_edgelist('testGlobinGraph250nodos.edgelist', data=(('weight',int),))
G1aa = nx.read_edgelist('testNebulinGraph250nodos.edgelist', data=(('weight',int),))

f = open('salida.out', 'a')
f.write("250 nodos\n")
f.write("diferencia de sets ini\n")
for elem in list(set(list(G1aa)) - set(list(G1a))):
        f.write(elem)
        f.write(" , ")
f.write("\n")
f.write("diferencia de sets fin\n")
f.close()

f = open('salida.out', 'a')
from datetime import datetime

# current date and time
before = datetime.now()
f.close()

result=nx.graph_edit_distance(G1a,G1aa) 
f = open('salida.out', 'a')
print(result)
f.write("result: ")
f.write(str(result))

# current date and time
after = datetime.now()
beforeTs= datetime.timestamp(before)
afterTs= datetime.timestamp(after)
diffTs= afterTs-beforeTs
f.write("250 nodos\n")
f.write("time\n")
f.write(str(before))
f.write("\n")
f.write(str(after))
f.write("\n")
f.write(str(diffTs))
f.write("\n")
f.write("--------------------------------------\n")
f.close()

G1a = nx.read_edgelist('testGlobinGraph500nodos.edgelist', data=(('weight',int),))
G1aa = nx.read_edgelist('testNebulinGraph500nodos.edgelist', data=(('weight',int),))

f = open('salida.out', 'a')
f.write("500 nodos\n")
f.write("diferencia de sets ini\n")
for elem in list(set(list(G1aa)) - set(list(G1a))):
        f.write(elem)
        f.write(" , ")
f.write("\n")
f.write("diferencia de sets fin\n")
f.close()

f = open('salida.out', 'a')
from datetime import datetime

# current date and time
before = datetime.now()
f.close()

result=nx.graph_edit_distance(G1a,G1aa) 
f = open('salida.out', 'a')
print(result)
f.write("result: ")
f.write(str(result))

# current date and time
after = datetime.now()
beforeTs= datetime.timestamp(before)
afterTs= datetime.timestamp(after)
diffTs= afterTs-beforeTs
f.write("500 nodos\n")
f.write("time\n")
f.write(str(before))
f.write("\n")
f.write(str(after))
f.write("\n")
f.write(str(diffTs))
f.write("\n")
f.write("--------------------------------------\n")
f.close()

G1a = nx.read_edgelist('testGlobinGraph1000nodos.edgelist', data=(('weight',int),))
G1aa = nx.read_edgelist('testNebulinGraph1000nodos.edgelist', data=(('weight',int),))

f = open('salida.out', 'a')
f.write("1000 nodos\n")
f.write("diferencia de sets ini\n")
for elem in list(set(list(G1aa)) - set(list(G1a))):
        f.write(elem)
        f.write(" , ")
f.write("\n")
f.write("diferencia de sets fin\n")
f.close()

f = open('salida.out', 'a')
from datetime import datetime

# current date and time
before = datetime.now()
f.close()

result=nx.graph_edit_distance(G1a,G1aa) 
print(result)
f = open('salida.out', 'a')
f.write("result: ")
f.write(str(result))

# current date and time
after = datetime.now()
beforeTs= datetime.timestamp(before)
afterTs= datetime.timestamp(after)
diffTs= afterTs-beforeTs
f.write("1000 nodos\n")
f.write("time\n")
f.write(str(before))
f.write("\n")
f.write(str(after))
f.write("\n")
f.write(str(diffTs))
f.write("\n")
f.write("--------------------------------------\n")
f.close()
