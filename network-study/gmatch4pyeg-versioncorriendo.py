# Gmatch4py use networkx graph 
import networkx as nx 
# import the GED using the munkres algorithm
import gmatch4py as gm

# ~ G1a = nx.read_edgelist('ejemplo1a.edgelist', data=(('weight',int),))
# ~ G1aa = nx.read_edgelist('ejemplo1a.edgelist', data=(('weight',int),))

# ~ G1a = nx.read_edgelist('GlobinGraphL12.edgelist', data=(('weight',int),))
G1a = nx.read_edgelist('testGlobinGraph40nodos.edgelist', data=(('weight',int),))
# ~ G1a = nx.read_edgelist('treeGGlobin12.edgelist', data=(('weight',int),))
# ~ G1a = nx.read_edgelist('treeGGlobin12.edgelist', data=(('weight',int),))
# ~ G1a = nx.read_edgelist('treeGNebulin.edgelist', data=(('weight',int),))
# ~ G1aa = nx.read_edgelist('NebulinGraphL12.edgelist', data=(('weight',int),))
G1aa = nx.read_edgelist('testNebulinGraph40nodos.edgelist', data=(('weight',int),))
# ~ G1aa = nx.read_edgelist('treeGNebulin.edgelist', data=(('weight',int),))
# ~ G1aa = nx.read_edgelist('treeGNebulin12.edgelist', data=(('weight',int),))
# ~ G1aa = nx.read_edgelist('treeGNewAnk12.edgelist', data=(('weight',int),))
# ~ G1aa = nx.read_edgelist('treeGNewAnk3.edgelist', data=(('weight',int),))

list(G1a)
list(G1a.edges(data=True))
# ~ print(list(G1a))
# ~ print(list(G1aa))

f = open('salida.out', 'a')
f.write("diferencia de sets ini\n")
for elem in list(set(list(G1aa)) - set(list(G1a))):
        f.write(elem)
        f.write(" , ")
f.write("\n")
f.write("diferencia de sets fin\n")
f.close()

f = open('salida.out', 'a')
# ~ ged=gm.GraphEditDistance(1,1,1,1) # all edit costs are equal to 1
ged=gm.GraphEditDistance(1,1,1,1)

from datetime import datetime

# current date and time
before = datetime.now()
result=ged.compare([G1a,G1aa],None) 
# current date and time
after = datetime.now()
beforeTs= datetime.timestamp(before)
afterTs= datetime.timestamp(after)
diffTs= afterTs-beforeTs
print(beforeTs)
print(afterTs)
print(diffTs)
print(after)
print(after-before)
f.write("time\n")
f.write(str(before))
f.write("\n")
f.write(str(after))
f.write("\n")
f.write(str(diffTs))
f.write("\n")
f.write("--------------------------------------\n")
f.write("[")
for elem in result:
        f.write("[")
        for subelem in elem:
                f.write(str(subelem))
                f.write(" , ")
        f.write("]\n")
f.write("]\n")
ged.similarity(result)
print(ged.similarity(result))
f.write("[")
for elem in ged.similarity(result):
        f.write("[")
        for subelem in elem:
                f.write(str(subelem))
                f.write(" , ")
        f.write("]\n")
f.write("]\n")
f.close()       



