#include "utils.h"
#include "trie.h"
#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <string.h>
#include <fstream>
#include <sstream>
#include <dirent.h>
#include <vector>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>
#include <set>
#include <stack>

using namespace std;

string proteina;
//~ string pid;
vector< string> pid;
unordered_set < char > alfabeto;


struct SuffixArray {
  const int L;
  string s;
  vector<vector<int> > P;
  vector<pair<pair<int,int>,int> > M;

  SuffixArray(const string &s) : L(s.length()), s(s), P(1, vector<int>(L, 0)), M(L) {
    for (int i = 0; i < L; i++) P[0][i] = int(s[i]);
    for (int skip = 1, level = 1; skip < L; skip *= 2, level++) {
		//~ cout << "skip: " << skip << endl;
      P.push_back(vector<int>(L, 0));
      for (int i = 0; i < L; i++) 
	M[i] = make_pair(make_pair(P[level-1][i], i + skip < L ? P[level-1][i + skip] : -1000), i);
      sort(M.begin(), M.end());
      for (int i = 0; i < L; i++) 
	P[level][M[i].second] = (i > 0 && M[i].first == M[i-1].first) ? P[level][M[i-1].second] : i;
    }    
  }

  vector<int> GetSuffixArray() { return P.back(); }
  vector<int> GetMiSuffix() {
    vector<int > suffixes(P.back().size());
    for(int i=0; i<(int)P.back().size(); ++i){
      suffixes[P.back()[i]] = i;
    }
    return suffixes;
  }

  // returns the length of the longest common prefix of s[i...L-1] and s[j...L-1]
  int LongestCommonPrefix(int i, int j) {
    int len = 0;
    if (i == j) return L - i;
    for (int k = P.size() - 1; k >= 0 && i < L && j < L; k--) {
      if (P[k][i] == P[k][j]) {
	i += 1 << k;
	j += 1 << k;
	len += 1 << k;
      }
    }
    return len;
  }
    
};


bool myComp(int i, int j);
void calculateSAandLCP(string &s, vector< string > &sa, vector< int > &lcp);
void calculateSAIandLCP(string &s, vector< int > &sai, vector< int > &lcp);

bool myCompForSet(int i, int j);
void calculateSAIandLCPForSet(string &s, vector< int > &sai, vector< int > &lcp);
void calculateSAIandLCPForSetWithStruct(string &s, vector< int > &sai, vector< int > &lcp);

void findIntervals(vector< int > &sai, vector< int > &lcp, vector< pair< int, pair< int, int > > > &intervals);
void findIntervalsPaper(vector< int > &sai, vector< int > &lcp, vector< pair< int, pair< int, int > > > &intervals);
void processIntervals(vector< pair< int, pair< int, int > > > &intervals, vector< int > &sa, vector< int > &lcp);
void processIntervalsWithAffectedProteins(vector< pair< int, pair< int, int > > > &intervals, vector< int > &sa, vector< int > &lcp);

void createDicctionaryOfSmrBySetPair(set< pair<string,int> > &smr, vector< set< pair<string,int> > > &dicc);
void createDicctionaryOfSmr(set< string > &smr, vector< set< string > > &dicc);
void createTrieOfSmr(set< string > &smr, trie * smrTrie);
void createTrieOfSmrWithFP(set< pair<string,int> > &smr, trie * smrTrie);
void createTrieOfSmrPref(set< string > &smr, trie * smrTrie);

float calculateCoverage(string p, set< string > m);
float calculateCoverageWriteToFile(string &p, vector< set< string > > &dicc, ofstream &coverageFile);
float calculateCoverageKMP(string &p, vector< set< string > > &dicc, ofstream &coverageFile);
void calculateFootPrint();

void calculateCoverageInProteins();

void processAffectedProteins(vector< string> &pid, vector< int > &sa, string fileName);

void mainCustomCases();
void mainCustomCasesByLevel();
void calculateMetrics (trie * myTrie1, trie * myTrie2);
void calculateMetricsByLevel (trie * myTrie1, trie * myTrie2, int baseLevel, int maxLevel, vector<string> files);
void calculateMetricsByLevelMap (trie * myTrie1, trie * myTrie2, int baseLevel, int maxLevel, vector<string> files);
void calculateMetricsWAPByLevelMap (trie * myTrie1, trie * myTrie2, int baseLevel, int maxLevel, vector<string> files);


int main(){
    
	
	emptyFiles();
	
	mainCustomCases();
	//~ mainCustomCasesByLevel();
	
	return 0;
}

void calculateMetrics (trie * myTrie1, trie * myTrie2){
	pair<int,int> metric = compare2TriesWithFP(myTrie1, myTrie2);
	double cantNodesMetric =  (1.0*metric.first / (countTree(myTrie1->ini)-1 + countTree(myTrie2->ini)-1));
	cout << "Case metric: " << metric.first << "  " << metric.second << endl;
	cout << "Case result: " << cantNodesMetric << endl;
	cout << "Case diff FP: " << metric.second << " maxFP1: "<< myTrie1->maxFootPrint << " maxFP2: " << myTrie2->maxFootPrint << endl;
	cout << "Case sum MFP1 y MFP2: " << myTrie1->maxFootPrint + myTrie2->maxFootPrint << endl;
	cout << "Case TFP1: " << myTrie1->totalFootPrint << " TFP2: " << myTrie2->totalFootPrint << endl;
	cout << "Case sum TFP1 y TFP2: " << myTrie1->totalFootPrint + myTrie2->totalFootPrint << endl;
	double mFP = (1.0*metric.second / (myTrie1->maxFootPrint + myTrie2->maxFootPrint));
	cout << "Case diff FP/(maxFP1+maxFP2): " << mFP << endl;
	double tFP = (1.0*metric.second / ( myTrie1->totalFootPrint + myTrie2->totalFootPrint));
	cout << "Case diff FP/(TFP1+TFP2): " << tFP << endl;
	writeResultsHeatMap("totalFootPrintMetricResult.csv", to_string(tFP) + ",");
	writeResultsHeatMap("maxFootPrintMetricResult.csv", to_string(mFP) + ",");
	writeResultsHeatMap("cantNodesMetricResult.csv", to_string(cantNodesMetric) + ",");
}


void calculateMetricsByLevel (trie * myTrie1, trie * myTrie2, int baseLevel, int maxLevel, vector<string> files){
	pair<int,int> metric = compare2TriesByLevel(myTrie1, myTrie2, baseLevel, maxLevel);
	double cantNodesMetric =  (1.0*metric.first / (countTree(myTrie1->ini)-1 + countTree(myTrie2->ini)-1));
	cout << "Case metric: " << metric.first << "  " << metric.second << endl;
	cout << "Case result: " << cantNodesMetric << endl;
	cout << "Case diff FP: " << metric.second << " maxFP1: "<< myTrie1->maxFootPrint << " maxFP2: " << myTrie2->maxFootPrint << endl;
	cout << "Case sum MFP1 y MFP2: " << myTrie1->maxFootPrint + myTrie2->maxFootPrint << endl;
	cout << "Case TFP1: " << myTrie1->totalFootPrint << " TFP2: " << myTrie2->totalFootPrint << endl;
	cout << "Case sum TFP1 y TFP2: " << myTrie1->totalFootPrint + myTrie2->totalFootPrint << endl;
	double mFP = (1.0*metric.second / (myTrie1->maxFootPrint + myTrie2->maxFootPrint));
	cout << "Case diff FP/(maxFP1+maxFP2): " << mFP << endl;
	double tFP = (1.0*metric.second / ( myTrie1->totalFootPrint + myTrie2->totalFootPrint));
	cout << "Case diff FP/(TFP1+TFP2): " << tFP << endl;
	
	for (int i = 0; i < files.size(); i++){
		if(files[i].find("totalFootPrintMetric") != string::npos){
			writeResultsHeatMap(files[i], to_string(tFP) + ",");
		}
		if(files[i].find("maxFootPrintMetric") != string::npos){
			writeResultsHeatMap(files[i], to_string(mFP) + ",");
		}
		if(files[i].find("cantNodesMetric") != string::npos){
			writeResultsHeatMap(files[i], to_string(cantNodesMetric) + ",");
		}
	}
}

void calculateMetricsByLevelMap (trie * myTrie1, trie * myTrie2, int baseLevel, int maxLevel, vector<string> files){
	map<string, int> metric = compare2TreeMetricByLevelMap(myTrie1->ini, myTrie2->ini, baseLevel, maxLevel);
	
	double cantNodesMetric =  (1.0*metric["cantNodes"] / (countTree(myTrie1->ini)-1 + countTree(myTrie2->ini)-1));
	cout << "Case metric: " << metric["cantNodes"] << "  " << metric["tFP"] << endl;
	cout << "Case result: " << cantNodesMetric << endl;
	cout << "Case diff FP: " << metric["tFP"] << " maxFP1: "<< myTrie1->maxFootPrint << " maxFP2: " << myTrie2->maxFootPrint << endl;
	cout << "Case sum MFP1 y MFP2: " << myTrie1->maxFootPrint + myTrie2->maxFootPrint << endl;
	cout << "Case TFP1: " << myTrie1->totalFootPrint << " TFP2: " << myTrie2->totalFootPrint << endl;
	cout << "Case sum TFP1 y TFP2: " << myTrie1->totalFootPrint + myTrie2->totalFootPrint << endl;
	double mFP = (1.0*metric["tFP"] / (myTrie1->maxFootPrint + myTrie2->maxFootPrint));
	cout << "Case diff FP/(maxFP1+maxFP2): " << mFP << endl;
	double tFP = (1.0*metric["tFP"] / ( myTrie1->totalFootPrint + myTrie2->totalFootPrint));
	cout << "Case diff FP/(TFP1+TFP2): " << tFP << endl;
	cout << "diff added by aTrie1-CN: " << metric["aTrie1-CN"] << endl;
	cout << "diff added by aTrie1-FP: " << metric["aTrie1-FP"] << endl;
	cout << "diff added by aTrie2-CN: " << metric["aTrie2-CN"] << endl;
	cout << "diff added by aTrie2-FP: " << metric["aTrie2-FP"] << endl;
	
	for (int i = 0; i < files.size(); i++){
		if(files[i].find("totalFootPrintMetric") != string::npos){
			writeResultsHeatMap(files[i], to_string(tFP) + ",");
		}
		if(files[i].find("maxFootPrintMetric") != string::npos){
			writeResultsHeatMap(files[i], to_string(mFP) + ",");
		}
		if(files[i].find("cantNodesMetric") != string::npos){
			writeResultsHeatMap(files[i], to_string(cantNodesMetric) + ",");
		}
	}
}

void calculateMetricsWAPByLevelMap (trie * myTrie1, trie * myTrie2, int baseLevel, int maxLevel, vector<string> files){
	map<string, int> metric = compare2TreeMetricByLevelMap(myTrie1->ini, myTrie2->ini, baseLevel, maxLevel);
	
	double cantNodesMetric =  (1.0*metric["cantNodes"] / (countTree(myTrie1->ini)-1 + countTree(myTrie2->ini)-1));
	cout << "Case metric: " << metric["cantNodes"] << "  " << metric["tFP"] << endl;
	cout << "Case result: " << cantNodesMetric << endl;
	cout << "Case diff FP: " << metric["tFP"] << " maxFP1: "<< myTrie1->maxFootPrint << " maxFP2: " << myTrie2->maxFootPrint << endl;
	cout << "Case sum MFP1 y MFP2: " << myTrie1->maxFootPrint + myTrie2->maxFootPrint << endl;
	cout << "Case TFP1: " << myTrie1->totalFootPrint << " TFP2: " << myTrie2->totalFootPrint << endl;
	cout << "Case sum TFP1 y TFP2: " << myTrie1->totalFootPrint + myTrie2->totalFootPrint << endl;
	double mFP = (1.0*metric["tFP"] / (myTrie1->maxFootPrint + myTrie2->maxFootPrint));
	cout << "Case diff FP/(maxFP1+maxFP2): " << mFP << endl;
	double tFP = (1.0*metric["tFP"] / ( myTrie1->totalFootPrint + myTrie2->totalFootPrint));
	cout << "Case diff FP/(TFP1+TFP2): " << tFP << endl;
	cout << "diff added by aTrie1-CN: " << metric["aTrie1-CN"] << endl;
	cout << "diff added by aTrie1-FP: " << metric["aTrie1-FP"] << endl;
	cout << "diff added by aTrie2-CN: " << metric["aTrie2-CN"] << endl;
	cout << "diff added by aTrie2-FP: " << metric["aTrie2-FP"] << endl;
	
	for (int i = 0; i < files.size(); i++){
		if(files[i].find("totalFootPrintMetric") != string::npos){
			writeResultsHeatMap(files[i], to_string(tFP) + ",");
		}
		if(files[i].find("maxFootPrintMetric") != string::npos){
			writeResultsHeatMap(files[i], to_string(mFP) + ",");
		}
		if(files[i].find("cantNodesMetric") != string::npos){
			writeResultsHeatMap(files[i], to_string(cantNodesMetric) + ",");
		}
	}
}

void mainCustomCases(){
	//case 1 - Disjoints sets
	cout << "case 1 - Disjoints sets: " << endl;
	trie * myTrie1 = new trie();
	trie * myTrie2 = new trie();
	myTrie1->totalFootPrint = 0;
	myTrie2->totalFootPrint = 0;
	set<string> patronsSet;
	set<string> patronsSet2;
	string symbols = "abcdefg";
	string symbols2 = "jklmnop";
	string symbols3 = "tuvwxyz";
	string symbols4 = "hiqrs";
	int maxdepth = 4;
	int maxdepth2 = 3;
	int seed = 1;
	int seed2 = 20;
	
	createSetWithSymbolsNoOcurrencies(patronsSet, symbols, maxdepth, seed);
	createTrieOfSmr(patronsSet, myTrie1);
	int totalFootPrint1 = 0;
	cout << " patronsSet:" ;
	for (set<string>::iterator it=patronsSet.begin(); it!=patronsSet.end(); ++it){
		cout << ' ' << *it;
		totalFootPrint1 += (*it).size();
	}
    cout << endl;
	createSetWithSymbolsNoOcurrencies(patronsSet2, symbols2, maxdepth2, seed2);
	createTrieOfSmr(patronsSet2, myTrie2);
	cout << " patronsSet2:" ;
	int totalFootPrint2 = 0;
	for (set<string>::iterator it=patronsSet2.begin(); it!=patronsSet2.end(); ++it){
		cout << ' ' << *it;
		totalFootPrint2 += (*it).size();
	}
	cout << endl;
	
	pair<int,int> metric = compare2TriesWithFP(myTrie1, myTrie2);
	double res =  (1.0*metric.first / (countTree(myTrie1->ini)-1 + countTree(myTrie2->ini)-1));
	cout << "Case 1 result: " << res << endl;
	cout << "Case 1 diff FP: " << metric.second << " maxFP1: "<< myTrie1->maxFootPrint << " maxFP2: " << myTrie2->maxFootPrint << endl;
	//~ cout << "Case 1 TFP: " << myTrie1->totalFootPrint << "=? " << totalFootPrint1 << " => " << myTrie1->totalFootPrint == totalFootPrint1 << " TFP2: " << myTrie2->totalFootPrint  << "=? " << totalFootPrint2 << " => " << myTrie2->totalFootPrint == totalFootPrint2 << << endl;
	cout << "Case 1 TFP: " << myTrie1->totalFootPrint << "=? " << totalFootPrint1 << " TFP2: " << myTrie2->totalFootPrint  << "=? " << totalFootPrint2 << endl;
	cout << "Case 1 sum FP1 y FP2: " << myTrie1->maxFootPrint + myTrie2->maxFootPrint << endl;
	cout << "Case 1 sum TFP1 y TFP2: " << myTrie1->totalFootPrint + myTrie2->totalFootPrint << endl;
	cout << "Case 1 diff FP/(maxFP1+maxFP2): " << (1.0*metric.second / (myTrie1->maxFootPrint + myTrie2->maxFootPrint)) << endl;
	cout << "Case 1 diff FP/(TFP1+TFP2): " << (1.0*metric.second / ( myTrie1->totalFootPrint + myTrie2->totalFootPrint)) << endl;
	
	cout << "-------------------------------------"<< endl;
	//case 2 - Equal sets
	cout << "case 2 - Equal sets: " << endl;
	trie * myTrie3 = new trie();
	
	createSetWithSymbolsNoOcurrencies(patronsSet, symbols, maxdepth, seed);
	createTrieOfSmr(patronsSet, myTrie3);
	//~ //createTireWithSetAndOcurrencies(patronsSet, myTrie1);
	
	pair<int,int> metric2 = compare2TriesWithFP(myTrie1, myTrie3);
	double res2 =  (1.0*metric2.first / (countTree(myTrie1->ini)-1 + countTree(myTrie3->ini)-1));
	cout << "Case 2 result: " << res2 << endl;
	cout << "Case 2 diff FP: " << metric2.second << " maxFP1: "<< myTrie1->maxFootPrint << " maxFP2: " << myTrie3->maxFootPrint << endl;
	cout << "Case 2 sum FP1 y FP2: " << myTrie1->maxFootPrint + myTrie3->maxFootPrint << endl;
	cout << "Case 2 diff FP/(maxFP1+maxFP2): " << (1.0*metric2.second / (myTrie1->maxFootPrint + myTrie3->maxFootPrint)) << endl;
	cout << "Case 2 TFP: " << myTrie1->totalFootPrint << "=? " << totalFootPrint1 << " TFP2: " << myTrie3->totalFootPrint  << "=? " << totalFootPrint1 << endl;
	cout << "Case 2 sum TFP1 y TFP2: " << myTrie1->totalFootPrint + myTrie3->totalFootPrint << endl;
	cout << "Case 2 diff FP/(TFP1+TFP2): " << (1.0*metric2.second / ( myTrie1->totalFootPrint + myTrie3->totalFootPrint)) << endl;
		
	cout << "-------------------------------------"<< endl;
	
	//case 3 - Set A included in B 
	cout << "case 3 - Set A included in B: " << endl;
	trie * myTrie4 = new trie();
	myTrie4->totalFootPrint = 0;
	
	set<string> patronsSet3;
	int maxdepth3 = 6;
	
	createSetWithSymbolsNoOcurrencies(patronsSet3, symbols, maxdepth, seed);
	int totalFootPrint4 = 0;
	for (set<string>::iterator it=patronsSet3.begin(); it!=patronsSet3.end(); ++it){
		totalFootPrint4 += (*it).size();
	}
	createTrieOfSmr(patronsSet3, myTrie4);
	
	int intersectionSize = patronsSet3.size() ;

	cout << "intersection size: " << intersectionSize << endl;
	
	for (int i = 1; i < 1000; i++)
	{
		createSetWithSymbolsNoOcurrencies(patronsSet3, symbols2, maxdepth3, i);
		cout << "difference size: " << patronsSet3.size() - intersectionSize << endl;
		
		trie * myTrie5 = new trie();
		myTrie5->totalFootPrint = 0;
		createTrieOfSmr(patronsSet3, myTrie5);
		
		int totalFootPrint5 = 0;
		for (set<string>::iterator it=patronsSet3.begin(); it!=patronsSet3.end(); ++it){
			totalFootPrint5 += (*it).size();
		}
		
		pair<int,int> metric3 = compare2TriesWithFP(myTrie4, myTrie5);
		double res3 =  (1.0*metric3.first / (countTree(myTrie4->ini)-1 + countTree(myTrie5->ini)-1));
		cout << "Case 3 result: " << res3 << endl;
		cout << "Case 3 diff FP: " << metric3.second << " maxFP1: "<< myTrie4->maxFootPrint << " maxFP2: " << myTrie5->maxFootPrint << endl;
		cout << "Case 3 sum FP1 y FP2: " << myTrie4->maxFootPrint + myTrie5->maxFootPrint << endl;
		cout << "Case 3 diff FP/(maxFP1+maxFP2): " << (1.0*metric3.second / (myTrie4->maxFootPrint + myTrie5->maxFootPrint)) << endl;
		cout << "Case 3 TFP: " << myTrie4->totalFootPrint << "=? " << totalFootPrint4 << " TFP2: " << myTrie5->totalFootPrint  << "=? " << totalFootPrint5 << endl;
		cout << "Case 3 sum TFP1 y TFP2: " << myTrie4->totalFootPrint + myTrie5->totalFootPrint << endl;
		cout << "Case 3 diff FP/(TFP1+TFP2): " << (1.0*metric3.second / ( myTrie4->totalFootPrint + myTrie5->totalFootPrint)) << endl;
		string line =  to_string(patronsSet3.size() - intersectionSize) + "," + to_string(res3);
		writeLineInFile("/home/naciraa/Documents/tesis/tesiskapow/codigo/custom-cases/results-increase-diff.csv", line );
	
	
	}
	
	
	
	cout << "-------------------------------------"<< endl;
	
	//case 4 - set A and set B  with intersection  
	cout << "case 4 - Set A and set B  with intersection: " << endl;
	trie * myTrie6 = new trie();
	trie * myTrie7 = new trie();
	
	myTrie6->totalFootPrint = 0;
	myTrie7->totalFootPrint = 0;
	
	set<string> patronsSet4;
	createTrieOfSmr(patronsSet, myTrie6);
	createTrieOfSmr(patronsSet2, myTrie7);
	for (int i = 1; i < 10000; i++)
	{
		createSetWithSymbolsNoOcurrencies(patronsSet4, symbols3, maxdepth, i);
		cout << "intersection size: " << patronsSet4.size() << endl;
		cout << "difference size: " << patronsSet.size() + patronsSet2.size() << endl;
		cout << "difference size TrieA: " << patronsSet.size() << endl;
		cout << "difference size TrieB: " << patronsSet2.size() << endl;
		createTrieOfSmr(patronsSet4, myTrie6);
		createTrieOfSmr(patronsSet4, myTrie7);
		int totalFootPrintI4 = 0;
		for (set<string>::iterator it=patronsSet4.begin(); it!=patronsSet4.end(); ++it){
			totalFootPrintI4 += (*it).size();
		}
		
		pair<int,int> metric4 = compare2TriesWithFP(myTrie6, myTrie7);
		double res4 =  (1.0*metric4.first / (countTree(myTrie6->ini)-1 + countTree(myTrie7->ini)-1));
		cout << "Case 4 result: " << res4 << endl;
		cout << "Case 4 diff FP: " << metric4.second << " maxFP1: "<< myTrie6->maxFootPrint << " maxFP2: " << myTrie7->maxFootPrint << endl;
		cout << "Case 4 sum FP1 y FP2: " << myTrie6->maxFootPrint + myTrie7->maxFootPrint << endl;
		cout << "Case 4 diff FP/(maxFP1+maxFP2): " << (1.0*metric4.second / (myTrie6->maxFootPrint + myTrie7->maxFootPrint)) << endl;
		cout << "Case 4 TFP: " << myTrie6->totalFootPrint << "=? " << totalFootPrint1+totalFootPrintI4 << " TFP2: " << myTrie7->totalFootPrint  << "=? " << totalFootPrint2+totalFootPrintI4 << endl;
		cout << "Case 4 sum TFP1 y TFP2: " << myTrie6->totalFootPrint + myTrie7->totalFootPrint << endl;
		cout << "Case 4 diff FP/(TFP1+TFP2): " << (1.0*metric4.second / ( myTrie6->totalFootPrint + myTrie7->totalFootPrint)) << endl;
		string line =  to_string(patronsSet4.size()) + "," + to_string(res4);
		writeLineInFile("/home/naciraa/Documents/tesis/tesiskapow/codigo/custom-cases/results-increase-intersection.csv", line );
	
	}
	cout << "-------------------------------------"<< endl;
	
	//case 5 - Disjoints sets with occurrencies
	cout << "case 5 - Disjoints sets with occurrencies: " << endl;
	trie * myTrie8 = new trie();
	trie * myTrie9 = new trie();
	myTrie8->totalFootPrint = 0;
	myTrie9->totalFootPrint = 0;
	
	set<string> patronsSet8;
	set<string> patronsSet9;
	
	createSetWithSymbolsNoOcurrencies(patronsSet8, symbols, maxdepth, seed);
	createTrieWithSetAndOcurrencies(patronsSet8, myTrie8, seed);

	createSetWithSymbolsNoOcurrencies(patronsSet9, symbols2, maxdepth2, seed2);
	createTrieWithSetAndOcurrencies(patronsSet9, myTrie9, seed2);
	
	pair<int,int> metric5 = compare2TriesWithFP(myTrie8, myTrie9);
	double res5 =  (1.0*metric5.first / (countTree(myTrie8->ini)-1 + countTree(myTrie9->ini)-1));
	cout << "Case 5 result: " << res5 << endl;
	cout << "Case 5 diff FP: " << metric5.second << " maxFP1: "<< myTrie8->maxFootPrint << " maxFP2: " << myTrie9->maxFootPrint << endl;
	cout << "Case 5 sum FP1 y FP2: " << myTrie8->maxFootPrint + myTrie9->maxFootPrint << endl;
	cout << "Case 5 diff FP/(maxFP1+maxFP2): " << (1.0*metric5.second / (myTrie8->maxFootPrint + myTrie9->maxFootPrint)) << endl;
	cout << "Case 5 TFP: " << myTrie8->totalFootPrint << " TFP2: " << myTrie9->totalFootPrint  << endl;
	cout << "Case 5 sum TFP1 y TFP2: " << myTrie8->totalFootPrint + myTrie9->totalFootPrint << endl;
	cout << "Case 5 diff FP/(TFP1+TFP2): " << (1.0*metric5.second / ( myTrie8->totalFootPrint + myTrie9->totalFootPrint)) << endl;
	
	cout << "-------------------------------------"<< endl;
	//case 6 - Equal sets with diff occurrencies
	cout << "case 6 - Equal sets swith occurrencies: " << endl;
	trie * myTrie10 = new trie();
	myTrie10->totalFootPrint = 0;
	
	createTrieWithSetAndOcurrencies(patronsSet8, myTrie10, seed2);
	
	pair<int,int> metric6 = compare2TriesWithFP(myTrie8, myTrie10);
	double res6 =  (1.0*metric6.first / (countTree(myTrie8->ini)-1 + countTree(myTrie10->ini)-1));
	cout << "Case 6 result: " << res6 << endl;
	cout << "Case 6 diff FP: " << metric6.second << " maxFP1: "<< myTrie8->maxFootPrint << " maxFP2: " << myTrie10->maxFootPrint << endl;
	cout << "Case 6 sum FP1 y FP2: " << myTrie8->maxFootPrint + myTrie10->maxFootPrint << endl;
	cout << "Case 6 diff FP/(maxFP1+maxFP2): " << (1.0*metric6.second / (myTrie8->maxFootPrint + myTrie10->maxFootPrint)) << endl;
	cout << "Case 6 TFP: " << myTrie8->totalFootPrint << " TFP2: " << myTrie10->totalFootPrint  << endl;
	cout << "Case 6 sum TFP1 y TFP2: " << myTrie8->totalFootPrint + myTrie10->totalFootPrint << endl;
	cout << "Case 6 diff FP/(TFP1+TFP2): " << (1.0*metric6.second / ( myTrie8->totalFootPrint + myTrie10->totalFootPrint)) << endl;
		
	cout << "-------------------------------------"<< endl;
	
}

void mainCustomCasesByLevel(){
	//case 1 - Disjoints sets
	cout << "case 1 - Disjoints sets: " << endl;
	trie * myTrie1 = new trie();
	trie * myTrie2 = new trie();
	myTrie1->totalFootPrint = 0;
	myTrie2->totalFootPrint = 0;
	set<string> patronsSet;
	set<string> patronsSet2;
	string symbols = "abcdefg";
	string symbols2 = "jklmnop";
	string symbols3 = "tuvwxyz";
	string symbols4 = "hiqrs";
	int maxdepth = 4;
	int maxdepth2 = 3;
	int seed = 1;
	int seed2 = 20;
	
	createSetWithSymbolsNoOcurrencies(patronsSet, symbols, maxdepth, seed);
	createTrieOfSmr(patronsSet, myTrie1);
	int totalFootPrint1 = 0;
	cout << " patronsSet:" ;
	for (set<string>::iterator it=patronsSet.begin(); it!=patronsSet.end(); ++it){
		cout << ' ' << *it;
		totalFootPrint1 += (*it).size();
	}
    cout << endl;
	createSetWithSymbolsNoOcurrencies(patronsSet2, symbols2, maxdepth2, seed2);
	createTrieOfSmr(patronsSet2, myTrie2);
	cout << " patronsSet2:" ;
	int totalFootPrint2 = 0;
	for (set<string>::iterator it=patronsSet2.begin(); it!=patronsSet2.end(); ++it){
		cout << ' ' << *it;
		totalFootPrint2 += (*it).size();
	}
	cout << endl;
	
	pair<int,int> metric = compare2TriesWithFP(myTrie1, myTrie2);
	pair<int,int> metricL = compare2TriesByLevel(myTrie1, myTrie2, 0, 3);
	double res =  (1.0*metric.first / (countTree(myTrie1->ini)-1 + countTree(myTrie2->ini)-1));
	double resL =  (1.0*metricL.first / (countTree(myTrie1->ini)-1 + countTree(myTrie2->ini)-1));
	cout << "Case 1 result: " << res << endl;
	cout << "Case 1 resultL: " << resL << endl;
	cout << "Case 1 diff FP: " << metric.second << " diffL FP: " << metricL.second  << " maxFP1: "<< myTrie1->maxFootPrint << " maxFP2: " << myTrie2->maxFootPrint << endl;
	//~ cout << "Case 1 TFP: " << myTrie1->totalFootPrint << "=? " << totalFootPrint1 << " => " << myTrie1->totalFootPrint == totalFootPrint1 << " TFP2: " << myTrie2->totalFootPrint  << "=? " << totalFootPrint2 << " => " << myTrie2->totalFootPrint == totalFootPrint2 << << endl;
	cout << "Case 1 TFP: " << myTrie1->totalFootPrint << "=? " << totalFootPrint1 << " TFP2: " << myTrie2->totalFootPrint  << "=? " << totalFootPrint2 << endl;
	cout << "Case 1 sum FP1 y FP2: " << myTrie1->maxFootPrint + myTrie2->maxFootPrint << endl;
	cout << "Case 1 sum TFP1 y TFP2: " << myTrie1->totalFootPrint + myTrie2->totalFootPrint << endl;
	cout << "Case 1 diff FP/(maxFP1+maxFP2): " << (1.0*metric.second / (myTrie1->maxFootPrint + myTrie2->maxFootPrint)) << endl;
	cout << "Case 1 diff FP/(TFP1+TFP2): " << (1.0*metric.second / ( myTrie1->totalFootPrint + myTrie2->totalFootPrint)) << endl;
	cout << "Case 1 diff FPL/(TFP1+TFP2): " << (1.0*metricL.second / ( myTrie1->totalFootPrint + myTrie2->totalFootPrint)) << endl;
	
	cout << "-------------------------------------"<< endl;
	//~ //case 2 - Equal sets
	//~ cout << "case 2 - Equal sets: " << endl;
	//~ trie * myTrie3 = new trie();
	
	//~ createSetWithSymbolsNoOcurrencies(patronsSet, symbols, maxdepth, seed);
	//~ createTrieOfSmr(patronsSet, myTrie3);
	//createTireWithSetAndOcurrencies(patronsSet, myTrie1);
	
	//~ pair<int,int> metric2 = compare2TriesWithFP(myTrie1, myTrie3);
	//~ double res2 =  (1.0*metric2.first / (countTree(myTrie1->ini)-1 + countTree(myTrie3->ini)-1));
	//~ cout << "Case 2 result: " << res2 << endl;
	//~ cout << "Case 2 diff FP: " << metric2.second << " maxFP1: "<< myTrie1->maxFootPrint << " maxFP2: " << myTrie3->maxFootPrint << endl;
	//~ cout << "Case 2 sum FP1 y FP2: " << myTrie1->maxFootPrint + myTrie3->maxFootPrint << endl;
	//~ cout << "Case 2 diff FP/(maxFP1+maxFP2): " << (1.0*metric2.second / (myTrie1->maxFootPrint + myTrie3->maxFootPrint)) << endl;
	//~ cout << "Case 2 TFP: " << myTrie1->totalFootPrint << "=? " << totalFootPrint1 << " TFP2: " << myTrie3->totalFootPrint  << "=? " << totalFootPrint1 << endl;
	//~ cout << "Case 2 sum TFP1 y TFP2: " << myTrie1->totalFootPrint + myTrie3->totalFootPrint << endl;
	//~ cout << "Case 2 diff FP/(TFP1+TFP2): " << (1.0*metric2.second / ( myTrie1->totalFootPrint + myTrie3->totalFootPrint)) << endl;
		
	//~ cout << "-------------------------------------"<< endl;
	
	//~ //case 3 - Set A included in B 
	//~ cout << "case 3 - Set A included in B: " << endl;
	//~ trie * myTrie4 = new trie();
	//~ myTrie4->totalFootPrint = 0;
	
	//~ set<string> patronsSet3;
	//~ int maxdepth3 = 6;
	
	//~ createSetWithSymbolsNoOcurrencies(patronsSet3, symbols, maxdepth, seed);
	//~ int totalFootPrint4 = 0;
	//~ for (set<string>::iterator it=patronsSet3.begin(); it!=patronsSet3.end(); ++it){
		//~ totalFootPrint4 += (*it).size();
	//~ }
	//~ createTrieOfSmr(patronsSet3, myTrie4);
	
	//~ int intersectionSize = patronsSet3.size() ;

	//~ cout << "intersection size: " << intersectionSize << endl;
	
	//~ for (int i = 1; i < 11; i++)
	//~ {
		//~ createSetWithSymbolsNoOcurrencies(patronsSet3, symbols2, maxdepth3, i);
		//~ cout << "difference size: " << patronsSet3.size() - intersectionSize << endl;
		
		//~ trie * myTrie5 = new trie();
		//~ myTrie5->totalFootPrint = 0;
		//~ createTrieOfSmr(patronsSet3, myTrie5);
		
		//~ int totalFootPrint5 = 0;
		//~ for (set<string>::iterator it=patronsSet3.begin(); it!=patronsSet3.end(); ++it){
			//~ totalFootPrint5 += (*it).size();
		//~ }
		
		//~ pair<int,int> metric3 = compare2TriesWithFP(myTrie4, myTrie5);
		//~ double res3 =  (1.0*metric3.first / (countTree(myTrie4->ini)-1 + countTree(myTrie5->ini)-1));
		//~ cout << "Case 3 result: " << res3 << endl;
		//~ cout << "Case 3 diff FP: " << metric3.second << " maxFP1: "<< myTrie4->maxFootPrint << " maxFP2: " << myTrie5->maxFootPrint << endl;
		//~ cout << "Case 3 sum FP1 y FP2: " << myTrie4->maxFootPrint + myTrie5->maxFootPrint << endl;
		//~ cout << "Case 3 diff FP/(maxFP1+maxFP2): " << (1.0*metric3.second / (myTrie4->maxFootPrint + myTrie5->maxFootPrint)) << endl;
		//~ cout << "Case 3 TFP: " << myTrie4->totalFootPrint << "=? " << totalFootPrint4 << " TFP2: " << myTrie5->totalFootPrint  << "=? " << totalFootPrint5 << endl;
		//~ cout << "Case 3 sum TFP1 y TFP2: " << myTrie4->totalFootPrint + myTrie5->totalFootPrint << endl;
		//~ cout << "Case 3 diff FP/(TFP1+TFP2): " << (1.0*metric3.second / ( myTrie4->totalFootPrint + myTrie5->totalFootPrint)) << endl;
	
	
	//~ }
	
	
	
	//~ cout << "-------------------------------------"<< endl;
	
	//~ //case 4 - set A and set B  with intersection  
	//~ cout << "case 4 - Set A and set B  with intersection: " << endl;
	//~ trie * myTrie6 = new trie();
	//~ trie * myTrie7 = new trie();
	
	//~ myTrie6->totalFootPrint = 0;
	//~ myTrie7->totalFootPrint = 0;
	
	//~ set<string> patronsSet4;
	//~ createTrieOfSmr(patronsSet, myTrie6);
	//~ createTrieOfSmr(patronsSet2, myTrie7);
	//~ for (int i = 1; i < 11; i++)
	//~ {
		//~ createSetWithSymbolsNoOcurrencies(patronsSet4, symbols3, maxdepth, i);
		//~ cout << "intersection size: " << patronsSet4.size() << endl;
		//~ cout << "difference size: " << patronsSet.size() + patronsSet2.size() << endl;
		//~ createTrieOfSmr(patronsSet4, myTrie6);
		//~ createTrieOfSmr(patronsSet4, myTrie7);
		//~ int totalFootPrintI4 = 0;
		//~ for (set<string>::iterator it=patronsSet4.begin(); it!=patronsSet4.end(); ++it){
			//~ totalFootPrintI4 += (*it).size();
		//~ }
		
		//~ pair<int,int> metric4 = compare2TriesWithFP(myTrie6, myTrie7);
		//~ double res4 =  (1.0*metric4.first / (countTree(myTrie6->ini)-1 + countTree(myTrie7->ini)-1));
		//~ cout << "Case 4 result: " << res4 << endl;
		//~ cout << "Case 4 diff FP: " << metric4.second << " maxFP1: "<< myTrie6->maxFootPrint << " maxFP2: " << myTrie7->maxFootPrint << endl;
		//~ cout << "Case 4 sum FP1 y FP2: " << myTrie6->maxFootPrint + myTrie7->maxFootPrint << endl;
		//~ cout << "Case 4 diff FP/(maxFP1+maxFP2): " << (1.0*metric4.second / (myTrie6->maxFootPrint + myTrie7->maxFootPrint)) << endl;
		//~ cout << "Case 4 TFP: " << myTrie6->totalFootPrint << "=? " << totalFootPrint1+totalFootPrintI4 << " TFP2: " << myTrie7->totalFootPrint  << "=? " << totalFootPrint2+totalFootPrintI4 << endl;
		//~ cout << "Case 4 sum TFP1 y TFP2: " << myTrie6->totalFootPrint + myTrie7->totalFootPrint << endl;
		//~ cout << "Case 4 diff FP/(TFP1+TFP2): " << (1.0*metric4.second / ( myTrie6->totalFootPrint + myTrie7->totalFootPrint)) << endl;
	
	//~ }
	//~ cout << "-------------------------------------"<< endl;
	
	//~ //case 5 - Disjoints sets with occurrencies
	//~ cout << "case 5 - Disjoints sets with occurrencies: " << endl;
	//~ trie * myTrie8 = new trie();
	//~ trie * myTrie9 = new trie();
	//~ myTrie8->totalFootPrint = 0;
	//~ myTrie9->totalFootPrint = 0;
	
	//~ set<string> patronsSet8;
	//~ set<string> patronsSet9;
	
	//~ createSetWithSymbolsNoOcurrencies(patronsSet8, symbols, maxdepth, seed);
	//~ createTrieWithSetAndOcurrencies(patronsSet8, myTrie8, seed);

	//~ createSetWithSymbolsNoOcurrencies(patronsSet9, symbols2, maxdepth2, seed2);
	//~ createTrieWithSetAndOcurrencies(patronsSet9, myTrie9, seed2);
	
	//~ pair<int,int> metric5 = compare2TriesWithFP(myTrie8, myTrie9);
	//~ double res5 =  (1.0*metric5.first / (countTree(myTrie8->ini)-1 + countTree(myTrie9->ini)-1));
	//~ cout << "Case 5 result: " << res5 << endl;
	//~ cout << "Case 5 diff FP: " << metric5.second << " maxFP1: "<< myTrie8->maxFootPrint << " maxFP2: " << myTrie9->maxFootPrint << endl;
	//~ cout << "Case 5 sum FP1 y FP2: " << myTrie8->maxFootPrint + myTrie9->maxFootPrint << endl;
	//~ cout << "Case 5 diff FP/(maxFP1+maxFP2): " << (1.0*metric5.second / (myTrie8->maxFootPrint + myTrie9->maxFootPrint)) << endl;
	//~ cout << "Case 5 TFP: " << myTrie8->totalFootPrint << " TFP2: " << myTrie9->totalFootPrint  << endl;
	//~ cout << "Case 5 sum TFP1 y TFP2: " << myTrie8->totalFootPrint + myTrie9->totalFootPrint << endl;
	//~ cout << "Case 5 diff FP/(TFP1+TFP2): " << (1.0*metric5.second / ( myTrie8->totalFootPrint + myTrie9->totalFootPrint)) << endl;
	
	//~ cout << "-------------------------------------"<< endl;
	//~ //case 6 - Equal sets with diff occurrencies
	//~ cout << "case 6 - Equal sets swith occurrencies: " << endl;
	//~ trie * myTrie10 = new trie();
	//~ myTrie10->totalFootPrint = 0;
	
	//~ createTrieWithSetAndOcurrencies(patronsSet8, myTrie10, seed2);
	
	//~ pair<int,int> metric6 = compare2TriesWithFP(myTrie8, myTrie10);
	//~ double res6 =  (1.0*metric6.first / (countTree(myTrie8->ini)-1 + countTree(myTrie10->ini)-1));
	//~ cout << "Case 6 result: " << res6 << endl;
	//~ cout << "Case 6 diff FP: " << metric6.second << " maxFP1: "<< myTrie8->maxFootPrint << " maxFP2: " << myTrie10->maxFootPrint << endl;
	//~ cout << "Case 6 sum FP1 y FP2: " << myTrie8->maxFootPrint + myTrie10->maxFootPrint << endl;
	//~ cout << "Case 6 diff FP/(maxFP1+maxFP2): " << (1.0*metric6.second / (myTrie8->maxFootPrint + myTrie10->maxFootPrint)) << endl;
	//~ cout << "Case 6 TFP: " << myTrie8->totalFootPrint << " TFP2: " << myTrie10->totalFootPrint  << endl;
	//~ cout << "Case 6 sum TFP1 y TFP2: " << myTrie8->totalFootPrint + myTrie10->totalFootPrint << endl;
	//~ cout << "Case 6 diff FP/(TFP1+TFP2): " << (1.0*metric6.second / ( myTrie8->totalFootPrint + myTrie10->totalFootPrint)) << endl;
		
	//~ cout << "-------------------------------------"<< endl;
	
}

void processAffectedProteins(vector< string> &pid, vector< int > &sa, string fileName){
	set< string > result;
	ifstream myfile(fileName);
	cout << "processing file: " << fileName << endl;
	//~ ifstream myfile("MR_NE_AP_ABCtran.csv");
	if (!myfile.good()) {
		cerr << "Error opening: " << fileName << " . You have failed." << endl;
	} 
	string ne;
	int ini, fin, l;
	map<string, pair<int,int>> toProcess;
	while (myfile >> ne) {
		myfile >> ini;
		myfile >> fin;
		myfile >> l;
		pair<int,int> aux = make_pair(ini, l);
		toProcess[ne] = aux;
	}
	myfile.close();
	cout << "finish reading file: " << fileName << endl;
	cout << "map size: " << toProcess.size() << endl;
	
	for (map<string, pair<int,int>>::iterator it=toProcess.begin(); it!=toProcess.end(); ++it){
		//~ cout << it->first << " => " << it->second.first << " , " << it->second.second << '\n';
		unsigned int cant = affectedProteins(it->second.first, it->second.second, sa, pid);
		string auxRes = "";
		auxRes = proteina.substr(sa[it->second.first],it->second.second) + " " + to_string(cant);
		//~ writeResultsHeatMap(fileName + ".out", auxRes);
		writeLineInFile(fileName + ".out", auxRes);
		//~ cout << auxRes <<endl;	
	}
	cout << "finish processing file: " << fileName << endl;

}


bool myComp(int i, int j){
	return proteina.substr(i) < proteina.substr(j);
}

void calculateSAandLCP(string &s, vector< string > &sa, vector< int > &lcp) {
	for(int i=0; i<s.length(); ++i){
		sa.push_back(s.substr(i));
	}
	sort(sa.begin(), sa.end());
	lcp.push_back(0);
	for(int i=0; i<sa.size()-1; ++i){
		int aux_min_len = min(sa[i].length(), sa[i+1].length());
		int aux_lcp = 0;
		for(int j=0; j<aux_min_len; ++j){
			if(sa[i][j] == sa[i+1][j]){
				aux_lcp++;
			} else {
				break;
			}
		}
		lcp.push_back(aux_lcp);
	}
}

void calculateSAIandLCP(string &s, vector< int > &sai, vector< int > &lcp) {
	alfabeto.clear();
	for(int i=0; i<s.length()-1; ++i){
		sai.push_back(i);
		alfabeto.insert(s[i]);
	}
	sort(sai.begin(), sai.end(), myComp);
	lcp.push_back(0);
	for(int i=0; i<sai.size()-1; ++i){
		int aux_min_len = min(s.substr(sai[i]).length(), s.substr(sai[i+1]).length());
		int aux_lcp = 0;
		//~ cout << "aux_min_len: " << aux_min_len << endl;
		//~ cout << s.substr(sai[i]) <<  " " << s.substr(sai[i+1])<<endl;
		for(int j=0; j<aux_min_len; ++j){
			if(s.substr(sai[i])[j] == s.substr(sai[i+1])[j]){
				aux_lcp++;
			} else {
				break;
			}
		}
		lcp.push_back(aux_lcp);
	}
}

bool myCompForSet(int i, int j){
	//~ if (proteina.substr(i)[0] == '+' && proteina.substr(j)[0] == '+') {
		//~ return false;
	//~ }
	return proteina.substr(i) < proteina.substr(j);
}

void calculateSAIandLCPForSet(string &s, vector< int > &sai, vector< int > &lcp) {
	alfabeto.clear();
	for(int i=0; i<s.length()-1; ++i){
		sai.push_back(i);
		alfabeto.insert(s[i]);
	}
	sort(sai.begin(), sai.end(), myCompForSet);
	lcp.push_back(0);
	for(int i=0; i<sai.size()-1; ++i){
		int aux_min_len = min(s.substr(sai[i]).length(), s.substr(sai[i+1]).length());
		int aux_lcp = 0;
		//~ cout << "aux_min_len: " << aux_min_len << endl;
		//~ cout << s.substr(sai[i]) <<  " " << s.substr(sai[i+1])<<endl;
		for(int j=0; j<aux_min_len; ++j){
			if(s.substr(sai[i])[j] == '{' && s.substr(sai[i+1])[j] == '{'){					
				aux_lcp = 0;
				break;
			}
			if(s.substr(sai[i])[j] == s.substr(sai[i+1])[j]){
				aux_lcp++;
			} else {
				break;
			}
		}
		lcp.push_back(aux_lcp);
	}
}

void calculateSAIandLCPForSetWithStruct(string &s, vector< int > &sai, vector< int > &lcp) {
	alfabeto.clear();
	for(int i=0; i<s.length()-1; ++i){
		alfabeto.insert(s[i]);
	}
	cout << "armando suffix array " << endl;
	SuffixArray suffix(s);
	sai = suffix.GetMiSuffix();
	cout << "suffix array listo, generando lcp.. " << endl;
	lcp.push_back(0);
	for(int i=0; i<sai.size()-1; ++i){
		//~ int aux_min_len = min(s.substr(sai[i]).length(), s.substr(sai[i+1]).length());
		int aux_min_len = min(s.size()-sai[i], s.size()-sai[i+1]);
		int aux_lcp = 0;
		//~ cout << "aux_min_len: " << aux_min_len << endl;
		//~ cout << s.substr(sai[i]) <<  " " << s.substr(sai[i+1])<<endl;
		for(int j=0; j<aux_min_len; ++j){
			if(s[sai[i] + j] == '{' && s[sai[i+1] + j] == '{'){					
				//~ aux_lcp = 0;
				break;
			}
			if(s[sai[i] +j] == s[sai[i+1] + j]){
				aux_lcp++;
			} else {
				break;
			}
		}
		lcp.push_back(aux_lcp);
	}
}


void findIntervals(vector< int > &sai, vector< int > &lcp, vector< pair< int, pair< int, int > > > &intervals){
	//~ cout << "proces Intervals" << endl;
	int fin, inicio;
	for(int len=1; len<proteina.length(); ++len){
		//~ cout << "len: " << len << endl;
		fin = -1;
		for(int i=0; i<lcp.size()-1; ++i){
			if (lcp[i] == len && lcp[i+1] < len) {
				//~ cout << "fin " << i << endl;
				fin = i;
				inicio = -1;
				bool isInterval = true;
				for(int j=0; j<fin; ++j){
					bool isPrefix = true;
					for(int k=0; k<len; ++k){
						//~ cout << "j: " << j << " k: " << k << " " << proteina[sai[j]+k] << " ? " << proteina[sai[fin]+k] <<endl;
						if (proteina[sai[j]+k] != proteina[sai[fin]+k]){
							isPrefix = false;
							break;
						}
					}
					//~ cout << "j: " << j << " isPrefix: " << isPrefix << " lcp[j]: " << lcp[j] << endl;
					if (isPrefix && lcp[j] < len){
						inicio = j;
					}
					//~ cout << "inicio: " << inicio << endl;
					if (inicio != -1) {
						if (inicio < j){
							if(lcp[j] < len) {
								isInterval = false;
							}
						}
					} else {
						isInterval = true;
					}
				}
				
				if (inicio != -1 && fin != -1 && isInterval) {
					//~ cout << "tengo par" << endl;
					//~ cout << "len: " << len << " inicio: " << inicio << " , fin: " << fin <<endl;
					pair< int, int > interval = make_pair (inicio, fin);
					pair<int, pair< int, int > > auxInterval = make_pair(len, interval);
					intervals.push_back(auxInterval);
				}
				
			}
		}
		
		
	}
	
}

void findIntervalsPaper(vector< int > &sai, vector< int > &lcp, vector< pair< int, pair< int, int > > > &intervals){
	stack<pair<int, pair<int, int>>> mystack;
	mystack.push(make_pair(0, make_pair(0, -1)));
	for(int i=1; i<lcp.size(); ++i){
		int leftBound = i-1;
		while(lcp[i] < mystack.top().first){
			mystack.top().second.second = i-1;
			pair<int, pair<int, int>> interval = mystack.top();
			mystack.pop();
			//~ cout << "report interval: lcp=" << interval.first << " lb=" << interval.second.first << " rb=" << interval.second.second <<endl; 
			intervals.push_back(interval);
			leftBound = interval.second.first;
		}
		if (lcp[i] > mystack.top().first){
			mystack.push(make_pair(lcp[i], make_pair(leftBound, -1)));
		}
		
	}
	
}

void processIntervals(vector< pair< int, pair< int, int > > > &intervals,
					vector< int > &sa,
					vector< int > &lcp){
					//~ vector< int > &lcp,
					//~ vector< pair< int, pair< int, int > > > &super,
					//~ vector< pair< int, pair< int, int > > > &local,
					//~ vector< int > &nested,
					//~ vector< int > &notNested){
	
	for(int i=0; i<intervals.size(); ++i){
		unordered_map < char, int > lctt;
		//~ vector < int > lctt (300, 0);
		unordered_map < char, int > rctt;
		//~ vector < int > rctt (300, 0);
		bool tmm = false;
		int ini = intervals[i].second.first;
		int fin = intervals[i].second.second;
		int l = intervals[i].first;
		char sigma = proteina[sa[ini]-1];
		//~ cout << "ini: " << ini << " fin: " << fin << " l: " << l << " sa[ini]: " << proteina.substr(sa[ini], l) << " sigma: " << sigma << endl;
		for(int k=ini; k<=fin; ++k){
			char sigmaLeft = proteina[sa[k]-1];
			char sigmaRight = proteina[sa[k]+l];
			//~ lctt[sigmaLeft] = lctt[sigmaLeft] + 1;
			//~ rctt[sigmaRight] = rctt[sigmaRight] +1;
			lctt[sigmaLeft]++;
			rctt[sigmaRight]++;
			if (sigma != sigmaLeft || sigma == '{') {
				tmm = true;
			}
			//~ cout << "k: " << k << " sa[k] " << sa[k] << " sigmaleft: " << sigmaLeft << " sigmaRight: " << sigmaRight << " lctt[sigmaLeft]: " << lctt[sigmaLeft] << " rctt[sigmaRight]: " << rctt[sigmaRight] << " tmm " << tmm <<endl;
		}
		lctt['{'] = 1;
		rctt['{'] = 1;
		//~ if (fin-ini+1 <= alfabeto.size()) {
			bool aux = true;
			unordered_set < char >::iterator it;
			for (it=alfabeto.begin(); it!=alfabeto.end(); ++it){
				if (lctt[*it] > 1 || rctt[*it] > 1){
					aux = false;
					break;
				}
			}
			//~ for(int i; i<lenAlfabeto; ++i){
				//~ if (lctt[alfabeto[i]] > 1 || rctt[alfabeto[i]] > 1){
					//~ aux = false;
					//~ break;
				//~ }
			//~ }
			if (aux){
				//~ cout << "super: " << proteina.substr(ini,fin-ini) << endl;
				//~ cout << "super: " << proteina.substr(ini,l) << endl;
				//~ pair< int, int > interval = make_pair (ini, fin);
				//~ pair<int, pair< int, int > > auxInterval = make_pair(l, interval);
				//~ super.push_back(auxInterval);
				processSuper(proteina.substr(sa[ini],l));
			} else {
		//~ }
				if (tmm == true) {
					//~ pair< int, int > interval = make_pair (ini, fin);
					//~ pair<int, pair< int, int > > auxInterval = make_pair(l, interval);
					//~ local.push_back(auxInterval);
					bool isNested = true;
					for(int k=ini; k<=fin; ++k){
						char sigmaLeft = proteina[sa[k]-1];
						char sigmaRight = proteina[sa[k]+l];
						//imprimir en el archivo desde k hasta k+l-1 inclusive
						//~ if (lctt[sigmaLeft] > 1 || rctt[sigmaRight] > 1) {
							//~ cout << "k: " << k << " nested: "<< proteina.substr(k,l) <<endl;
							//~ nested.push_back(k);
							//~ processNested(proteina.substr(sa[k],l));
						//~ } else {
							//~ cout << "k: " << k << " notNested: " << proteina.substr(k,l) <<endl;
							//~ notNested.push_back(k);
							//~ processNonNested(proteina.substr(sa[k],l));
						//~ }
						if (lctt[sigmaLeft] <= 1 && rctt[sigmaRight] <= 1){
							isNested = false;
						}
					}
					if (isNested) {
						processNested(proteina.substr(sa[ini],l));
					} else {
						processNonNested(proteina.substr(sa[ini],l));
					}	
				}
			}
		
		
	}
	
}


void processIntervalsWithAffectedProteins(vector< pair< int, pair< int, int > > > &intervals,
					vector< int > &sa,
					vector< int > &lcp){
					//~ vector< int > &lcp,
					//~ vector< pair< int, pair< int, int > > > &super,
					//~ vector< pair< int, pair< int, int > > > &local,
					//~ vector< int > &nested,
					//~ vector< int > &notNested){
	
	for(int i=0; i<intervals.size(); ++i){
		unordered_map < char, int > lctt;
		//~ vector < int > lctt (300, 0);
		unordered_map < char, int > rctt;
		//~ vector < int > rctt (300, 0);
		bool tmm = false;
		int ini = intervals[i].second.first;
		int fin = intervals[i].second.second;
		int l = intervals[i].first;
		char sigma = proteina[sa[ini]-1];
		//~ cout << "ini: " << ini << " fin: " << fin << " l: " << l << " sa[ini]: " << proteina.substr(sa[ini], l) << " sigma: " << sigma << endl;
		for(int k=ini; k<=fin; ++k){
			char sigmaLeft = proteina[sa[k]-1];
			char sigmaRight = proteina[sa[k]+l];
			//~ lctt[sigmaLeft] = lctt[sigmaLeft] + 1;
			//~ rctt[sigmaRight] = rctt[sigmaRight] +1;
			lctt[sigmaLeft]++;
			rctt[sigmaRight]++;
			if (sigma != sigmaLeft || sigma == '{') {
				tmm = true;
			}
			//~ cout << "k: " << k << " sa[k] " << sa[k] << " sigmaleft: " << sigmaLeft << " sigmaRight: " << sigmaRight << " lctt[sigmaLeft]: " << lctt[sigmaLeft] << " rctt[sigmaRight]: " << rctt[sigmaRight] << " tmm " << tmm <<endl;
		}
		lctt['{'] = 1;
		rctt['{'] = 1;
		//~ if (fin-ini+1 <= alfabeto.size()) {
			bool aux = true;
			unordered_set < char >::iterator it;
			for (it=alfabeto.begin(); it!=alfabeto.end(); ++it){
				if (lctt[*it] > 1 || rctt[*it] > 1){
					aux = false;
					break;
				}
			}
			//~ for(int i; i<lenAlfabeto; ++i){
				//~ if (lctt[alfabeto[i]] > 1 || rctt[alfabeto[i]] > 1){
					//~ aux = false;
					//~ break;
				//~ }
			//~ }
			if (aux){
				//~ cout << "super: " << proteina.substr(ini,fin-ini) << endl;
				//~ cout << "super: " << proteina.substr(ini,l) << endl;
				//~ pair< int, int > interval = make_pair (ini, fin);
				//~ pair<int, pair< int, int > > auxInterval = make_pair(l, interval);
				//~ super.push_back(auxInterval);
				//~ unsigned int cant = affectedProteins(ini, l, sa, pid);
				//~ string auxRes = proteina.substr(sa[ini],l) + " , " + to_string(cant);		
				string auxRes = proteina.substr(sa[ini],l) + " " + to_string(ini) + " " + to_string(fin) + " " + to_string(l);
				processSuper(auxRes);
			} else {
		//~ }
				if (tmm == true) {
					//~ pair< int, int > interval = make_pair (ini, fin);
					//~ pair<int, pair< int, int > > auxInterval = make_pair(l, interval);
					//~ local.push_back(auxInterval);
					bool isNested = true;
					for(int k=ini; k<=fin; ++k){
						char sigmaLeft = proteina[sa[k]-1];
						char sigmaRight = proteina[sa[k]+l];
						//imprimir en el archivo desde k hasta k+l-1 inclusive
						//~ if (lctt[sigmaLeft] > 1 || rctt[sigmaRight] > 1) {
							//~ cout << "k: " << k << " nested: "<< proteina.substr(k,l) <<endl;
							//~ nested.push_back(k);
							//~ processNested(proteina.substr(sa[k],l));
						//~ } else {
							//~ cout << "k: " << k << " notNested: " << proteina.substr(k,l) <<endl;
							//~ notNested.push_back(k);
							//~ processNonNested(proteina.substr(sa[k],l));
						//~ }
						if (lctt[sigmaLeft] <= 1 && rctt[sigmaRight] <= 1){
							isNested = false;
						}
					}
					if (isNested) {
						//~ unsigned int cant = affectedProteins(ini, l, sa, pid);
						//~ string auxRes = proteina.substr(sa[ini],l) + " , " + to_string(cant);
						string auxRes = proteina.substr(sa[ini],l) + " " + to_string(ini) + " " + to_string(fin) + " " + to_string(l);
						processNested(auxRes);
					} else {
						//~ unsigned int cant = affectedProteins(ini, l, sa, pid);
						//~ string auxRes = proteina.substr(sa[ini],l) + " , " + to_string(cant);
						string auxRes = proteina.substr(sa[ini],l) + " " + to_string(ini) + " " + to_string(fin) + " " + to_string(l);
						processNonNested(auxRes);
					}	
				}
			}
		
		
	}
	
}

void createDicctionaryOfSmrBySetPair(set< pair<string,int> > &smr, vector< set< pair<string,int> > > &dicc){
	set< pair<string,int> >::iterator it;
	cout << "set smr length: " << smr.size() <<endl;
	cout << "set dic length: " << dicc.size() <<endl;
	int contador =0 ;
	for (it=smr.begin(); it!=smr.end(); ++it){
		//~ cout << ' ' << *it << " it-len: " << it->length() << endl;
		int len = it->first.length();
		if(len <= 3)contador++;
		dicc[len].insert(*it);
	}
	cout << "contador: " << contador <<endl;
}

void createDicctionaryOfSmr(set< string > &smr, vector< set< string > > &dicc){
	set<string>::iterator it;
	cout << "set smr length: " << smr.size() <<endl;
	cout << "set dic length: " << dicc.size() <<endl;
	int contador =0 ;
	for (it=smr.begin(); it!=smr.end(); ++it){
		//~ cout << ' ' << *it << " it-len: " << it->length() << endl;
		int len = it->length();
		if(it->length() <= 3)contador++;
		dicc[len].insert(*it);
	}
	cout << "contador: " << contador <<endl;
}

void createTrieOfSmr(set< string > &smr, trie * smrTrie){
	set<string>::iterator it;
	for (it=smr.begin(); it!=smr.end(); ++it){
		//~ cout << "string ne to add to trie: " << *it << endl;
		insertString (smrTrie, *it);
	}
}

void createTrieOfSmrWithFP(set< pair<string,int> > &smr, trie * smrTrie){
	set< pair<string,int> >::iterator it;
	for (it=smr.begin(); it!=smr.end(); ++it){
		//~ cout << "string ne to add to trie: " << *it << endl;
		insertStringWithFP(smrTrie, *it);
	}
	calculateTotalFootPrint(smrTrie);
}


void createTrieOfSmrPref(set< string > &smr, trie * smrTrie){
	set<string>::iterator it;
	for (it=smr.begin(); it!=smr.end(); ++it){
		//~ cout << "string ne to add to trie: " << (*it).substr(0,4) << endl;
		insertString (smrTrie, (*it).substr(0,5));
	}
}


float calculateCoverage(string p, set< string > m){
	vector< bool > cov(p.length(), 0);
	set<string>::iterator it;
	for (it=m.begin(); it!=m.end(); ++it){
		//~ cout << ' ' << *it <<endl;
		size_t pos = p.find(*it, 0);
		while(pos != string::npos) {
			for(int j=0; j<it->length(); ++j){
				cov[pos+j] = 1;
			}
			pos = p.find(*it,pos+1);
		}
	}
	float res = 0;
	for(int i=0; i<cov.size(); ++i){
		res += cov[i];
	}
	cout << "res: " << res << " string-size: " << p.length() << endl;
	cout << "coverage: " << res/p.length() << endl;
	return res/p.length();	
}

float calculateCoverageWriteToFile(string &p, vector< set< string > > &dicc, ofstream &coverageFile){
	vector< bool > cov(p.length(), 0);
	float familiarity = 0.0;
	
	for(int i=10; 0<i; --i){
		set<string>::iterator it;
		
		
		for (it=dicc[i].begin(); it!=dicc[i].end(); ++it){
			//~ cout << ' ' << *it <<endl;
			size_t pos = p.find(*it, 0);
			while(pos != string::npos) {
				for(int j=0; j<it->length(); ++j){
					cov[pos+j] = 1;
				}
				pos = p.find(*it,pos+1);
			}
		}
		
		float res = 0;
		for(int i=0; i<cov.size(); ++i){
			res += cov[i];
		}
		res = res/p.length();
		if(i == p.length()-1){
			familiarity += (1 + res)/2;
		} else {
			familiarity += res;
		}
		
		//~ cout << "writing " << res << " to coverage file"<<endl;
		coverageFile << i << " , " << res << "\n";

		
	}
	coverageFile << 0 << " , " << 1 << "\n";
	
	cout << "familiarity: " << familiarity <<endl;
	return familiarity;
}

float calculateCoverageKMP(string &p, vector< set< string > > &dicc, ofstream &coverageFile){
	vector< int > cov(p.length(), 0);
	float familiarity = 0.0;
	
	for(int i=p.length(); 0<i; --i){
		//~ cout << "current length: " << i << endl;
		
		
		set<string>::iterator it;
		
		
		for (it=dicc[i].begin(); it!=dicc[i].end(); ++it){
			//~ cout << ' ' << *it <<endl;
			KMPSearch(*it, p, cov);
		}
		
		float res = 0;
		int acum = 0;
		for(int i=0; i<cov.size(); ++i){
			//~ cout << "cov[" << i <<"] " << cov[i] << endl;
			acum += cov[i];
			if (acum > 0){
				res += 1;
			}
		}
		//~ cout << "res: " << res << " string-size: " << p.length() << endl;
		//~ cout << "coverage: " << res/p.length() << endl;
		res = res/p.length();
		if(i == p.length()-1){
			familiarity += (1 + res)/2;
		} else {
			familiarity += res;
		}
		
		//~ cout << "writing " << res << " to coverage file"<<endl;
		coverageFile << i << " , " << res << "\n";
		//~ cout << "next"<<endl;
		
	}
	coverageFile << 0 << " , " << 1 << "\n";
	
	cout << "familiarity: " << familiarity <<endl;
	return familiarity;	
}


void calculateCoverageInProteins(){
	string proteinSet = "";
	//~ readWholeProtein(proteinSet);
	proteina = proteinSet;
	vector< string > files = listFile();
	proteinSet 	= concatenateProteins(files);
    //~ cout << proteinSet <<endl;
    //~ proteina = proteinSet;
	//~ vector< int > sai;
	//~ vector< int > lcp;
	//~ cout << "calculando SA y LCP ..." <<endl;
	//~ cout << "longitud alfabeto: " << alfabeto.size() <<endl;
	//~ calculateSAIandLCPForSetWithStruct(proteinSet, sai, lcp);
	//~ cout << "longitud alfabeto: " << alfabeto.size() <<endl;
	//~ vector< pair< int, pair< int, int > > > intervals;
	//~ cout << "buscando intervalos ..." <<endl;
	//~ findIntervalsPaper(sai, lcp, intervals);
	//~ cout << "procesando intervalos ..." <<endl;
	//~ processIntervals(intervals, sai, lcp);
	//~ cout << "calculando diferencia smr ..." <<endl;
	//~ set< string > mySMRSet = processFileToSet("MR_SMR.csv");
	set< string > mySMRSet = processFileToSet("MR_NE.csv");
	//~ set< string > mySMRSet = processFileToSet("MR_NN.csv");
	vector< set< string > > dicc(proteinSet.length());
	cout << "creating dicctionary... " << endl; 
	createDicctionaryOfSmr(mySMRSet, dicc);
	cout << "dicctionary ready... " << endl;
	for(int i=0; i<50; ++i){
		cout << "dicc[" << i << "] : " << dicc[i].size() <<endl; 
	}
	
	//~ cout << "files size : " << files.size() <<endl;
	ofstream familiarityFile;
	ofstream coverageFile;
	familiarityFile.open ("familiarity.csv", ofstream::app);
	coverageFile.open ("coverage.csv", ofstream::app);
	cout << "files ready.. calculating coverage.." <<endl;
	for(int i=0; i<1; ++i){
		//~ cout << "i: " << i << " files size: " << files.size()<<endl;
		ifstream myfile("/home/naciraa/Documents/materias/tesis/kapow/tesiskapow/codigo/NEWAnk/UniRef90_P25963.fasta");
		if (!myfile.good()) {
			cerr << "Error opening: " << files[i] << " . You have failed." << endl;
			//~ return -1;
		} 
		string line, id, protein;
		while (getline(myfile, line)) {
			if(line.empty()){
				continue;
			}
				
			if (line[0] == '>') {
				// output previous line before overwriting id
				// but ONLY if id actually contains something
				if(!id.empty()){
					cout << id << " : " << protein << endl;
				}
				id = line.substr(1);
				protein.clear();
			} else {
				//  if (line[0] != '>'){ // not needed because implicit
				protein += line;
			}
		}
		cout << protein <<endl;
		
		//~ float familiarity = 0.0;
		//~ for(int i=1; i<proteina.size()-1; ++i){
			//~ float aux = calculateCoverageKMP(proteina, dicc, i);
			//~ familiarity += aux;
			//~ cout << "writing " << aux << " to coverage file"<<endl;
			//~ coverageFile << aux << "\n";
		//~ }
		//~ cout << "writing separator ; to coverage file"<<endl;
		//~ coverageFile << ';' << "\n";
		//~ float familiarity = calculateCoverageWriteToFile(protein, dicc, coverageFile);
		float familiarity = calculateCoverageKMP(protein, dicc, coverageFile);
		cout << "writing " << familiarity << " to familiarity file"<<endl;
		familiarityFile << familiarity << "\n";
		cout << "writing separator ; to familiarity file"<<endl;
		familiarityFile << ';' << "\n";
	}
	
	familiarityFile.close();	
	coverageFile.close();	
	cout << "fin" <<endl;	
}

void calculateFootPrint(){
	string proteinSet = "";
	//~ readWholeProtein(proteinSet);
	//~ proteina = proteinSet;
	vector< string > files = listFile();
	proteinSet 	= concatenateProteins(files);
    //~ cout << proteinSet <<endl;
    //~ proteina = proteinSet;
	//~ vector< int > sai;
	//~ vector< int > lcp;
	//~ cout << "calculando SA y LCP ..." <<endl;
	//~ cout << "longitud alfabeto: " << alfabeto.size() <<endl;
	//~ calculateSAIandLCPForSetWithStruct(proteinSet, sai, lcp);
	//~ cout << "longitud alfabeto: " << alfabeto.size() <<endl;
	//~ vector< pair< int, pair< int, int > > > intervals;
	//~ cout << "buscando intervalos ..." <<endl;
	//~ findIntervalsPaper(sai, lcp, intervals);
	//~ cout << "procesando intervalos ..." <<endl;
	//~ processIntervals(intervals, sai, lcp);
	//~ cout << "calculando diferencia smr ..." <<endl;
	//~ set< string > mySMRSet = processFileToSet("MR_SMR.csv");
	set< string > mySMRSet = processFileToSet("MR_NE.csv");
	//~ set< string > mySMRSet = processFileToSet("MR_NN.csv");
	trie * myTrie = new trie();
	vector< set< string > > dicc(1000000);
	cout << "creating dicctionary... " << endl; 
	createDicctionaryOfSmr(mySMRSet, dicc);
	cout << "dicctionary ready... dicc[3]: " << dicc[3].size()<< endl;
	cout << "creating trie... " << endl; 
	cout << "#NEXUS" <<endl;
	cout << "" <<endl;
	cout << "Begin trees;" <<endl;
	cout << "" <<endl;
	
	for (int i = 1; i < 10; i++){
		createTrieOfSmr(dicc[i], myTrie);
	}
	
	cout << "trie ready... " << endl;
	//~ printFootPrint (myTrie, 3);
	printFootPrint (myTrie, 2, 3);
	printTrieAsGraph (myTrie, 2, 3);
	cout << "cant nodos " << countTree(myTrie->ini) <<endl;
	//~ cout << "fin" <<endl;	
	//~ cout << "End;" <<endl;
}
