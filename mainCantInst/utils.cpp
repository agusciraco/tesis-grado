#include "utils.h"

vector< string > listFile(){
	vector < string > files;
	DIR *pDIR;
	struct dirent *entry;
	string dirPath = "/home/naciraa/Documents/materias/tesis/kapow/tesiskapow/codigo/NEWAnk/";
    if( pDIR=opendir("/home/naciraa/Documents/materias/tesis/kapow/tesiskapow/codigo/NEWAnk") ){
		cout << "pDir open" << endl;
		while(entry = readdir(pDIR)){
			if( strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0 ){
				string fileName = dirPath + entry->d_name;
				files.push_back(fileName);
			}
			
		}
	} else {
		cout << "pDir fail open" << endl;
	}
	closedir(pDIR);
	return files;

}

vector< string > listFileFromPath(string dirPath){
	vector < string > files;
	DIR *pDIR;
	struct dirent *entry;
	//~ string dirPath = "/home/naciraa/Documents/materias/tesis/kapow/tesiskapow/codigo/NEWAnk/";
    if( pDIR=opendir(dirPath.c_str()) ){
		cout << "pDir open" << endl;
		while(entry = readdir(pDIR)){
			if( strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0 ){
				string fileName = dirPath + entry->d_name;
				files.push_back(fileName);
			}
			
		}
	} else {
		cout << "pDir fail open" << endl;
	}
	closedir(pDIR);
	return files;

}


vector< string > listDirsFromPath(string dirPath){
	vector < string > files;
	DIR *pDIR;
	struct dirent *entry;
	if( pDIR=opendir(dirPath.c_str()) ){
		cout << "pDir open" << endl;
		while(entry = readdir(pDIR)){
			if( strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0 ){
				string fileName = dirPath + entry->d_name;
				files.push_back(fileName);
			}
		}
	} else {
		cout << "pDir fail open" << endl;
	}
	closedir(pDIR);
	return files;

}


string concatenateProteins(vector< string > &files){
	cout << "protein concat " <<endl;
	string proteinSet;
	cout << "files size : " << files.size() <<endl;
	for(int i=0; i<files.size(); ++i){
		//~ cout << "i: " << i << " files size: " << files.size()<<endl;
		ifstream myfile(files[i]);
		if (!myfile.good()) {
			cout << "Error opening: " << files[i] << " . You have failed." << endl;
			cerr << "Error opening: " << files[i] << " . You have failed." << endl;
			//~ return -1;
		} 
		string line, id, DNA_sequence;
		while (getline(myfile, line)) {
			if(line.empty()){
				continue;
			}
				
			if (line[0] == '>') {
				// output previous line before overwriting id
				// but ONLY if id actually contains something
				if(!id.empty()){
					cout << id << " : " << DNA_sequence << endl;
				}
				id = line.substr(1);
				DNA_sequence.clear();
			} else {
				//  if (line[0] != '>'){ // not needed because implicit
				DNA_sequence += line;
			}
		}
		proteinSet = proteinSet + DNA_sequence + '{';
		
	}
	return proteinSet;
}

pair<string, vector< string >> concatenateProteinsPid(vector< string > &files){
	cout << "protein concat " <<endl;
	string proteinSet;
	vector<string> proteinSetPid;
	cout << "files size : " << files.size() <<endl;
	for(int i=0; i<files.size(); ++i){
		//~ cout << "i: " << i << " files size: " << files.size()<<endl;
		ifstream myfile(files[i]);
		if (!myfile.good()) {
			cout << "Error opening: " << files[i] << " . You have failed." << endl;
			cerr << "Error opening: " << files[i] << " . You have failed." << endl;
			//~ return -1;
		} 
		string line, id, DNA_sequence;
		while (getline(myfile, line)) {
			if(line.empty()){
				continue;
			}
				
			if (line[0] == '>') {
				// output previous line before overwriting id
				// but ONLY if id actually contains something
				if(!id.empty()){
					cout << id << " : " << DNA_sequence << endl;
				}
				id = line.substr(1);
				DNA_sequence.clear();
			} else {
				//  if (line[0] != '>'){ // not needed because implicit
				DNA_sequence += line;
			}
		}
		proteinSet = proteinSet + DNA_sequence + '{';
		char buffer [100];
		int auxI = i+1;
		snprintf(buffer, sizeof(buffer), "%d", auxI);
		for (int k = 0; k < DNA_sequence.size(); k++){
			proteinSetPid.push_back(buffer);
		}
		proteinSetPid.push_back("0");
		//~ cout << "i+1 auxI " << auxI << endl; 
		//~ cout << "i+1 char " << buffer << endl; 
		//~ string aux (DNA_sequence.length(), buffer);
		//~ proteinSetPid = proteinSetPid + aux + '0';
		
	}
	//~ vector< string > res;
	//~ res.push_back(proteinSet);
	//~ res.push_back(proteinSetPid);
	//~ return res;
	return make_pair(proteinSet, proteinSetPid);
}

//~ unsigned int affectedProteins(int ini,int fin, vector< int > SA, string Pid){
unsigned int affectedProteins(int ini,int len, vector< int > &SA, vector< string> &Pid){
	//~ cout<< "calculating affected proteins..."<<endl;
	//~ cout<< "vector pid size..."<< Pid.size() <<endl;
	int fin = ini+len;
	//~ cout<< "ini.."<< ini << "  len.. "<< len << "  fin "<< fin << endl;
	set<string> res;
	for (int it = ini; it <= fin; it++) {
		res.insert(Pid[SA[it]]);
	}
	//~ for (set<string>::iterator it=res.begin(); it != res.end(); ++it){
		//~ cout << ' ' << *it; 
	//~ }
	return res.size();
}

//~ string readProteinLines(ifstream myfile){
	
	//~ string line, id, DNA_sequence;
	//~ while (getline(myfile, line) && myfile.good()) {
		//~ if(line.empty()){
			//~ continue;
		//~ }
			
		//~ if (line[0] == '>') {
			//~ // output previous line before overwriting id
			//~ // but ONLY if id actually contains something
			//~ if(!id.empty()){
				//~ cout << id << " : " << DNA_sequence << endl;
			//~ }
			//~ id = line.substr(1);
			//~ DNA_sequence.clear();
		//~ } else {
			//~ //  if (line[0] != '>'){ // not needed because implicit
			//~ DNA_sequence += line;
		//~ }
	//~ }
	//~ return DNA_sequence;
//~ }

//~ string concatenateProteins2(vector< string > &files){
	//~ cout << "protein concat " <<endl;
	//~ string proteinSet;
	//~ cout << "files size : " << files.size() <<endl;
	//~ for(int i=0; i<files.size(); ++i){
		//~ //cout << "i: " << i << " files size: " << files.size()<<endl;
		//~ ifstream myfile(files[i]);
		//~ if (myfile.is_open()){
			//~ string DNA_sequence = readProteinLines(myfile);
			//~ if(!myfile.good()){
				//~ cout << "error with file: " << files[i] << " the file could not be read properly." <<endl;
			//~ }
			//~ proteinSet = proteinSet + DNA_sequence + '{';
		//~ }
	//~ }
	//~ return proteinSet;
//~ }

void printVectorContent(vector < int > &v){
	for(int i=0; i<v.size(); ++i){
		cout << i << ": " << v[i] <<endl;
	}
}

void processMaxRepPRago(vector< int > &maxRep, string &s){
	ofstream myfile;
	//~ myfile.open ("MR_PRago.csv", ofstream::app);
	for(int j=0; j<maxRep.size(); ++j){
		//~ cout << "maxRep[ " << j << "] = "<< maxRep[j] <<endl;
		if(maxRep[j] != s.size()+1){
			myfile << s.substr(maxRep[j], j) << "\n";
		}
	}
	myfile.close();
}

void emptyFiles(){
	ofstream mySMRFile;
	ofstream myNEFile;
	ofstream myNNFile;
	mySMRFile.open ("MR_SMR.csv", ofstream::out | ofstream::trunc);
	myNEFile.open ("MR_NE.csv", ofstream::out | ofstream::trunc);
	myNNFile.open ("MR_NN.csv", ofstream::out | ofstream::trunc);
	
	mySMRFile.close();
	myNEFile.close();
	myNNFile.close();
}

void processSuper(string smr){
	ofstream myfile;
	//~ cout << "writing " << smr << " to SMR file"<<endl;
	myfile.open ("MR_SMR.csv", ofstream::app);
	myfile << smr << "\n";
	myfile.close();
}

void processNonNested(string nn){
	ofstream myfile;
	myfile.open ("MR_NN.csv", ofstream::app);
	myfile << nn << "\n";
	myfile.close();
	//~ cout << "writing " << nn << " to NN file"<<endl;
}

void processNested(string ne){
	ofstream myfile;
	myfile.open ("MR_NE.csv", ofstream::app);
	//~ cout << "writing " << ne << " to NE file"<<endl;
	myfile << ne << "\n";
	myfile.close();
}

set< string > processFileToSet(string fileName){
	set< string > result;
	ifstream myfile(fileName);
	if (!myfile.good()) {
		cerr << "Error opening: " << fileName << " . You have failed." << endl;
	} 
	string line, id, DNA_sequence;
	while (getline(myfile, line)) {
		if(line.empty()){
			break;
		}
		//~ cout << line << endl;
		result.insert(line);
	}
	myfile.close();
	return result;
}


set< pair<string,int> > processFileToSetPair(string fileName){
	set< pair<string,int> > result;
	ifstream myfile(fileName);
	if (!myfile.good()) {
		cerr << "Error opening: " << fileName << " . You have failed." << endl;
	} 
	string pattern;
	int cant;
	while (myfile >> pattern) {
		myfile >> cant;
		pair<string,int> aux = make_pair(pattern, cant);
		result.insert(aux);
	}
	myfile.close();
	return result;
}

set< pair<string,int> > processFileToSetPair2(string fileName){
	set< pair<string,int> > result;
	ifstream myfile(fileName);
	if (!myfile.good()) {
		cerr << "Error opening: " << fileName << " . You have failed." << endl;
	} 
	string pattern;
	int ini, fin,l;
	while (myfile >> pattern) {
		myfile >> ini;
		myfile >> fin;
		myfile >> l;
		pair<string,int> aux = make_pair(pattern, fin-ini+1);
		result.insert(aux);
	}
	myfile.close();
	return result;
}



void readWholeProtein(string &proteina){
	freopen("proteinaEntera","r",stdin);
	for(char c = getchar_unlocked();c>50;c=getchar_unlocked()){
		proteina.push_back(c);
	}
	return;
}


// Fills lps[] for given patttern pat[0..M-1]
void computeLPSArray(string pat, int M, vector < int > &lps)
{
	// length of the previous longest prefix suffix
	int len = 0;

	lps[0] = 0; // lps[0] is always 0

	// the loop calculates lps[i] for i = 1 to M-1
	int i = 1;
	while (i < M)
	{
		if (pat[i] == pat[len])
		{
			len++;
			lps[i] = len;
			i++;
		}
		else // (pat[i] != pat[len])
		{
			// This is tricky. Consider the example.
			// AAACAAAA and i = 7. The idea is similar 
			// to search step.
			if (len != 0)
			{
				len = lps[len-1];

				// Also, note that we do not increment
				// i here
			}
			else // if (len == 0)
			{
				lps[i] = 0;
				i++;
			}
		}
	}
}

// Prints occurrences of txt[] in pat[]
void KMPSearch(string pat, string &txt, vector< int > &cov)
{
	int M = pat.size();
	int N = txt.size();

	// create lps[] that will hold the longest prefix suffix
	// values for pattern
	vector < int > lps(M,0);

	// Preprocess the pattern (calculate lps[] array)
	computeLPSArray(pat, M, lps);

	int i = 0; // index for txt[]
	int j = 0; // index for pat[]
	while (i < N)
	{
		if (pat[j] == txt[i])
		{
			j++;
			i++;
		}

		if (j == M)
		{
			//~ cout<< "Found pattern at index " << i-j <<endl;
			//~ cout<< "index start " << i-j <<endl;
			//~ cout<< "index end " << i-j+pat.size() <<endl;
			cov[i-j] += 1;
			if(i-j+pat.size()<cov.size())cov[i-j+pat.size()] += -1;
			j = lps[j-1];
		}

		// mismatch after j matches
		else if (i < N && pat[j] != txt[i])
		{
			// Do not match lps[0..lps[j-1]] characters,
			// they will match anyway
			if (j != 0)
				j = lps[j-1];
			else
				i = i+1;
		}
	}
}

void writeResultsHeatMap(string filename, string results){
	ofstream myfile;
	myfile.open (filename, ofstream::app);
	//~ cout << "writing " << results << " to "<< filename <<endl;
	myfile << results;
	myfile.close();
}

void writeLineInFile(string filename, string line){
	ofstream myfile;
	myfile.open (filename, ofstream::app);
	myfile << line << "\n";
	myfile.close();
}

