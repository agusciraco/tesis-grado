#include "trie.h"
int insertAux (nodoTrie * aNode, string s){
	if(1 <= s.size()){
		nodoTrie * auxNode = new nodoTrie();
		auxNode->isFinal = false;
		auxNode->footPrint += 1;
		if(aNode->info[s[0]] == NULL){
			aNode->info[s[0]] = auxNode;
		} else {
			aNode->info[s[0]]->footPrint += 1;
		}
		int auxMaxFP = insertAux(aNode->info[s[0]], s.substr(1));
		if(s.size() == 1){
			aNode->isFinal = true;
		} else {
			aNode->isFinal = false;
		}
		//~ cout << "s[0]: " << s[0] << endl;
		//~ cout << "aNode->info[s[0]]->footPrint " << aNode->info[s[0]]->footPrint << endl;
		//~ cout << "footPrint: " << auxNode->footPrint << " auxMaxFP: " << auxMaxFP << endl;
		if (aNode->info[s[0]]->footPrint < auxMaxFP) return auxMaxFP;
		return aNode->info[s[0]]->footPrint;
	}
	return aNode->footPrint;
}

int insertAuxWithFP (nodoTrie * aNode, string s, int fp){
	if(1 <= s.size()){
		nodoTrie * auxNode = new nodoTrie();
		auxNode->isFinal = false;
		auxNode->footPrint = max(auxNode->footPrint, fp);
		if(aNode->info[s[0]] == NULL){
			aNode->info[s[0]] = auxNode;
		} else {
			aNode->info[s[0]]->footPrint = max(aNode->info[s[0]]->footPrint, fp);
		}
		int auxMaxFP = insertAuxWithFP(aNode->info[s[0]], s.substr(1), fp);
		if(s.size() == 1){
			aNode->isFinal = true;
		} else {
			aNode->isFinal = false;
		}
		//~ cout << "s[0]: " << s[0] << endl;
		//~ cout << "aNode->info[s[0]]->footPrint " << aNode->info[s[0]]->footPrint << endl;
		//~ cout << "footPrint: " << auxNode->footPrint << " auxMaxFP: " << auxMaxFP << endl;
		if (aNode->info[s[0]]->footPrint < auxMaxFP) return auxMaxFP;
		return aNode->info[s[0]]->footPrint;
	}
	return aNode->footPrint;
}

void insertString (trie * aTrie, string s){
	if(aTrie->ini == NULL){
		nodoTrie * auxNode = new nodoTrie();
		auxNode->isFinal = false;
		aTrie->ini = auxNode;
		auxNode = new nodoTrie();
		auxNode->isFinal = false;
		map< char, nodoTrie * > auxMap;
		auxMap.insert(make_pair(s[0], auxNode));
		aTrie->ini->info = auxMap;
		if(s.size() == 1){
			aTrie->ini->isFinal = true;
		} else {
			aTrie->ini->isFinal = false;
		}
		aTrie->maxFootPrint = 1;
	}
	if(aTrie->ini->info[s[0]] == NULL){
		nodoTrie * auxNode = new nodoTrie();
		aTrie->ini->info[s[0]] = auxNode;
	}
	aTrie->ini->info[s[0]]->footPrint += 1;
	aTrie->totalFootPrint += s.size(); 
	int auxMaxFP = insertAux(aTrie->ini->info[s[0]], s.substr(1));
	if (aTrie->ini->info[s[0]]->footPrint > auxMaxFP) auxMaxFP = aTrie->ini->info[s[0]]->footPrint;
	if (aTrie->maxFootPrint < auxMaxFP) aTrie->maxFootPrint = auxMaxFP;
	//~ cout << "aTrie->maxFootPrint: " << aTrie->maxFootPrint << " auxMaxFP: " << auxMaxFP << endl; 
	//~ cout << "aTrie->ini->info[" << s[0] <<"]->footPrint " << aTrie->ini->info[s[0]]->footPrint << endl; 
	
}

void calculateTotalFootPrint(trie * aTrie){
	aTrie->totalFootPrint = calculateTotalFootPrintAux(aTrie->ini);
}

int calculateTotalFootPrintAux(nodoTrie * node){
	int sumFP = 0;
	if (node != NULL ){
		queue< nodoTrie * > nodesToProcess;
		
		for (map< char, nodoTrie *>::iterator it=node->info.begin(); it!=node->info.end(); ++it) {
			
			if (it->second->info.size() >= 1) nodesToProcess.push(it->second);
			sumFP += it->second->footPrint;
			
		}
		while (!nodesToProcess.empty()){
			nodoTrie * currNode = nodesToProcess.front();
			sumFP += calculateTotalFootPrintAux(currNode);
			nodesToProcess.pop();
		}
	}
	return sumFP;	
}

void insertStringWithFP (trie * aTrie, pair<string, int> pattern){
	string s = pattern.first;
	int fp = pattern.second;
	//~ cout << "s: " << s << "  fp: " << fp <<endl;
	if(aTrie->ini == NULL){
		nodoTrie * auxNode = new nodoTrie();
		auxNode->isFinal = false;
		aTrie->ini = auxNode;
		auxNode = new nodoTrie();
		auxNode->isFinal = false;
		map< char, nodoTrie * > auxMap;
		auxMap.insert(make_pair(s[0], auxNode));
		aTrie->ini->info = auxMap;
		if(s.size() == 1){
			aTrie->ini->isFinal = true;
		} else {
			aTrie->ini->isFinal = false;
		}
		aTrie->maxFootPrint = 1;
	}
	if(aTrie->ini->info[s[0]] == NULL){
		nodoTrie * auxNode = new nodoTrie();
		aTrie->ini->info[s[0]] = auxNode;
	}
	aTrie->ini->info[s[0]]->footPrint = max(aTrie->ini->info[s[0]]->footPrint, fp);
	//~ cout << "s: " << aTrie->ini->info[s[0]] << "  fp: " << aTrie->ini->info[s[0]]->footPrint <<endl;
	aTrie->totalFootPrint += (s.size()*aTrie->ini->info[s[0]]->footPrint); 
	int auxMaxFP = insertAuxWithFP(aTrie->ini->info[s[0]], s.substr(1), fp);
	if (aTrie->ini->info[s[0]]->footPrint > auxMaxFP) auxMaxFP = aTrie->ini->info[s[0]]->footPrint;
	if (aTrie->maxFootPrint < auxMaxFP) aTrie->maxFootPrint = auxMaxFP;
	//~ cout << "aTrie->maxFootPrint: " << aTrie->maxFootPrint << " auxMaxFP: " << auxMaxFP << endl; 
	//~ cout << "aTrie->ini->info[" << s[0] <<"]->footPrint " << aTrie->ini->info[s[0]]->footPrint << endl; 
}

bool searchString (trie * aTrie, string s){
	nodoTrie * auxNode = aTrie->ini->info[s[0]];
	int i = 1;
	while(auxNode != NULL && i< s.size()){
		auxNode = auxNode-> info[s[i]];
		i++;
	}
	if(auxNode == NULL && i<=s.size()){
		return false;
	} else {
		if(auxNode == NULL && i>s.size()){
			return false;
		}else{
			return true;
		}
	}
}

int searchStringCov (trie * aTrie, string &s){
	nodoTrie * auxNode = aTrie->ini->info[s[0]];
	nodoTrie * preNode = aTrie->ini->info[s[0]];
	int i = 1;
	int res = 0;
	while(auxNode != NULL && i <= s.size()){
		if (preNode->isFinal){
			//~ cout << s[i-1] << " is final: " << preNode->isFinal << " res: " << i << endl;
			res = i;
		}
		preNode = auxNode;
		auxNode = auxNode-> info[s[i]];
		i++;
	}
	return res;
}

void stringCoverage (trie * aTrie, string &s){
	vector< int > cov(s.size(), 0);
	for(int i=0; i<s.size(); ++i){
		string currString = s.substr(i);
		//~ cout << currString << endl;
		int currCov = searchStringCov(aTrie, currString);
		//~ cout << "se cubre desde: " << i << " hasta: " << i+currCov << endl;
		cov[i] += 1;
		cov[i+currCov] += -1;
	}
	
	float res = 0;
	int acum = 0;
	for(int i=0; i<cov.size(); ++i){
		//~ cout << "cov[" << i <<"] " << cov[i] << endl;
		acum += cov[i];
		if (acum > 0){
			res += 1;
		}
	}
	cout << "res: " << res << " string-size: " << s.length() << endl;
	cout << "coverage: " << res/s.length() << endl;
	res = res/s.length();
	
	return;
}

void printTrieLine(nodoTrie * tree, trie * aTrie, int maxDepth){
	if (tree != NULL && maxDepth > 0){
		int maxFootPrint = aTrie->maxFootPrint; 
		//~ cout << " maxFootPrint: " << aTrie->maxFootPrint;
		cout << "(";
		for (map< char, nodoTrie *>::iterator it=tree->info.begin(); it!=tree->info.end(); ++it) {
			if (it != tree->info.begin()) cout << " , ";
			if (it->second->info.size() >= 1) printTrieLine(it->second, aTrie, maxDepth-1);
			int currFootPrint =it->second->footPrint;
			//~ cout << " div: " << it->second->footPrint << " / " << maxFootPrint << " = " << currFootPrint <<endl;
			//~ printf("%.8lf", currFootPrint);
			cout << it->first << " " << currFootPrint;
		}
		cout << ")";
	}
}

void printTree(nodoTrie * tree, trie * aTrie, int maxDepth){
	if (tree != NULL && maxDepth > 0){
		int maxFootPrint = aTrie->maxFootPrint; 
		//~ cout << " maxFootPrint: " << aTrie->maxFootPrint;
		cout << "(";
		for (map< char, nodoTrie *>::iterator it=tree->info.begin(); it!=tree->info.end(); ++it) {
			if (it != tree->info.begin()) cout << ",";
			if (it->second->info.size() >= 1) printTree(it->second, aTrie, maxDepth-1);
			float currFootPrint = ((float) it->second->footPrint/ (float) maxFootPrint);
			//~ cout << " div: " << it->second->footPrint << " / " << maxFootPrint << " = " << currFootPrint <<endl;
			//~ printf("%.8lf", currFootPrint);
			if (currFootPrint > 1 ) cout << " Error: " << it->second->footPrint << " / " << maxFootPrint << " = " << currFootPrint <<endl;
			cout << it->first << "[&footPrint=" << currFootPrint << "]:0.9" ;
		}
		cout << ")";
	}
}

void printTreeWord(nodoTrie * tree, trie * aTrie, string parent, int maxDepth){
	if (tree != NULL && maxDepth > 0){
		int maxFootPrint = aTrie->maxFootPrint; 
		//~ cout << "maxFootPrint: "<< maxFootPrint <<endl;
		cout << "(";
		for (map< char, nodoTrie *>::iterator it=tree->info.begin(); it!=tree->info.end(); ++it) {
			string aux = parent;
			aux.push_back(it->first);
			if (it != tree->info.begin()) cout << ",";
			if (it->second->info.size() >= 1) printTreeWord(it->second, aTrie, aux, maxDepth-1);
			float currFootPrint = ((float) it->second->footPrint/ (float) maxFootPrint);
			if (currFootPrint > 1 ) cout << " Error: " << it->second->footPrint << " / " << maxFootPrint << " = " << currFootPrint <<endl;
			//~ cout << it->first << "[&footPrint=" << currFootPrint << "]:0.9" ;
			cout << aux << "[&footPrint=" << currFootPrint << "]:0.9" ;
			//~ cout << aux << ":0.9" ;
			aux = aux + " " + to_string(currFootPrint);
			writeToFile("metadata.txt", aux);
		}
		cout << ")";
	}
}

void printTreeWord2(nodoTrie * tree, trie * aTrie, string parent, int minDepth, int maxDepth){
	//~ cout << "minDepth: " << minDepth <<endl;
	if (tree != NULL && maxDepth > 0){
		int maxFootPrint = aTrie->maxFootPrint; 
		//~ cout << "maxFootPrint: "<< maxFootPrint <<endl;
		cout << "(";
		for (map< char, nodoTrie *>::iterator it=tree->info.begin(); it!=tree->info.end(); ++it) {
			string aux = parent;
			aux.push_back(it->first);
			if (it != tree->info.begin()) cout << ",";
			if (it->second->info.size() >= 1) printTreeWord2(it->second, aTrie, aux, minDepth-1, maxDepth-1);
			float currFootPrint = ((float) it->second->footPrint/ (float) maxFootPrint);
			if (currFootPrint > 1 ) cout << " Error: " << it->second->footPrint << " / " << maxFootPrint << " = " << currFootPrint <<endl;
			//~ cout << it->first << "[&footPrint=" << currFootPrint << "]:0.9" ;
			if(minDepth <= 0)cout << aux << "[&footPrint=" << currFootPrint << "]:0.9" ;
			//~ cout << aux << ":0.9" ;
			aux = aux + " " + to_string(currFootPrint);
			if(minDepth <= 0)writeToFile("metadata.txt", aux);
		}
		cout << ")";
	}
}

int printTreeWord2Sim(nodoTrie * tree, trie * aTrie, int parent, int nodeNumber, int minDepth, int maxDepth){
	int currNodeNumber = nodeNumber;
	if (tree != NULL && maxDepth > 0){
		int maxFootPrint = aTrie->maxFootPrint; 
		queue< pair<int, nodoTrie *> > nodesToProcess;
		
		for (map< char, nodoTrie *>::iterator it=tree->info.begin(); it!=tree->info.end(); ++it) {
			if (it->second->info.size() >= 1) nodesToProcess.push(make_pair(currNodeNumber, it->second));
			float currFootPrint = ((float) it->second->footPrint/ (float) maxFootPrint);
			if (currFootPrint > 1 ) cout << " Error: " << it->second->footPrint << " / " << maxFootPrint << " = " << currFootPrint <<endl;
			
			string currEdge = to_string(parent) + " " + to_string(currNodeNumber) + " " + to_string(currFootPrint);
			writeToFile("treeG.txt", currEdge);
			currNodeNumber++;
		}
		while (!nodesToProcess.empty()){
			currNodeNumber = printTreeWord2Sim(nodesToProcess.front().second, aTrie, nodesToProcess.front().first, currNodeNumber, minDepth-1, maxDepth-1);
			nodesToProcess.pop();
		}
	}
	return currNodeNumber;
}

string printTreeWord2SimStr(nodoTrie * tree, trie * aTrie, string parentLabel, int maxDepth){
	//~ cout << "maxDepth: " << maxDepth << endl;
	string currNodeLabel = parentLabel;
	if (tree != NULL && maxDepth > 0){
		int maxFootPrint = aTrie->maxFootPrint; 
		queue< pair< pair< string, int >, nodoTrie *> > nodesToProcess;
		
		for (map< char, nodoTrie *>::iterator it=tree->info.begin(); it!=tree->info.end(); ++it) {
			string nodeLabelAux = currNodeLabel + it->first;
			pair < string, int > paramsAux = make_pair(nodeLabelAux, maxDepth-1);
			if (it->second->info.size() >= 1) nodesToProcess.push(make_pair(paramsAux, it->second));
			float currFootPrint = ((float) it->second->footPrint/ (float) maxFootPrint);
			if (currFootPrint > 1 ) cout << " Error: " << it->second->footPrint << " / " << maxFootPrint << " = " << currFootPrint <<endl;
			
			string currEdge = parentLabel + " " + nodeLabelAux + " " + to_string((int)round(currFootPrint*100000));
			writeToFile("treeGStr.txt", currEdge);
			
		}
		while (!nodesToProcess.empty()){
			pair < string, int > params = nodesToProcess.front().first;
			//~ cout << params.first << "  " << params.second << endl;
			currNodeLabel = printTreeWord2SimStr(nodesToProcess.front().second, aTrie, params.first, params.second);
			nodesToProcess.pop();
		}
	}
	return currNodeLabel;
}

int countTree(nodoTrie * tree){
	int contador = 0 ;
	if (tree != NULL){
		for (map< char, nodoTrie *>::iterator it=tree->info.begin(); it!=tree->info.end(); ++it) {
			//~ if (it->second->info.size() >= 1) 
			contador += countTree(it->second);
		}
		contador++;;
	}
	return contador;
}

void printFootPrint (trie * aTrie, int minDepth, int maxDepth){
	cout << "tree TREE1 = [&R] ";
	writeToFile("metadata.txt", "taxa footPrint");
	//~ printTree(aTrie->ini, aTrie, maxDepth);
	//~ printTreeWord(aTrie->ini, aTrie, "", maxDepth);
	printTreeWord2(aTrie->ini, aTrie, "", minDepth, maxDepth);
	cout << "[&footPrint=0];" << endl;
	//~ cout << ";" << endl;
	cout << "End;" <<endl;
}

void printTrieAsGraph (trie * aTrie, int minDepth, int maxDepth){
	printTreeWord2Sim(aTrie->ini, aTrie, 1, 2, minDepth, maxDepth);
}

void printTrieAsGraphStr (trie * aTrie, int maxDepth){
	printTreeWord2SimStr(aTrie->ini, aTrie, "1", maxDepth);
}


void writeToFile(string filename, string line){
	ofstream myfile;
	myfile.open (filename, ofstream::app);
	myfile << line << "\n";
	myfile.close();
}

void writeAdjancencyMatrixToFile(string filename, string line){
	
}


pair<int,int> compare2TreeMetric(nodoTrie * aTrie1, nodoTrie * aTrie2) {
	
	pair<int, int> res (0,0) ;

	//revisar que ninguno sea null
	if (aTrie1 == NULL){
		if(aTrie2 == NULL){
			//~ cout << " ambos null ";
			return res;
		} else {
			return getMetricOfBranch(aTrie2);
		}
	} else {
		if(aTrie2 == NULL){
			return getMetricOfBranch(aTrie1);
		}
	}
	
	vector<char> aTrie1Keys;
	vector<char> aTrie2Keys;
	for (map< char, nodoTrie *>::iterator it1=aTrie1->info.begin(); it1!=aTrie1->info.end(); ++it1) {
		aTrie1Keys.push_back(it1->first);
	}
	
	
	for (map< char, nodoTrie *>::iterator it2=aTrie2->info.begin(); it2!=aTrie2->info.end(); ++it2) {
		aTrie2Keys.push_back(it2->first);
	}
	
	sort(aTrie1Keys.begin(), aTrie1Keys.end());
	sort(aTrie2Keys.begin(), aTrie2Keys.end());
	
	int i = 0;
	int j = 0;
	while (i < aTrie1Keys.size() && j < aTrie2Keys.size()) {
		//~ cout << " " << aTrie1Keys[i] << " " << aTrie1->info[aTrie1Keys[i]]->footPrint << " " << aTrie2Keys[j] <<  " " << aTrie2->info[aTrie2Keys[j]]->footPrint << endl;
		if (aTrie1Keys[i] == aTrie2Keys[j]){
			//bajo a ver los hijos
			pair<int,int> auxComp = compare2TreeMetric(aTrie1->info[aTrie1Keys[i]], aTrie2->info[aTrie2Keys[j]]);
			res.first += auxComp.first;
			res.second += auxComp.second;
			if(aTrie1->info[aTrie1Keys[i]]->footPrint < aTrie2->info[aTrie2Keys[j]]->footPrint){
				//~ res.second += (aTrie2->footPrint - aTrie1->footPrint)*(10^(-1*level));
				res.second += (aTrie2->info[aTrie2Keys[j]]->footPrint - aTrie1->info[aTrie1Keys[i]]->footPrint);
			}else{
				//~ res.second += (aTrie1->footPrint - aTrie2->footPrint)*(10^(-1*level));
				res.second += (aTrie1->info[aTrie1Keys[i]]->footPrint - aTrie2->info[aTrie2Keys[j]]->footPrint);
			}

			
			i++;
			j++;
		} else if ( aTrie1Keys[i] < aTrie2Keys[j]){
			//process aTrie1Keys[i] 
			pair<int,int> auxBranch1 = getMetricOfBranch(aTrie1->info[aTrie1Keys[i]]);
			res.first += (auxBranch1.first + 1);
			res.second += (auxBranch1.second + aTrie1->info[aTrie1Keys[i]]->footPrint);
			
			i++;
			//~ cout << "res: " << res << endl;
		} else {
			//process aTrie2Keys[j]
			pair<int,int> auxBranch2 = getMetricOfBranch(aTrie2->info[aTrie2Keys[j]]);
			res.first += (auxBranch2.first + 1);
			res.second += (auxBranch2.second + aTrie2->info[aTrie2Keys[j]]->footPrint);
			j++;
			//~ cout << "res: " << res << endl;
		}
		
	}
	//~ cout << endl;
	//~ cout << "fin main while " << endl;
	if (i < aTrie1Keys.size()) {
		while (i < aTrie1Keys.size()) {
			//process aTrie1Keys[i] 
			//~ cout << " " << aTrie1Keys[i] << " ";
			pair<int,int> auxBranch1 = getMetricOfBranch(aTrie1->info[aTrie1Keys[i]]);
			res.first += (auxBranch1.first + 1);
			res.second += (auxBranch1.second + aTrie1->info[aTrie1Keys[i]]->footPrint);
			i++;
		}
	} 
	if (j < aTrie2Keys.size()) {
		while (j < aTrie2Keys.size()) {
			//process aTrie2Keys[j]
			//~ cout << " " << aTrie2Keys[j] << " ";
			pair<int,int> auxBranch2 = getMetricOfBranch(aTrie2->info[aTrie2Keys[j]]);
			res.first += (auxBranch2.first + 1);
			res.second += (auxBranch2.second + aTrie2->info[aTrie2Keys[j]]->footPrint);
			j++;
		}
	}
	
	return res;
	
}

pair<int,int> compare2TreeMetricByLevel(nodoTrie * aTrie1, nodoTrie * aTrie2, int level, int maxLevel) {
	//~ cout << "level: " << level << " maxlevel: " << maxLevel <<endl;
	pair<int, int> res (0,0) ;
	
	if(level == maxLevel)return res;

	//revisar que ninguno sea null
	if (aTrie1 == NULL){
		if(aTrie2 == NULL){
			//~ cout << " ambos null ";
			return res;
		} else {
			return getMetricOfBranchByLevel(aTrie2, level, maxLevel);
		}
	} else {
		if(aTrie2 == NULL){
			return getMetricOfBranchByLevel(aTrie1, level, maxLevel);
		}
	}

	vector<char> aTrie1Keys;
	vector<char> aTrie2Keys;
	for (map< char, nodoTrie *>::iterator it1=aTrie1->info.begin(); it1!=aTrie1->info.end(); ++it1) {
		aTrie1Keys.push_back(it1->first);
	}
	
	
	for (map< char, nodoTrie *>::iterator it2=aTrie2->info.begin(); it2!=aTrie2->info.end(); ++it2) {
		aTrie2Keys.push_back(it2->first);
	}
	
	sort(aTrie1Keys.begin(), aTrie1Keys.end());
	sort(aTrie2Keys.begin(), aTrie2Keys.end());
	
	//~ if(level == maxLevel-1){
		//~ cout << "level: " << level << " maxlevel: " << maxLevel <<endl;
		//~ for (map< char, nodoTrie *>::iterator it1=aTrie1->info.begin(); it1!=aTrie1->info.end(); ++it1) {
			//~ cout << it1->first << " ";
		//~ }
		//~ cout << endl;
		
		
		//~ for (map< char, nodoTrie *>::iterator it2=aTrie2->info.begin(); it2!=aTrie2->info.end(); ++it2) {
			//~ cout << it2->first << " ";
		//~ }
		//~ cout << endl;
	//~ }
	
	//~ cout << "aTrie1Keys.size(): " << aTrie1Keys.size() << " aTrie2Keys.size(): " << aTrie2Keys.size() << endl;
	
	int i = 0;
	int j = 0;
	
	while (i < aTrie1Keys.size() && j < aTrie2Keys.size()) {
			//~ cout << " " << aTrie1Keys[i] << " " << aTrie1->footPrint << " " << aTrie2Keys[j] <<  " " << aTrie2->footPrint << endl;
		if (aTrie1Keys[i] == aTrie2Keys[j]){
			//bajo a ver los hijos
			pair<int,int> auxComp = compare2TreeMetricByLevel(aTrie1->info[aTrie1Keys[i]], aTrie2->info[aTrie2Keys[j]], level+1, maxLevel);
			res.first += auxComp.first;
			res.second += auxComp.second;
			if(aTrie1->info[aTrie1Keys[i]]->footPrint < aTrie2->info[aTrie2Keys[j]]->footPrint){
				//~ res.second += (aTrie2->footPrint - aTrie1->footPrint)*(10^(-1*level));
				res.second += (aTrie2->info[aTrie2Keys[j]]->footPrint - aTrie1->info[aTrie1Keys[i]]->footPrint);
			}else{
				//~ res.second += (aTrie1->footPrint - aTrie2->footPrint)*(10^(-1*level));
				res.second += (aTrie1->info[aTrie1Keys[i]]->footPrint - aTrie2->info[aTrie2Keys[j]]->footPrint);
			}
			
			i++;
			j++;
		} else if ( aTrie1Keys[i] < aTrie2Keys[j] ){
			//process aTrie1Keys[i] 
			pair<int,int> auxBranch1 = getMetricOfBranchByLevel(aTrie1->info[aTrie1Keys[i]], level+1, maxLevel);
			res.first += (auxBranch1.first + 1);
			//~ res.second += (auxBranch1.second + (aTrie1->info[aTrie1Keys[i]]->footPrint*(10^(-1*level))));
			res.second += (auxBranch1.second + (aTrie1->info[aTrie1Keys[i]]->footPrint));
			
			i++;
		} else {
			//process aTrie2Keys[j]
			pair<int,int> auxBranch2 = getMetricOfBranchByLevel(aTrie2->info[aTrie2Keys[j]], level+1, maxLevel);
			res.first += (auxBranch2.first + 1);
			//~ res.second += (auxBranch2.second + (aTrie2->info[aTrie2Keys[j]]->footPrint*(10^(-1*level))));
			res.second += (auxBranch2.second + (aTrie2->info[aTrie2Keys[j]]->footPrint));
			j++;
		}
	}
	if (i < aTrie1Keys.size()) {
		while (i < aTrie1Keys.size()) {	
			//process aTrie1Keys[i] 
			pair<int,int> auxBranch1 = getMetricOfBranchByLevel(aTrie1->info[aTrie1Keys[i]], level, maxLevel);
			res.first += (auxBranch1.first + 1);
			//~ //res.second += (auxBranch1.second + (aTrie1->info[aTrie1Keys[i]]->footPrint*(10^(-1*level))));
			res.second += (auxBranch1.second + (aTrie1->info[aTrie1Keys[i]]->footPrint));
			i++;
		}
	} 
	if (j < aTrie2Keys.size()) {
		while (j < aTrie2Keys.size()) {
			//process aTrie2Keys[j]
			pair<int,int> auxBranch2 = getMetricOfBranchByLevel(aTrie2->info[aTrie2Keys[j]], level, maxLevel);
			res.first += (auxBranch2.first + 1);
			//~ //res.second += (auxBranch2.second + (aTrie2->info[aTrie2Keys[j]]->footPrint*(10^(-1*level))));
			res.second += (auxBranch2.second + (aTrie2->info[aTrie2Keys[j]]->footPrint));
			j++;
		}
	}
	
	return res;
	
}

map<string, int> compare2TreeMetricByLevelMap(nodoTrie * aTrie1, nodoTrie * aTrie2, int level, int maxLevel) {
	//~ cout << "level: " << level << " maxlevel: " << maxLevel <<endl;
	map<string, int> res;
	res["cantNodes"] = 0;
	res["tFP"] = 0;
	res["aTrie1-CN"] = 0;
	res["aTrie1-FP"] = 0;
	res["aTrie2-CN"] = 0;
	res["aTrie2-FP"] = 0;
	
	if(level == maxLevel)return res;

	//revisar que ninguno sea null
	if (aTrie1 == NULL){
		if(aTrie2 == NULL){
			//~ cout << " ambos null ";
			return res;
		} else {
			pair<int,int> auxBranch2 = getMetricOfBranchByLevel(aTrie2, level, maxLevel);
			res["cantNodes"] += auxBranch2.first;
			res["tFP"] += auxBranch2.second;
			res["aTrie2-CN"] += auxBranch2.first;
			res["aTrie2-FP"] += auxBranch2.second;
			return res;
		}
	} else {
		if(aTrie2 == NULL){
			pair<int,int> auxBranch1 = getMetricOfBranchByLevel(aTrie1, level, maxLevel);
			res["cantNodes"] += auxBranch1.first;
			res["tFP"] += auxBranch1.second;
			res["aTrie1-CN"] += auxBranch1.first;
			res["aTrie1-FP"] += auxBranch1.second;
			return res;
		}
	}

	vector<char> aTrie1Keys;
	vector<char> aTrie2Keys;
	for (map< char, nodoTrie *>::iterator it1=aTrie1->info.begin(); it1!=aTrie1->info.end(); ++it1) {
		aTrie1Keys.push_back(it1->first);
	}
	
	
	for (map< char, nodoTrie *>::iterator it2=aTrie2->info.begin(); it2!=aTrie2->info.end(); ++it2) {
		aTrie2Keys.push_back(it2->first);
	}
	
	sort(aTrie1Keys.begin(), aTrie1Keys.end());
	sort(aTrie2Keys.begin(), aTrie2Keys.end());

	int i = 0;
	int j = 0;
	
	while (i < aTrie1Keys.size() && j < aTrie2Keys.size()) {
			//~ cout << " " << aTrie1Keys[i] << " " << aTrie1->footPrint << " " << aTrie2Keys[j] <<  " " << aTrie2->footPrint << endl;
		if (aTrie1Keys[i] == aTrie2Keys[j]){
			//bajo a ver los hijos
			map<string, int> auxComp = compare2TreeMetricByLevelMap(aTrie1->info[aTrie1Keys[i]], aTrie2->info[aTrie2Keys[j]], level+1, maxLevel);
			res["cantNodes"] += auxComp["cantNodes"];
			res["tFP"] += auxComp["tFP"];
			res["aTrie1-CN"] += auxComp["aTrie1-CN"];
			res["aTrie1-FP"] += auxComp["aTrie1-FP"];
			res["aTrie2-CN"] += auxComp["aTrie2-CN"];
			res["aTrie2-FP"] += auxComp["aTrie2-FP"];
			if(aTrie1->info[aTrie1Keys[i]]->footPrint < aTrie2->info[aTrie2Keys[j]]->footPrint){
				//~ res.second += (aTrie2->footPrint - aTrie1->footPrint)*(10^(-1*level));
				res["tFP"] += (aTrie2->info[aTrie2Keys[j]]->footPrint - aTrie1->info[aTrie1Keys[i]]->footPrint);
				res["aTrie2-FP"] += (aTrie2->info[aTrie2Keys[j]]->footPrint - aTrie1->info[aTrie1Keys[i]]->footPrint);
			}else{
				//~ res.second += (aTrie1->footPrint - aTrie2->footPrint)*(10^(-1*level));
				res["tFP"] += (aTrie1->info[aTrie1Keys[i]]->footPrint - aTrie2->info[aTrie2Keys[j]]->footPrint);
				res["aTrie1-FP"] += (aTrie1->info[aTrie1Keys[i]]->footPrint - aTrie2->info[aTrie2Keys[j]]->footPrint);
			}
			
			i++;
			j++;
		} else if ( aTrie1Keys[i] < aTrie2Keys[j] ){
			//process aTrie1Keys[i] 
			pair<int,int> auxBranch1 = getMetricOfBranchByLevel(aTrie1->info[aTrie1Keys[i]], level+1, maxLevel);
			res["cantNodes"] += (auxBranch1.first + 1);
			//~ res.second += (auxBranch1.second + (aTrie1->info[aTrie1Keys[i]]->footPrint*(10^(-1*level))));
			res["tFP"] += (auxBranch1.second + (aTrie1->info[aTrie1Keys[i]]->footPrint));
			res["aTrie1-CN"] += (auxBranch1.first + 1);
			res["aTrie1-FP"] += (auxBranch1.second + (aTrie1->info[aTrie1Keys[i]]->footPrint));
			i++;
		} else {
			//process aTrie2Keys[j]
			pair<int,int> auxBranch2 = getMetricOfBranchByLevel(aTrie2->info[aTrie2Keys[j]], level+1, maxLevel);
			res["cantNodes"] += (auxBranch2.first + 1);
			//~ res.second += (auxBranch2.second + (aTrie2->info[aTrie2Keys[j]]->footPrint*(10^(-1*level))));
			res["tFP"] += (auxBranch2.second + (aTrie2->info[aTrie2Keys[j]]->footPrint));
			res["aTrie2-CN"] += (auxBranch2.first + 1);
			res["aTrie2-FP"] += (auxBranch2.second + (aTrie2->info[aTrie2Keys[j]]->footPrint));
			j++;
		}
	}
	if (i < aTrie1Keys.size()) {
		while (i < aTrie1Keys.size()) {	
			//process aTrie1Keys[i] 
			pair<int,int> auxBranch1 = getMetricOfBranchByLevel(aTrie1->info[aTrie1Keys[i]], level, maxLevel);
			//~ cout << auxBranch1.first << "  " << (auxBranch1.first + 1) << endl;
			res["cantNodes"] += (auxBranch1.first + 1);
			//~ //res.second += (auxBranch1.second + (aTrie1->info[aTrie1Keys[i]]->footPrint*(10^(-1*level))));
			res["tFP"] += (auxBranch1.second + (aTrie1->info[aTrie1Keys[i]]->footPrint));
			res["aTrie1-CN"] += (auxBranch1.first + 1);
			res["aTrie1-FP"] += (auxBranch1.second + (aTrie1->info[aTrie1Keys[i]]->footPrint));
			i++;
		}
	} 
	if (j < aTrie2Keys.size()) {
		while (j < aTrie2Keys.size()) {
			//process aTrie2Keys[j]
			pair<int,int> auxBranch2 = getMetricOfBranchByLevel(aTrie2->info[aTrie2Keys[j]], level, maxLevel);
			//~ cout << auxBranch2.first << "  " << (auxBranch2.first + 1) << endl;
			res["cantNodes"] += (auxBranch2.first + 1);
			//~ //res.second += (auxBranch2.second + (aTrie2->info[aTrie2Keys[j]]->footPrint*(10^(-1*level))));
			res["tFP"] += (auxBranch2.second + (aTrie2->info[aTrie2Keys[j]]->footPrint));
			res["aTrie2-CN"] += (auxBranch2.first + 1);
			res["aTrie2-FP"] += (auxBranch2.second + (aTrie2->info[aTrie2Keys[j]]->footPrint));
			j++;
		}
	}
	
	return res;
	
}

pair<int, int> getMetricOfBranch(nodoTrie * aTrie1){
	int cantNodes = 0;
	int diffFootPrint = 0;
	for (map< char, nodoTrie *>::iterator it1=aTrie1->info.begin(); it1!=aTrie1->info.end(); ++it1) {
		//~ cout << " " << it1->first << " " ;
		pair<int, int> auxRes = getMetricOfBranch(it1->second);
		cantNodes += auxRes.first;
		diffFootPrint += auxRes.second;
		cantNodes += 1;
		diffFootPrint += it1->second->footPrint ;
	}
	return make_pair(cantNodes, diffFootPrint);
}


pair<int, int> getMetricOfBranchByLevel(nodoTrie * aTrie1, int level, int maxLevel){
	
	int cantNodes = 0;
	int diffFootPrint = 0;
	//~ cout << "getMetricOfBranchByLevel level: " << level << " maxLevel: " << maxLevel << endl;
	if(level == maxLevel) return make_pair(cantNodes, diffFootPrint);
	
	for (map< char, nodoTrie *>::iterator it1=aTrie1->info.begin(); it1!=aTrie1->info.end(); ++it1) {
		//~ cout << " " << it1->first << " children: " ;
		pair<int, int> auxRes = getMetricOfBranchByLevel(it1->second, level+1, maxLevel);
		cantNodes += auxRes.first;
		diffFootPrint += auxRes.second;
		cantNodes += 1;
		diffFootPrint += it1->second->footPrint ;
	}
	return make_pair(cantNodes, diffFootPrint);
}

pair<int,int> compare2TriesWithFP(trie * myTrie1, trie * myTrie2) {
	cout << "trie1 cant nodos " << countTree(myTrie1->ini) <<endl;
	cout << "trie2 cant nodos " << countTree(myTrie2->ini) <<endl;
	pair<int,int> metric = compare2TreeMetric(myTrie1->ini, myTrie2->ini);
	//~ pair<int,int> metric = compare2TreeMetricByLevel(myTrie1->ini, myTrie2->ini, 0, 5);
	//~ cout << "max level: 5"<< endl;
	//~ cout << endl;
	//~ cout << "diff between two families " << metric <<endl;
	return metric;
}


pair<int,int> compare2TriesByLevel(trie * myTrie1, trie * myTrie2, int baseLevel, int maxLevel) {
	cout << "trie1 cant nodos " << countTree(myTrie1->ini) <<endl;
	cout << "trie2 cant nodos " << countTree(myTrie2->ini) <<endl;
	//~ pair<int,int> metric = compare2TreeMetric(myTrie1->ini, myTrie2->ini);
	pair<int,int> metric = compare2TreeMetricByLevel(myTrie1->ini, myTrie2->ini, baseLevel, maxLevel);
	//~ cout << "max level: 5"<< endl;
	//~ cout << endl;
	//~ cout << "diff between two families " << metric <<endl;
	return metric;
}

int compare2Tries(trie * myTrie1, trie * myTrie2) {
	cout << "trie1 cant nodos " << countTree(myTrie1->ini) <<endl;
	cout << "trie2 cant nodos " << countTree(myTrie2->ini) <<endl;
	pair<int,int> metric = compare2TreeMetric(myTrie1->ini, myTrie2->ini);
	//~ cout << endl;
	//~ cout << "diff between two families " << metric <<endl;
	return metric.first;
}


string randomStringLength(string symbols, int length, int seed){
	srand (seed);
	string res = "";
	for (int i = 0; i < length; i++){
		char aux = symbols[rand() % symbols.length()];
		//~ cout << aux << endl;
		res += aux;
	}
	return res;
}

void createSetWithSymbolsNoOcurrencies(set<string> &patronsSet, string symbols, int maxdepth, int seed){
	//~ srand (time(NULL));
	srand (seed);
	//para no tener repetidos puedo guardar los strings en un conjunto
	for (int i = 1; i <= maxdepth; i++){
		int cantStrigWithLength = rand() % 100;
		//~ cout << "cant string with lenth " << cantStrigWithLength << endl;
		for (int j = 0; j < cantStrigWithLength; j++){
			string ne = randomStringLength(symbols, i, seed);
			//~ cout << "string ne to add to set: " << ne << endl;
			patronsSet.insert(ne);
		}
	}
	//~ cout << "cant elems in set: " << patronsSet.size() << endl;
	
}


void createTrieWithSetAndOcurrencies(set<string> &smr, trie * smrTrie, int seed){
	//~ srand (time(NULL));
	srand (seed);
	set<string>::iterator it;
	for (it=smr.begin(); it!=smr.end(); ++it){
		string auxPattern = *it;
		int cantOccurrencies = rand() % 10;
		//~ cout << "string ne to add to trie: " << auxPattern << " " << cantOccurrencies << " times" << endl;
		for (int i = 0; i < cantOccurrencies; i++){
			//~ cout << "string ne to add to trie: " << auxPattern << endl;
			insertString (smrTrie, auxPattern);
		}
	}
}

void deleteTrie(trie * aTrie){
	deleteNodoTrie(aTrie->ini);
	delete aTrie;
}

void deleteNodoTrie(nodoTrie * node){
	if (node != NULL ){
		queue< nodoTrie * > nodesToProcess;
		
		for (map< char, nodoTrie *>::iterator it=node->info.begin(); it!=node->info.end(); ++it) {
			
			if (it->second != NULL) nodesToProcess.push(it->second);
			
		}
		
		delete node;
		
		while (!nodesToProcess.empty()){
			//~ nodoTrie * currNode = nodesToProcess.front();
			deleteNodoTrie(nodesToProcess.front());
			nodesToProcess.pop();
		}
	}
}


int validateTotalFootPrintAux(nodoTrie * node){
	int sumFP = 0;
	if (node != NULL ){
		queue< nodoTrie * > nodesToProcess;
		
		for (map< char, nodoTrie *>::iterator it=node->info.begin(); it!=node->info.end(); ++it) {
			
			if (it->second->info.size() >= 1) nodesToProcess.push(it->second);
			sumFP += it->second->footPrint;
			
		}
		while (!nodesToProcess.empty()){
			nodoTrie * currNode = nodesToProcess.front();
			sumFP += validateTotalFootPrintAux(currNode);
			nodesToProcess.pop();
		}
	}
	return sumFP;	
}

void validateTotalFootPrint(trie * aTrie){
	int sumFP = validateTotalFootPrintAux(aTrie->ini);
	bool res = (aTrie->totalFootPrint == sumFP);
	if (res){
		cout << "totalFootPrint equals the sum of all footPrints in Trie" << endl; 
	} else {
		cout << "totalFootPrint differs the sum of all footPrints in Trie. TFP = " << aTrie->totalFootPrint << " sumFP = " << sumFP << endl; 
	}
}
