#include "utils.h"
#include "trie.h"
#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <string.h>
#include <fstream>
#include <sstream>
#include <dirent.h>
#include <vector>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>
#include <set>
#include <stack>

using namespace std;

string proteina;
//~ string pid;
vector< string> pid;
unordered_set < char > alfabeto;


struct SuffixArray {
  const int L;
  string s;
  vector<vector<int> > P;
  vector<pair<pair<int,int>,int> > M;

  SuffixArray(const string &s) : L(s.length()), s(s), P(1, vector<int>(L, 0)), M(L) {
    for (int i = 0; i < L; i++) P[0][i] = int(s[i]);
    for (int skip = 1, level = 1; skip < L; skip *= 2, level++) {
		//~ cout << "skip: " << skip << endl;
      P.push_back(vector<int>(L, 0));
      for (int i = 0; i < L; i++) 
	M[i] = make_pair(make_pair(P[level-1][i], i + skip < L ? P[level-1][i + skip] : -1000), i);
      sort(M.begin(), M.end());
      for (int i = 0; i < L; i++) 
	P[level][M[i].second] = (i > 0 && M[i].first == M[i-1].first) ? P[level][M[i-1].second] : i;
    }    
  }

  vector<int> GetSuffixArray() { return P.back(); }
  vector<int> GetMiSuffix() {
    vector<int > suffixes(P.back().size());
    for(int i=0; i<(int)P.back().size(); ++i){
      suffixes[P.back()[i]] = i;
    }
    return suffixes;
  }

  // returns the length of the longest common prefix of s[i...L-1] and s[j...L-1]
  int LongestCommonPrefix(int i, int j) {
    int len = 0;
    if (i == j) return L - i;
    for (int k = P.size() - 1; k >= 0 && i < L && j < L; k--) {
      if (P[k][i] == P[k][j]) {
	i += 1 << k;
	j += 1 << k;
	len += 1 << k;
      }
    }
    return len;
  }
    
};


bool myComp(int i, int j);
void calculateSAandLCP(string &s, vector< string > &sa, vector< int > &lcp);
void calculateSAIandLCP(string &s, vector< int > &sai, vector< int > &lcp);

bool myCompForSet(int i, int j);
void calculateSAIandLCPForSet(string &s, vector< int > &sai, vector< int > &lcp);
void calculateSAIandLCPForSetWithStruct(string &s, vector< int > &sai, vector< int > &lcp);

void findIntervals(vector< int > &sai, vector< int > &lcp, vector< pair< int, pair< int, int > > > &intervals);
void findIntervalsPaper(vector< int > &sai, vector< int > &lcp, vector< pair< int, pair< int, int > > > &intervals);
void processIntervals(vector< pair< int, pair< int, int > > > &intervals, vector< int > &sa, vector< int > &lcp);
void processIntervalsWithAffectedProteins(vector< pair< int, pair< int, int > > > &intervals, vector< int > &sa, vector< int > &lcp);

void createDicctionaryOfSmrBySetPair(set< pair<string,int> > &smr, vector< set< pair<string,int> > > &dicc);
void createDicctionaryOfSmr(set< string > &smr, vector< set< string > > &dicc);
void createTrieOfSmr(set< string > &smr, trie * smrTrie);
void createTrieOfSmrWithFP(set< pair<string,int> > &smr, trie * smrTrie);
void createTrieOfSmrPref(set< string > &smr, trie * smrTrie);

float calculateCoverage(string p, set< string > m);
float calculateCoverageWriteToFile(string &p, vector< set< string > > &dicc, ofstream &coverageFile);
float calculateCoverageKMP(string &p, vector< set< string > > &dicc, ofstream &coverageFile);
void calculateFootPrint();

void mainConcatProtein();
//~ void mainConcatProteinFromFamily();
void mainConcatProteinFromFamily(string path);
void mainCoverageExample();

void calculateCoverageInProteins();
void mainCompare2Families(string path1, string path2);
void mainCompare2FamiliesByLevel(string path1, string path2, int baseLevel, int maxLevel);

void mainCompare2Tries();

void mainCompareAllFamilies();
void mainCompareAllFamiliesWithFamilyWeight();
void mainCompareAllFamiliesWithFPByLevel();

void mainComputeAllFamiliesNE();
void mainComputeAllFamiliesNEWithAffectedProteins();

void processAffectedProteins(vector< string> &pid, vector< int > &sa, string fileName);

void mainCustomCases();
void mainCustomCasesByLevel();
void calculateMetrics (trie * myTrie1, trie * myTrie2);
void calculateMetricsByLevel (trie * myTrie1, trie * myTrie2, int baseLevel, int maxLevel, vector<string> files);
void calculateMetricsByLevelMap (trie * myTrie1, trie * myTrie2, int baseLevel, int maxLevel, vector<string> files);
void calculateMetricsWAPByLevelMap (trie * myTrie1, trie * myTrie2, int baseLevel, int maxLevel, vector<string> files);
void calculateMetricsWAPMap (trie * myTrie1, trie * myTrie2, vector<string> files);


int main(){
	
	emptyFiles();

	mainCompareAllFamiliesWithFamilyWeight();
	
    return 0;
}

void calculateMetrics (trie * myTrie1, trie * myTrie2){
	pair<int,int> metric = compare2TriesWithFP(myTrie1, myTrie2);
	double cantNodesMetric =  (1.0*metric.first / (countTree(myTrie1->ini)-1 + countTree(myTrie2->ini)-1));
	cout << "Case metric: " << metric.first << "  " << metric.second << endl;
	cout << "Case result: " << cantNodesMetric << endl;
	cout << "Case diff FP: " << metric.second << " maxFP1: "<< myTrie1->maxFootPrint << " maxFP2: " << myTrie2->maxFootPrint << endl;
	cout << "Case sum MFP1 y MFP2: " << myTrie1->maxFootPrint + myTrie2->maxFootPrint << endl;
	cout << "Case TFP1: " << myTrie1->totalFootPrint << " TFP2: " << myTrie2->totalFootPrint << endl;
	cout << "Case sum TFP1 y TFP2: " << myTrie1->totalFootPrint + myTrie2->totalFootPrint << endl;
	double mFP = (1.0*metric.second / (myTrie1->maxFootPrint + myTrie2->maxFootPrint));
	cout << "Case diff FP/(maxFP1+maxFP2): " << mFP << endl;
	double tFP = (1.0*metric.second / ( myTrie1->totalFootPrint + myTrie2->totalFootPrint));
	cout << "Case diff FP/(TFP1+TFP2): " << tFP << endl;
	writeResultsHeatMap("corridaActual/totalFootPrintMetricResult.csv", to_string(tFP) + ",");
	writeResultsHeatMap("corridaActual/maxFootPrintMetricResult.csv", to_string(mFP) + ",");
	writeResultsHeatMap("corridaActual/cantNodesMetricResult.csv", to_string(cantNodesMetric) + ",");
}


void calculateMetricsByLevel (trie * myTrie1, trie * myTrie2, int baseLevel, int maxLevel, vector<string> files){
	pair<int,int> metric = compare2TriesByLevel(myTrie1, myTrie2, baseLevel, maxLevel);
	double cantNodesMetric =  (1.0*metric.first / (countTree(myTrie1->ini)-1 + countTree(myTrie2->ini)-1));
	cout << "Case metric: " << metric.first << "  " << metric.second << endl;
	cout << "Case result: " << cantNodesMetric << endl;
	cout << "Case diff FP: " << metric.second << " maxFP1: "<< myTrie1->maxFootPrint << " maxFP2: " << myTrie2->maxFootPrint << endl;
	cout << "Case sum MFP1 y MFP2: " << myTrie1->maxFootPrint + myTrie2->maxFootPrint << endl;
	cout << "Case TFP1: " << myTrie1->totalFootPrint << " TFP2: " << myTrie2->totalFootPrint << endl;
	cout << "Case sum TFP1 y TFP2: " << myTrie1->totalFootPrint + myTrie2->totalFootPrint << endl;
	double mFP = (1.0*metric.second / (myTrie1->maxFootPrint + myTrie2->maxFootPrint));
	cout << "Case diff FP/(maxFP1+maxFP2): " << mFP << endl;
	double tFP = (1.0*metric.second / ( myTrie1->totalFootPrint + myTrie2->totalFootPrint));
	cout << "Case diff FP/(TFP1+TFP2): " << tFP << endl;
	
	for (int i = 0; i < files.size(); i++){
		if(files[i].find("totalFootPrintMetric") != string::npos){
			writeResultsHeatMap(files[i], to_string(tFP) + ",");
		}
		if(files[i].find("maxFootPrintMetric") != string::npos){
			writeResultsHeatMap(files[i], to_string(mFP) + ",");
		}
		if(files[i].find("cantNodesMetric") != string::npos){
			writeResultsHeatMap(files[i], to_string(cantNodesMetric) + ",");
		}
	}
}

void calculateMetricsByLevelMap (trie * myTrie1, trie * myTrie2, int baseLevel, int maxLevel, vector<string> files){
	map<string, int> metric = compare2TreeMetricByLevelMap(myTrie1->ini, myTrie2->ini, baseLevel, maxLevel);
	
	double cantNodesMetric =  (1.0*metric["cantNodes"] / (countTree(myTrie1->ini)-1 + countTree(myTrie2->ini)-1));
	cout << "Case metric: " << metric["cantNodes"] << "  " << metric["tFP"] << endl;
	cout << "Case result: " << cantNodesMetric << endl;
	cout << "Case diff FP: " << metric["tFP"] << " maxFP1: "<< myTrie1->maxFootPrint << " maxFP2: " << myTrie2->maxFootPrint << endl;
	cout << "Case sum MFP1 y MFP2: " << myTrie1->maxFootPrint + myTrie2->maxFootPrint << endl;
	cout << "Case TFP1: " << myTrie1->totalFootPrint << " TFP2: " << myTrie2->totalFootPrint << endl;
	cout << "Case sum TFP1 y TFP2: " << myTrie1->totalFootPrint + myTrie2->totalFootPrint << endl;
	double mFP = (1.0*metric["tFP"] / (myTrie1->maxFootPrint + myTrie2->maxFootPrint));
	cout << "Case diff FP/(maxFP1+maxFP2): " << mFP << endl;
	double tFP = (1.0*metric["tFP"] / ( myTrie1->totalFootPrint + myTrie2->totalFootPrint));
	cout << "Case diff FP/(TFP1+TFP2): " << tFP << endl;
	cout << "diff added by aTrie1-CN: " << metric["aTrie1-CN"] << endl;
	cout << "diff added by aTrie1-FP: " << metric["aTrie1-FP"] << endl;
	cout << "diff added by aTrie2-CN: " << metric["aTrie2-CN"] << endl;
	cout << "diff added by aTrie2-FP: " << metric["aTrie2-FP"] << endl;
	
	for (int i = 0; i < files.size(); i++){
		if(files[i].find("totalFootPrintMetric") != string::npos){
			writeResultsHeatMap(files[i], to_string(tFP) + ",");
		}
		if(files[i].find("maxFootPrintMetric") != string::npos){
			writeResultsHeatMap(files[i], to_string(mFP) + ",");
		}
		if(files[i].find("cantNodesMetric") != string::npos){
			writeResultsHeatMap(files[i], to_string(cantNodesMetric) + ",");
		}
	}
}

void calculateMetricsWAPByLevelMap (trie * myTrie1, trie * myTrie2, int baseLevel, int maxLevel, vector<string> files){
	map<string, int> metric = compare2TreeMetricByLevelMap(myTrie1->ini, myTrie2->ini, baseLevel, maxLevel);
	
	double cantNodesMetric =  (1.0*metric["cantNodes"] / (countTree(myTrie1->ini)-1 + countTree(myTrie2->ini)-1));
	cout << "Case metric: " << metric["cantNodes"] << "  " << metric["tFP"] << endl;
	cout << "Case result: " << cantNodesMetric << endl;
	cout << "Case diff FP: " << metric["tFP"] << " maxFP1: "<< myTrie1->maxFootPrint << " maxFP2: " << myTrie2->maxFootPrint << endl;
	cout << "Case sum MFP1 y MFP2: " << myTrie1->maxFootPrint + myTrie2->maxFootPrint << endl;
	cout << "Case TFP1: " << myTrie1->totalFootPrint << " TFP2: " << myTrie2->totalFootPrint << endl;
	cout << "Case sum TFP1 y TFP2: " << myTrie1->totalFootPrint + myTrie2->totalFootPrint << endl;
	double mFP = (1.0*metric["tFP"] / (myTrie1->maxFootPrint + myTrie2->maxFootPrint));
	cout << "Case diff FP/(maxFP1+maxFP2): " << mFP << endl;
	double tFP = (1.0*metric["tFP"] / ( myTrie1->totalFootPrint + myTrie2->totalFootPrint));
	cout << "Case diff FP/(TFP1+TFP2): " << tFP << endl;
	cout << "diff added by aTrie1-CN: " << metric["aTrie1-CN"] << endl;
	cout << "diff added by aTrie1-FP: " << metric["aTrie1-FP"] << endl;
	cout << "diff added by aTrie2-CN: " << metric["aTrie2-CN"] << endl;
	cout << "diff added by aTrie2-FP: " << metric["aTrie2-FP"] << endl;
	
	for (int i = 0; i < files.size(); i++){
		if(files[i].find("totalFootPrintMetric") != string::npos){
			writeResultsHeatMap(files[i], to_string(tFP) + ",");
		}
		if(files[i].find("maxFootPrintMetric") != string::npos){
			writeResultsHeatMap(files[i], to_string(mFP) + ",");
		}
		if(files[i].find("cantNodesMetric") != string::npos){
			writeResultsHeatMap(files[i], to_string(cantNodesMetric) + ",");
		}
	}
}

//~ void calculateMetricsWAPMap (trie * myTrie1, trie * myTrie2, vector<string> files){
	//~ map<string, int> metric = compare2TreeMetricMap(myTrie1->ini, myTrie2->ini);
	
	//~ double cantNodesMetric =  (1.0*metric["cantNodes"] / (countTree(myTrie1->ini)-1 + countTree(myTrie2->ini)-1));
	//~ cout << "Case metric: " << metric["cantNodes"] << "  " << metric["tFP"] << endl;
	//~ cout << "Case result: " << cantNodesMetric << endl;
	//~ cout << "Case diff FP: " << metric["tFP"] << " maxFP1: "<< myTrie1->maxFootPrint << " maxFP2: " << myTrie2->maxFootPrint << endl;
	//~ cout << "Case sum MFP1 y MFP2: " << myTrie1->maxFootPrint + myTrie2->maxFootPrint << endl;
	//~ cout << "Case TFP1: " << myTrie1->totalFootPrint << " TFP2: " << myTrie2->totalFootPrint << endl;
	//~ cout << "Case sum TFP1 y TFP2: " << myTrie1->totalFootPrint + myTrie2->totalFootPrint << endl;
	//~ double mFP = (1.0*metric["tFP"] / (myTrie1->maxFootPrint + myTrie2->maxFootPrint));
	//~ cout << "Case diff FP/(maxFP1+maxFP2): " << mFP << endl;
	//~ double tFP = (1.0*metric["tFP"] / ( myTrie1->totalFootPrint + myTrie2->totalFootPrint));
	//~ cout << "Case diff FP/(TFP1+TFP2): " << tFP << endl;
	//~ cout << "diff added by aTrie1-CN: " << metric["aTrie1-CN"] << endl;
	//~ cout << "diff added by aTrie1-FP: " << metric["aTrie1-FP"] << endl;
	//~ cout << "diff added by aTrie2-CN: " << metric["aTrie2-CN"] << endl;
	//~ cout << "diff added by aTrie2-FP: " << metric["aTrie2-FP"] << endl;
	
	//~ for (int i = 0; i < files.size(); i++){
		//~ if(files[i].find("totalFootPrintMetric") != string::npos){
			//~ writeResultsHeatMap(files[i], to_string(tFP) + ",");
		//~ }
		//~ if(files[i].find("maxFootPrintMetric") != string::npos){
			//~ writeResultsHeatMap(files[i], to_string(mFP) + ",");
		//~ }
		//~ if(files[i].find("cantNodesMetric") != string::npos){
			//~ writeResultsHeatMap(files[i], to_string(cantNodesMetric) + ",");
		//~ }
	//~ }
//~ }
void mainCustomCases(){
	//case 1 - Disjoints sets
	cout << "case 1 - Disjoints sets: " << endl;
	trie * myTrie1 = new trie();
	trie * myTrie2 = new trie();
	myTrie1->totalFootPrint = 0;
	myTrie2->totalFootPrint = 0;
	set<string> patronsSet;
	set<string> patronsSet2;
	string symbols = "abcdefg";
	string symbols2 = "jklmnop";
	string symbols3 = "tuvwxyz";
	string symbols4 = "hiqrs";
	int maxdepth = 4;
	int maxdepth2 = 3;
	int seed = 1;
	int seed2 = 20;
	
	createSetWithSymbolsNoOcurrencies(patronsSet, symbols, maxdepth, seed);
	createTrieOfSmr(patronsSet, myTrie1);
	int totalFootPrint1 = 0;
	cout << " patronsSet:" ;
	for (set<string>::iterator it=patronsSet.begin(); it!=patronsSet.end(); ++it){
		cout << ' ' << *it;
		totalFootPrint1 += (*it).size();
	}
    cout << endl;
	createSetWithSymbolsNoOcurrencies(patronsSet2, symbols2, maxdepth2, seed2);
	createTrieOfSmr(patronsSet2, myTrie2);
	cout << " patronsSet2:" ;
	int totalFootPrint2 = 0;
	for (set<string>::iterator it=patronsSet2.begin(); it!=patronsSet2.end(); ++it){
		cout << ' ' << *it;
		totalFootPrint2 += (*it).size();
	}
	cout << endl;
	
	pair<int,int> metric = compare2TriesWithFP(myTrie1, myTrie2);
	double res =  (1.0*metric.first / (countTree(myTrie1->ini)-1 + countTree(myTrie2->ini)-1));
	cout << "Case 1 result: " << res << endl;
	cout << "Case 1 diff FP: " << metric.second << " maxFP1: "<< myTrie1->maxFootPrint << " maxFP2: " << myTrie2->maxFootPrint << endl;
	//~ cout << "Case 1 TFP: " << myTrie1->totalFootPrint << "=? " << totalFootPrint1 << " => " << myTrie1->totalFootPrint == totalFootPrint1 << " TFP2: " << myTrie2->totalFootPrint  << "=? " << totalFootPrint2 << " => " << myTrie2->totalFootPrint == totalFootPrint2 << << endl;
	cout << "Case 1 TFP: " << myTrie1->totalFootPrint << "=? " << totalFootPrint1 << " TFP2: " << myTrie2->totalFootPrint  << "=? " << totalFootPrint2 << endl;
	cout << "Case 1 sum FP1 y FP2: " << myTrie1->maxFootPrint + myTrie2->maxFootPrint << endl;
	cout << "Case 1 sum TFP1 y TFP2: " << myTrie1->totalFootPrint + myTrie2->totalFootPrint << endl;
	cout << "Case 1 diff FP/(maxFP1+maxFP2): " << (1.0*metric.second / (myTrie1->maxFootPrint + myTrie2->maxFootPrint)) << endl;
	cout << "Case 1 diff FP/(TFP1+TFP2): " << (1.0*metric.second / ( myTrie1->totalFootPrint + myTrie2->totalFootPrint)) << endl;
	
	cout << "-------------------------------------"<< endl;
	//case 2 - Equal sets
	cout << "case 2 - Equal sets: " << endl;
	trie * myTrie3 = new trie();
	
	createSetWithSymbolsNoOcurrencies(patronsSet, symbols, maxdepth, seed);
	createTrieOfSmr(patronsSet, myTrie3);
	//~ //createTireWithSetAndOcurrencies(patronsSet, myTrie1);
	
	pair<int,int> metric2 = compare2TriesWithFP(myTrie1, myTrie3);
	double res2 =  (1.0*metric2.first / (countTree(myTrie1->ini)-1 + countTree(myTrie3->ini)-1));
	cout << "Case 2 result: " << res2 << endl;
	cout << "Case 2 diff FP: " << metric2.second << " maxFP1: "<< myTrie1->maxFootPrint << " maxFP2: " << myTrie3->maxFootPrint << endl;
	cout << "Case 2 sum FP1 y FP2: " << myTrie1->maxFootPrint + myTrie3->maxFootPrint << endl;
	cout << "Case 2 diff FP/(maxFP1+maxFP2): " << (1.0*metric2.second / (myTrie1->maxFootPrint + myTrie3->maxFootPrint)) << endl;
	cout << "Case 2 TFP: " << myTrie1->totalFootPrint << "=? " << totalFootPrint1 << " TFP2: " << myTrie3->totalFootPrint  << "=? " << totalFootPrint1 << endl;
	cout << "Case 2 sum TFP1 y TFP2: " << myTrie1->totalFootPrint + myTrie3->totalFootPrint << endl;
	cout << "Case 2 diff FP/(TFP1+TFP2): " << (1.0*metric2.second / ( myTrie1->totalFootPrint + myTrie3->totalFootPrint)) << endl;
		
	cout << "-------------------------------------"<< endl;
	
	//case 3 - Set A included in B 
	cout << "case 3 - Set A included in B: " << endl;
	trie * myTrie4 = new trie();
	myTrie4->totalFootPrint = 0;
	
	set<string> patronsSet3;
	int maxdepth3 = 6;
	
	createSetWithSymbolsNoOcurrencies(patronsSet3, symbols, maxdepth, seed);
	int totalFootPrint4 = 0;
	for (set<string>::iterator it=patronsSet3.begin(); it!=patronsSet3.end(); ++it){
		totalFootPrint4 += (*it).size();
	}
	createTrieOfSmr(patronsSet3, myTrie4);
	
	int intersectionSize = patronsSet3.size() ;

	cout << "intersection size: " << intersectionSize << endl;
	
	for (int i = 1; i < 1000; i++)
	{
		createSetWithSymbolsNoOcurrencies(patronsSet3, symbols2, maxdepth3, i);
		cout << "difference size: " << patronsSet3.size() - intersectionSize << endl;
		
		trie * myTrie5 = new trie();
		myTrie5->totalFootPrint = 0;
		createTrieOfSmr(patronsSet3, myTrie5);
		
		int totalFootPrint5 = 0;
		for (set<string>::iterator it=patronsSet3.begin(); it!=patronsSet3.end(); ++it){
			totalFootPrint5 += (*it).size();
		}
		
		pair<int,int> metric3 = compare2TriesWithFP(myTrie4, myTrie5);
		double res3 =  (1.0*metric3.first / (countTree(myTrie4->ini)-1 + countTree(myTrie5->ini)-1));
		cout << "Case 3 result: " << res3 << endl;
		cout << "Case 3 diff FP: " << metric3.second << " maxFP1: "<< myTrie4->maxFootPrint << " maxFP2: " << myTrie5->maxFootPrint << endl;
		cout << "Case 3 sum FP1 y FP2: " << myTrie4->maxFootPrint + myTrie5->maxFootPrint << endl;
		cout << "Case 3 diff FP/(maxFP1+maxFP2): " << (1.0*metric3.second / (myTrie4->maxFootPrint + myTrie5->maxFootPrint)) << endl;
		cout << "Case 3 TFP: " << myTrie4->totalFootPrint << "=? " << totalFootPrint4 << " TFP2: " << myTrie5->totalFootPrint  << "=? " << totalFootPrint5 << endl;
		cout << "Case 3 sum TFP1 y TFP2: " << myTrie4->totalFootPrint + myTrie5->totalFootPrint << endl;
		cout << "Case 3 diff FP/(TFP1+TFP2): " << (1.0*metric3.second / ( myTrie4->totalFootPrint + myTrie5->totalFootPrint)) << endl;
		string line =  to_string(patronsSet3.size() - intersectionSize) + "," + to_string(res3);
		writeLineInFile("/home/naciraa/Documents/tesis/tesiskapow/codigo/custom-cases/results-increase-diff.csv", line );
	
	
	}
	
	
	
	cout << "-------------------------------------"<< endl;
	
	//case 4 - set A and set B  with intersection  
	cout << "case 4 - Set A and set B  with intersection: " << endl;
	trie * myTrie6 = new trie();
	trie * myTrie7 = new trie();
	
	myTrie6->totalFootPrint = 0;
	myTrie7->totalFootPrint = 0;
	
	set<string> patronsSet4;
	createTrieOfSmr(patronsSet, myTrie6);
	createTrieOfSmr(patronsSet2, myTrie7);
	for (int i = 1; i < 10000; i++)
	{
		createSetWithSymbolsNoOcurrencies(patronsSet4, symbols3, maxdepth, i);
		cout << "intersection size: " << patronsSet4.size() << endl;
		cout << "difference size: " << patronsSet.size() + patronsSet2.size() << endl;
		cout << "difference size TrieA: " << patronsSet.size() << endl;
		cout << "difference size TrieB: " << patronsSet2.size() << endl;
		createTrieOfSmr(patronsSet4, myTrie6);
		createTrieOfSmr(patronsSet4, myTrie7);
		int totalFootPrintI4 = 0;
		for (set<string>::iterator it=patronsSet4.begin(); it!=patronsSet4.end(); ++it){
			totalFootPrintI4 += (*it).size();
		}
		
		pair<int,int> metric4 = compare2TriesWithFP(myTrie6, myTrie7);
		double res4 =  (1.0*metric4.first / (countTree(myTrie6->ini)-1 + countTree(myTrie7->ini)-1));
		cout << "Case 4 result: " << res4 << endl;
		cout << "Case 4 diff FP: " << metric4.second << " maxFP1: "<< myTrie6->maxFootPrint << " maxFP2: " << myTrie7->maxFootPrint << endl;
		cout << "Case 4 sum FP1 y FP2: " << myTrie6->maxFootPrint + myTrie7->maxFootPrint << endl;
		cout << "Case 4 diff FP/(maxFP1+maxFP2): " << (1.0*metric4.second / (myTrie6->maxFootPrint + myTrie7->maxFootPrint)) << endl;
		cout << "Case 4 TFP: " << myTrie6->totalFootPrint << "=? " << totalFootPrint1+totalFootPrintI4 << " TFP2: " << myTrie7->totalFootPrint  << "=? " << totalFootPrint2+totalFootPrintI4 << endl;
		cout << "Case 4 sum TFP1 y TFP2: " << myTrie6->totalFootPrint + myTrie7->totalFootPrint << endl;
		cout << "Case 4 diff FP/(TFP1+TFP2): " << (1.0*metric4.second / ( myTrie6->totalFootPrint + myTrie7->totalFootPrint)) << endl;
		string line =  to_string(patronsSet4.size()) + "," + to_string(res4);
		writeLineInFile("/home/naciraa/Documents/tesis/tesiskapow/codigo/custom-cases/results-increase-intersection.csv", line );
	
	}
	cout << "-------------------------------------"<< endl;
	
	//case 5 - Disjoints sets with occurrencies
	cout << "case 5 - Disjoints sets with occurrencies: " << endl;
	trie * myTrie8 = new trie();
	trie * myTrie9 = new trie();
	myTrie8->totalFootPrint = 0;
	myTrie9->totalFootPrint = 0;
	
	set<string> patronsSet8;
	set<string> patronsSet9;
	
	createSetWithSymbolsNoOcurrencies(patronsSet8, symbols, maxdepth, seed);
	createTrieWithSetAndOcurrencies(patronsSet8, myTrie8, seed);

	createSetWithSymbolsNoOcurrencies(patronsSet9, symbols2, maxdepth2, seed2);
	createTrieWithSetAndOcurrencies(patronsSet9, myTrie9, seed2);
	
	pair<int,int> metric5 = compare2TriesWithFP(myTrie8, myTrie9);
	double res5 =  (1.0*metric5.first / (countTree(myTrie8->ini)-1 + countTree(myTrie9->ini)-1));
	cout << "Case 5 result: " << res5 << endl;
	cout << "Case 5 diff FP: " << metric5.second << " maxFP1: "<< myTrie8->maxFootPrint << " maxFP2: " << myTrie9->maxFootPrint << endl;
	cout << "Case 5 sum FP1 y FP2: " << myTrie8->maxFootPrint + myTrie9->maxFootPrint << endl;
	cout << "Case 5 diff FP/(maxFP1+maxFP2): " << (1.0*metric5.second / (myTrie8->maxFootPrint + myTrie9->maxFootPrint)) << endl;
	cout << "Case 5 TFP: " << myTrie8->totalFootPrint << " TFP2: " << myTrie9->totalFootPrint  << endl;
	cout << "Case 5 sum TFP1 y TFP2: " << myTrie8->totalFootPrint + myTrie9->totalFootPrint << endl;
	cout << "Case 5 diff FP/(TFP1+TFP2): " << (1.0*metric5.second / ( myTrie8->totalFootPrint + myTrie9->totalFootPrint)) << endl;
	
	cout << "-------------------------------------"<< endl;
	//case 6 - Equal sets with diff occurrencies
	cout << "case 6 - Equal sets swith occurrencies: " << endl;
	trie * myTrie10 = new trie();
	myTrie10->totalFootPrint = 0;
	
	createTrieWithSetAndOcurrencies(patronsSet8, myTrie10, seed2);
	
	pair<int,int> metric6 = compare2TriesWithFP(myTrie8, myTrie10);
	double res6 =  (1.0*metric6.first / (countTree(myTrie8->ini)-1 + countTree(myTrie10->ini)-1));
	cout << "Case 6 result: " << res6 << endl;
	cout << "Case 6 diff FP: " << metric6.second << " maxFP1: "<< myTrie8->maxFootPrint << " maxFP2: " << myTrie10->maxFootPrint << endl;
	cout << "Case 6 sum FP1 y FP2: " << myTrie8->maxFootPrint + myTrie10->maxFootPrint << endl;
	cout << "Case 6 diff FP/(maxFP1+maxFP2): " << (1.0*metric6.second / (myTrie8->maxFootPrint + myTrie10->maxFootPrint)) << endl;
	cout << "Case 6 TFP: " << myTrie8->totalFootPrint << " TFP2: " << myTrie10->totalFootPrint  << endl;
	cout << "Case 6 sum TFP1 y TFP2: " << myTrie8->totalFootPrint + myTrie10->totalFootPrint << endl;
	cout << "Case 6 diff FP/(TFP1+TFP2): " << (1.0*metric6.second / ( myTrie8->totalFootPrint + myTrie10->totalFootPrint)) << endl;
		
	cout << "-------------------------------------"<< endl;
	
}

void mainCustomCasesByLevel(){
	//case 1 - Disjoints sets
	cout << "case 1 - Disjoints sets: " << endl;
	trie * myTrie1 = new trie();
	trie * myTrie2 = new trie();
	myTrie1->totalFootPrint = 0;
	myTrie2->totalFootPrint = 0;
	set<string> patronsSet;
	set<string> patronsSet2;
	string symbols = "abcdefg";
	string symbols2 = "jklmnop";
	string symbols3 = "tuvwxyz";
	string symbols4 = "hiqrs";
	int maxdepth = 4;
	int maxdepth2 = 3;
	int seed = 1;
	int seed2 = 20;
	
	createSetWithSymbolsNoOcurrencies(patronsSet, symbols, maxdepth, seed);
	createTrieOfSmr(patronsSet, myTrie1);
	int totalFootPrint1 = 0;
	cout << " patronsSet:" ;
	for (set<string>::iterator it=patronsSet.begin(); it!=patronsSet.end(); ++it){
		cout << ' ' << *it;
		totalFootPrint1 += (*it).size();
	}
    cout << endl;
	createSetWithSymbolsNoOcurrencies(patronsSet2, symbols2, maxdepth2, seed2);
	createTrieOfSmr(patronsSet2, myTrie2);
	cout << " patronsSet2:" ;
	int totalFootPrint2 = 0;
	for (set<string>::iterator it=patronsSet2.begin(); it!=patronsSet2.end(); ++it){
		cout << ' ' << *it;
		totalFootPrint2 += (*it).size();
	}
	cout << endl;
	
	pair<int,int> metric = compare2TriesWithFP(myTrie1, myTrie2);
	pair<int,int> metricL = compare2TriesByLevel(myTrie1, myTrie2, 0, 3);
	double res =  (1.0*metric.first / (countTree(myTrie1->ini)-1 + countTree(myTrie2->ini)-1));
	double resL =  (1.0*metricL.first / (countTree(myTrie1->ini)-1 + countTree(myTrie2->ini)-1));
	cout << "Case 1 result: " << res << endl;
	cout << "Case 1 resultL: " << resL << endl;
	cout << "Case 1 diff FP: " << metric.second << " diffL FP: " << metricL.second  << " maxFP1: "<< myTrie1->maxFootPrint << " maxFP2: " << myTrie2->maxFootPrint << endl;
	//~ cout << "Case 1 TFP: " << myTrie1->totalFootPrint << "=? " << totalFootPrint1 << " => " << myTrie1->totalFootPrint == totalFootPrint1 << " TFP2: " << myTrie2->totalFootPrint  << "=? " << totalFootPrint2 << " => " << myTrie2->totalFootPrint == totalFootPrint2 << << endl;
	cout << "Case 1 TFP: " << myTrie1->totalFootPrint << "=? " << totalFootPrint1 << " TFP2: " << myTrie2->totalFootPrint  << "=? " << totalFootPrint2 << endl;
	cout << "Case 1 sum FP1 y FP2: " << myTrie1->maxFootPrint + myTrie2->maxFootPrint << endl;
	cout << "Case 1 sum TFP1 y TFP2: " << myTrie1->totalFootPrint + myTrie2->totalFootPrint << endl;
	cout << "Case 1 diff FP/(maxFP1+maxFP2): " << (1.0*metric.second / (myTrie1->maxFootPrint + myTrie2->maxFootPrint)) << endl;
	cout << "Case 1 diff FP/(TFP1+TFP2): " << (1.0*metric.second / ( myTrie1->totalFootPrint + myTrie2->totalFootPrint)) << endl;
	cout << "Case 1 diff FPL/(TFP1+TFP2): " << (1.0*metricL.second / ( myTrie1->totalFootPrint + myTrie2->totalFootPrint)) << endl;
	
	cout << "-------------------------------------"<< endl;
	//~ //case 2 - Equal sets
	//~ cout << "case 2 - Equal sets: " << endl;
	//~ trie * myTrie3 = new trie();
	
	//~ createSetWithSymbolsNoOcurrencies(patronsSet, symbols, maxdepth, seed);
	//~ createTrieOfSmr(patronsSet, myTrie3);
	//createTireWithSetAndOcurrencies(patronsSet, myTrie1);
	
	//~ pair<int,int> metric2 = compare2TriesWithFP(myTrie1, myTrie3);
	//~ double res2 =  (1.0*metric2.first / (countTree(myTrie1->ini)-1 + countTree(myTrie3->ini)-1));
	//~ cout << "Case 2 result: " << res2 << endl;
	//~ cout << "Case 2 diff FP: " << metric2.second << " maxFP1: "<< myTrie1->maxFootPrint << " maxFP2: " << myTrie3->maxFootPrint << endl;
	//~ cout << "Case 2 sum FP1 y FP2: " << myTrie1->maxFootPrint + myTrie3->maxFootPrint << endl;
	//~ cout << "Case 2 diff FP/(maxFP1+maxFP2): " << (1.0*metric2.second / (myTrie1->maxFootPrint + myTrie3->maxFootPrint)) << endl;
	//~ cout << "Case 2 TFP: " << myTrie1->totalFootPrint << "=? " << totalFootPrint1 << " TFP2: " << myTrie3->totalFootPrint  << "=? " << totalFootPrint1 << endl;
	//~ cout << "Case 2 sum TFP1 y TFP2: " << myTrie1->totalFootPrint + myTrie3->totalFootPrint << endl;
	//~ cout << "Case 2 diff FP/(TFP1+TFP2): " << (1.0*metric2.second / ( myTrie1->totalFootPrint + myTrie3->totalFootPrint)) << endl;
		
	//~ cout << "-------------------------------------"<< endl;
	
	//~ //case 3 - Set A included in B 
	//~ cout << "case 3 - Set A included in B: " << endl;
	//~ trie * myTrie4 = new trie();
	//~ myTrie4->totalFootPrint = 0;
	
	//~ set<string> patronsSet3;
	//~ int maxdepth3 = 6;
	
	//~ createSetWithSymbolsNoOcurrencies(patronsSet3, symbols, maxdepth, seed);
	//~ int totalFootPrint4 = 0;
	//~ for (set<string>::iterator it=patronsSet3.begin(); it!=patronsSet3.end(); ++it){
		//~ totalFootPrint4 += (*it).size();
	//~ }
	//~ createTrieOfSmr(patronsSet3, myTrie4);
	
	//~ int intersectionSize = patronsSet3.size() ;

	//~ cout << "intersection size: " << intersectionSize << endl;
	
	//~ for (int i = 1; i < 11; i++)
	//~ {
		//~ createSetWithSymbolsNoOcurrencies(patronsSet3, symbols2, maxdepth3, i);
		//~ cout << "difference size: " << patronsSet3.size() - intersectionSize << endl;
		
		//~ trie * myTrie5 = new trie();
		//~ myTrie5->totalFootPrint = 0;
		//~ createTrieOfSmr(patronsSet3, myTrie5);
		
		//~ int totalFootPrint5 = 0;
		//~ for (set<string>::iterator it=patronsSet3.begin(); it!=patronsSet3.end(); ++it){
			//~ totalFootPrint5 += (*it).size();
		//~ }
		
		//~ pair<int,int> metric3 = compare2TriesWithFP(myTrie4, myTrie5);
		//~ double res3 =  (1.0*metric3.first / (countTree(myTrie4->ini)-1 + countTree(myTrie5->ini)-1));
		//~ cout << "Case 3 result: " << res3 << endl;
		//~ cout << "Case 3 diff FP: " << metric3.second << " maxFP1: "<< myTrie4->maxFootPrint << " maxFP2: " << myTrie5->maxFootPrint << endl;
		//~ cout << "Case 3 sum FP1 y FP2: " << myTrie4->maxFootPrint + myTrie5->maxFootPrint << endl;
		//~ cout << "Case 3 diff FP/(maxFP1+maxFP2): " << (1.0*metric3.second / (myTrie4->maxFootPrint + myTrie5->maxFootPrint)) << endl;
		//~ cout << "Case 3 TFP: " << myTrie4->totalFootPrint << "=? " << totalFootPrint4 << " TFP2: " << myTrie5->totalFootPrint  << "=? " << totalFootPrint5 << endl;
		//~ cout << "Case 3 sum TFP1 y TFP2: " << myTrie4->totalFootPrint + myTrie5->totalFootPrint << endl;
		//~ cout << "Case 3 diff FP/(TFP1+TFP2): " << (1.0*metric3.second / ( myTrie4->totalFootPrint + myTrie5->totalFootPrint)) << endl;
	
	
	//~ }
	
	
	
	//~ cout << "-------------------------------------"<< endl;
	
	//~ //case 4 - set A and set B  with intersection  
	//~ cout << "case 4 - Set A and set B  with intersection: " << endl;
	//~ trie * myTrie6 = new trie();
	//~ trie * myTrie7 = new trie();
	
	//~ myTrie6->totalFootPrint = 0;
	//~ myTrie7->totalFootPrint = 0;
	
	//~ set<string> patronsSet4;
	//~ createTrieOfSmr(patronsSet, myTrie6);
	//~ createTrieOfSmr(patronsSet2, myTrie7);
	//~ for (int i = 1; i < 11; i++)
	//~ {
		//~ createSetWithSymbolsNoOcurrencies(patronsSet4, symbols3, maxdepth, i);
		//~ cout << "intersection size: " << patronsSet4.size() << endl;
		//~ cout << "difference size: " << patronsSet.size() + patronsSet2.size() << endl;
		//~ createTrieOfSmr(patronsSet4, myTrie6);
		//~ createTrieOfSmr(patronsSet4, myTrie7);
		//~ int totalFootPrintI4 = 0;
		//~ for (set<string>::iterator it=patronsSet4.begin(); it!=patronsSet4.end(); ++it){
			//~ totalFootPrintI4 += (*it).size();
		//~ }
		
		//~ pair<int,int> metric4 = compare2TriesWithFP(myTrie6, myTrie7);
		//~ double res4 =  (1.0*metric4.first / (countTree(myTrie6->ini)-1 + countTree(myTrie7->ini)-1));
		//~ cout << "Case 4 result: " << res4 << endl;
		//~ cout << "Case 4 diff FP: " << metric4.second << " maxFP1: "<< myTrie6->maxFootPrint << " maxFP2: " << myTrie7->maxFootPrint << endl;
		//~ cout << "Case 4 sum FP1 y FP2: " << myTrie6->maxFootPrint + myTrie7->maxFootPrint << endl;
		//~ cout << "Case 4 diff FP/(maxFP1+maxFP2): " << (1.0*metric4.second / (myTrie6->maxFootPrint + myTrie7->maxFootPrint)) << endl;
		//~ cout << "Case 4 TFP: " << myTrie6->totalFootPrint << "=? " << totalFootPrint1+totalFootPrintI4 << " TFP2: " << myTrie7->totalFootPrint  << "=? " << totalFootPrint2+totalFootPrintI4 << endl;
		//~ cout << "Case 4 sum TFP1 y TFP2: " << myTrie6->totalFootPrint + myTrie7->totalFootPrint << endl;
		//~ cout << "Case 4 diff FP/(TFP1+TFP2): " << (1.0*metric4.second / ( myTrie6->totalFootPrint + myTrie7->totalFootPrint)) << endl;
	
	//~ }
	//~ cout << "-------------------------------------"<< endl;
	
	//~ //case 5 - Disjoints sets with occurrencies
	//~ cout << "case 5 - Disjoints sets with occurrencies: " << endl;
	//~ trie * myTrie8 = new trie();
	//~ trie * myTrie9 = new trie();
	//~ myTrie8->totalFootPrint = 0;
	//~ myTrie9->totalFootPrint = 0;
	
	//~ set<string> patronsSet8;
	//~ set<string> patronsSet9;
	
	//~ createSetWithSymbolsNoOcurrencies(patronsSet8, symbols, maxdepth, seed);
	//~ createTrieWithSetAndOcurrencies(patronsSet8, myTrie8, seed);

	//~ createSetWithSymbolsNoOcurrencies(patronsSet9, symbols2, maxdepth2, seed2);
	//~ createTrieWithSetAndOcurrencies(patronsSet9, myTrie9, seed2);
	
	//~ pair<int,int> metric5 = compare2TriesWithFP(myTrie8, myTrie9);
	//~ double res5 =  (1.0*metric5.first / (countTree(myTrie8->ini)-1 + countTree(myTrie9->ini)-1));
	//~ cout << "Case 5 result: " << res5 << endl;
	//~ cout << "Case 5 diff FP: " << metric5.second << " maxFP1: "<< myTrie8->maxFootPrint << " maxFP2: " << myTrie9->maxFootPrint << endl;
	//~ cout << "Case 5 sum FP1 y FP2: " << myTrie8->maxFootPrint + myTrie9->maxFootPrint << endl;
	//~ cout << "Case 5 diff FP/(maxFP1+maxFP2): " << (1.0*metric5.second / (myTrie8->maxFootPrint + myTrie9->maxFootPrint)) << endl;
	//~ cout << "Case 5 TFP: " << myTrie8->totalFootPrint << " TFP2: " << myTrie9->totalFootPrint  << endl;
	//~ cout << "Case 5 sum TFP1 y TFP2: " << myTrie8->totalFootPrint + myTrie9->totalFootPrint << endl;
	//~ cout << "Case 5 diff FP/(TFP1+TFP2): " << (1.0*metric5.second / ( myTrie8->totalFootPrint + myTrie9->totalFootPrint)) << endl;
	
	//~ cout << "-------------------------------------"<< endl;
	//~ //case 6 - Equal sets with diff occurrencies
	//~ cout << "case 6 - Equal sets swith occurrencies: " << endl;
	//~ trie * myTrie10 = new trie();
	//~ myTrie10->totalFootPrint = 0;
	
	//~ createTrieWithSetAndOcurrencies(patronsSet8, myTrie10, seed2);
	
	//~ pair<int,int> metric6 = compare2TriesWithFP(myTrie8, myTrie10);
	//~ double res6 =  (1.0*metric6.first / (countTree(myTrie8->ini)-1 + countTree(myTrie10->ini)-1));
	//~ cout << "Case 6 result: " << res6 << endl;
	//~ cout << "Case 6 diff FP: " << metric6.second << " maxFP1: "<< myTrie8->maxFootPrint << " maxFP2: " << myTrie10->maxFootPrint << endl;
	//~ cout << "Case 6 sum FP1 y FP2: " << myTrie8->maxFootPrint + myTrie10->maxFootPrint << endl;
	//~ cout << "Case 6 diff FP/(maxFP1+maxFP2): " << (1.0*metric6.second / (myTrie8->maxFootPrint + myTrie10->maxFootPrint)) << endl;
	//~ cout << "Case 6 TFP: " << myTrie8->totalFootPrint << " TFP2: " << myTrie10->totalFootPrint  << endl;
	//~ cout << "Case 6 sum TFP1 y TFP2: " << myTrie8->totalFootPrint + myTrie10->totalFootPrint << endl;
	//~ cout << "Case 6 diff FP/(TFP1+TFP2): " << (1.0*metric6.second / ( myTrie8->totalFootPrint + myTrie10->totalFootPrint)) << endl;
		
	//~ cout << "-------------------------------------"<< endl;
	
}

void mainCompareAllFamilies() {
	vector< string > allFamilies = listDirsFromPath("/home/naciraa/Documents/tesis/Datos/canonicalAminoacidsProteins/familyDataset/");
	sort(allFamilies.begin(), allFamilies.end());
	
	int index1 = 1;
	int index2 = 0;
	
	for (int i = 0; i < allFamilies.size(); i++){
		if(index1 <= i){
			if(index2 == 0){
				writeResultsHeatMap("corridaActual/totalFootPrintMetricResult.csv", allFamilies[i].substr(78) + ",");
				writeResultsHeatMap("corridaActual/maxFootPrintMetricResult.csv", allFamilies[i].substr(78) + ",");
				writeResultsHeatMap("corridaActual/cantNodesMetricResult.csv", allFamilies[i].substr(78) + ",");
			}
			
			
			string filename = "MR-Families/MR_NE_" + allFamilies[i].substr(78) + ".csv";
			cout << "filename mr ne: " << filename << endl;
			set< string > myNESet = processFileToSet(filename);
			
			trie * myTrie = new trie();
			createTrieOfSmr(myNESet, myTrie);
			cout << "set myNESet of index: "<< index1 << " length: " << myNESet.size() <<endl;
			vector< set< string > > dicc(10000000);
			createDicctionaryOfSmr(myNESet, dicc);
			for (int i = 0; i < 6; i++)
			{
				cout << "cant patterns of size " << i << " : " << dicc[i].size() << endl;
			}
			
			
			//~ for (int i = 1; i < 12; i++){
				//~ createTrieOfSmr(dicc[i], myTrie);
				//~ //createTrieOfSmrPref(dicc[i], myTrie);
			//~ }
			
		
			for (int j = 0; j < allFamilies.size(); j++){
				cout << "index1 " << index1 << " i " << i << " index2 " << index2 << " j " << j <<endl;
				if(index1 <= i && index2 <= j){	
					cout << "----------------  Inicio  del caso  ---------------------" << endl;
					cout << "comparing families: " << endl;
					cout << allFamilies[i] << endl;
					cout << allFamilies[j] << endl;
					//~ mainCompare2Families(allFamilies[i], allFamilies[j]);
					//~ mainCompare2Families(allFamilies[i] + "/", allFamilies[j] + "/");
					string filename2 = "MR-Families/MR_NE_" + allFamilies[j].substr(78) + ".csv";
					cout << "filename mr ne: " << filename2 << endl;
					set< string > myNESet2 = processFileToSet(filename2);
					cout << "set myNESet2 of index: "<< index2 << " length: " << myNESet2.size() <<endl;
					trie * myTrie2 = new trie();
					createTrieOfSmr(myNESet2, myTrie2);
					//~ vector< set< string > > dicc2(10000000);
					//~ createDicctionaryOfSmr(myNESet2, dicc2);
					
					//~ for (int i = 1; i < 12; i++){
						//~ createTrieOfSmr(dicc2[i], myTrie2);
						//~ //createTrieOfSmrPref(dicc2[i], myTrie2);
					//~ }
					
					calculateMetrics (myTrie, myTrie2);
					validateTotalFootPrint(myTrie);
					validateTotalFootPrint(myTrie2);
					
					cout << "----------------  Fin  del caso  ---------------------" << endl;
					index1 = 0;
					index2 = 0;
					deleteTrie(myTrie2);
				}
			}
			
			writeResultsHeatMap("corridaActual/totalFootPrintMetricResult.csv", "\n");
			writeResultsHeatMap("corridaActual/maxFootPrintMetricResult.csv", "\n");
			writeResultsHeatMap("corridaActual/cantNodesMetricResult.csv", "\n");
			deleteTrie(myTrie);
		}
	}
	
}

void mainCompareAllFamiliesByLevel() {
	vector< string > allFamilies = listDirsFromPath("/home/naciraa/Documents/tesis/Datos/canonicalAminoacidsProteins/familyDataset/");
	sort(allFamilies.begin(), allFamilies.end());
	vector<string> files;
	files.push_back("totalFootPrintMetricResultLevelScrambledTest.csv");
	files.push_back("maxFootPrintMetricResultLevelScrambledTest.csv");
	files.push_back("cantNodesMetricResultLevelScrambledTest.csv");
	//~ files.push_back("totalFootPrintMetricResultLevelaux.csv");
	//~ files.push_back("maxFootPrintMetricResultLevelaux.csv");
	//~ files.push_back("cantNodesMetricResultLevelaux.csv");
	int level = 0;
	int index1 = 0;
	int index2 = 1;
	
	for (int i = 0; i < allFamilies.size(); i++){
		if(index1 <= i){
			if(index2 == 0){
				for (int h = 0; h < files.size(); h++){
					writeResultsHeatMap(files[h], allFamilies[i].substr(78) + ",");
				}
			
				//~ writeResultsHeatMap("totalFootPrintMetricResultLevel4.csv", allFamilies[i].substr(78) + ",");
				//~ writeResultsHeatMap("maxFootPrintMetricResultLevel4.csv", allFamilies[i].substr(78) + ",");
				//~ writeResultsHeatMap("cantNodesMetricResultLevel4.csv", allFamilies[i].substr(78) + ",");
			}
			
			
			string filename = "MR-Families/MR_NE_" + allFamilies[i].substr(78) + ".csv";
			cout << "filename mr ne: " << filename << endl;
			set< string > myNESet = processFileToSet(filename);
			
			trie * myTrie = new trie();
			vector< set< string > > dicc(10000000);
			createDicctionaryOfSmr(myNESet, dicc);
			
			for (int i = 1; i < 12; i++){
				createTrieOfSmr(dicc[i], myTrie);
				//~ createTrieOfSmrPref(dicc[i], myTrie);
			}
			
		
			for (int j = 0; j < allFamilies.size(); j++){
				cout << "index1 " << index1 << " i " << i << " index2 " << index2 << " j " << j <<endl;
				if(index1 <= i && index2 <= j){	
					cout << "----------------  Inicio  del caso  ---------------------" << endl;
					cout << "comparing families: " << endl;
					cout << allFamilies[i] << endl;
					cout << allFamilies[j] << endl;
					//~ mainCompare2Families(allFamilies[i], allFamilies[j]);
					//~ mainCompare2Families(allFamilies[i] + "/", allFamilies[j] + "/");
					string filename2 = "MR-Families/MR_NE_" + allFamilies[j].substr(78) + ".csv";
					cout << "filename mr ne: " << filename2 << endl;
					set< string > myNESet2 = processFileToSet(filename2);
					
					trie * myTrie2 = new trie();
					vector< set< string > > dicc2(10000000);
					createDicctionaryOfSmr(myNESet2, dicc2);
					
					for (int i = 1; i < 12; i++){
						createTrieOfSmr(dicc2[i], myTrie2);
						//~ createTrieOfSmrPref(dicc2[i], myTrie2);
					}
					
					//~ calculateMetricsByLevel(myTrie, myTrie2, 0, level, files);
					cout << "level: " << level << endl;
					calculateMetricsByLevelMap(myTrie, myTrie2, 0, level, files);
					validateTotalFootPrint(myTrie);
					validateTotalFootPrint(myTrie2);
					
					cout << "----------------  Fin  del caso  ---------------------" << endl;
					index1 = 0;
					index2 = 0;
					deleteTrie(myTrie2);
				}
			}
			
			for (int i = 0; i < files.size(); i++){
				writeResultsHeatMap(files[i], "\n");
			}
			
			
			//~ writeResultsHeatMap("maxFootPrintMetricResultLevel4.csv", "\n");
			//~ writeResultsHeatMap("cantNodesMetricResultLevel4.csv", "\n");
			deleteTrie(myTrie);
		}
	}
	
}

void mainCompareAllFamiliesWithFamilyWeight() {
	vector< string > allFamilies = listDirsFromPath("/home/naciraa/Documents/tesis/Datos/canonicalAminoacidsProteins/familyDataset/");
	sort(allFamilies.begin(), allFamilies.end());
	vector<string> files;
	files.push_back("../corridaActual/aporte-por-familia/totalFootPrintMetricResult-L0.csv");
	files.push_back("../corridaActual/aporte-por-familia/maxFootPrintMetricResult-L0.csv");
	files.push_back("../corridaActual/aporte-por-familia/cantNodesMetricResult-L0.csv");
	
	int level = 0;
	int index1 = 0;
	int index2 = 1;
	
	for (int i = 0; i < allFamilies.size(); i++){
		if(index1 <= i){
			if(index2 == 0){
				for (int h = 0; h < files.size(); h++){
					writeResultsHeatMap(files[h], allFamilies[i].substr(78) + ",");
				}
			}
			
			
			//~ string filename = "MR-Families-CantAffectedProteins/MR_NE_AP_" + allFamilies[i].substr(78) + ".csv";
			//~ cout << "filename mr ne: " << filename << endl;
			//~ set< pair<string,int> > myNESet = processFileToSetPair2(filename);
			string filename = "../MR-Families/MR_NE_" + allFamilies[i].substr(78) + ".csv";
			cout << "filename mr ne: " << filename << endl;
			set< string> myNESet = processFileToSet(filename);
			cout << "creating Trie for: " << filename << endl;
			 
			trie * myTrie = new trie();
			//~ createTrieOfSmrWithFP(myNESet, myTrie);
			createTrieOfSmr(myNESet, myTrie);
			cout << "comparing family: " << filename << endl;
		
			for (int j = 0; j < allFamilies.size(); j++){
				cout << "index1 " << index1 << " i " << i << " index2 " << index2 << " j " << j <<endl;
				if(index1 <= i && index2 <= j){	
					cout << "----------------  Inicio  del caso  ---------------------" << endl;
					cout << "comparing families: " << endl;
					cout << allFamilies[i] << endl;
					cout << allFamilies[j] << endl;
			
					//~ string filename2 = "MR-Families-CantAffectedProteins/MR_NE_AP_" + allFamilies[j].substr(78) + ".csv";
					//~ cout << "filename mr ne: " << filename2 << endl;
					//~ set< pair<string,int> > myNESet2 = processFileToSetPair2(filename2);
					string filename2 = "../MR-Families/MR_NE_" + allFamilies[j].substr(78) + ".csv";
					cout << "filename mr ne: " << filename2 << endl;
					set< string> myNESet2 = processFileToSet(filename2);
					
					trie * myTrie2 = new trie();
					//~ createTrieOfSmrWithFP(myNESet2, myTrie2);
					createTrieOfSmr(myNESet2, myTrie2);
					cout << "level: " << level << endl;
					calculateMetricsWAPByLevelMap(myTrie, myTrie2, 0, level, files);
					validateTotalFootPrint(myTrie);
					validateTotalFootPrint(myTrie2);
					
					cout << "----------------  Fin  del caso  ---------------------" << endl;
					index1 = 0;
					index2 = 0;
					deleteTrie(myTrie2);
					break;
				}
			}
			
			for (int i = 0; i < files.size(); i++){
				writeResultsHeatMap(files[i], "\n");
			}
			deleteTrie(myTrie);
			break;
		}
	}
		
}

void mainComputeAllFamiliesNEWithAffectedProteins() {
	vector< string > allFamilies = listDirsFromPath("/home/naciraa/Documents/tesis/Datos/canonicalAminoacidsProteins/familyDataset/");
	sort(allFamilies.begin(), allFamilies.end());
	
	for (int i = 0; i < allFamilies.size(); i++){
		cout << "computing family: " << allFamilies[i]<< endl;
		emptyFiles();
		vector< string > files = listFileFromPath(allFamilies[i] + "/");
		//~ vector < string > auxProteinSet = concatenateProteinsPid(files);
		pair<string, vector< string >> auxProteinSet = concatenateProteinsPid(files);
		proteina = "";
		pid.clear();
		//~ pid = "";
		//~ cout << "proteina: " << proteina << endl;
		//~ proteina = auxProteinSet[0];
		proteina = auxProteinSet.first;
		//~ pid = auxProteinSet[1];
		pid = auxProteinSet.second;
		//~ cout << "proteina: " << proteina <<endl;
		//~ cout << "pid: " << pid <<endl;
		vector< int > sai;
		vector< int > lcp;
		alfabeto.clear();
		//~ cout << "longitud alfabeto: " << alfabeto.size() <<endl;
		
		calculateSAIandLCPForSetWithStruct(proteina, sai, lcp);
		vector< pair< int, pair< int, int > > > intervals;
		cout<< "find intervals..."<<endl;
		findIntervalsPaper(sai, lcp, intervals);
		cout<< "process intervals with affected proteins..."<<endl;
		cout<< "protein size..." << proteina.size()<<endl;
		cout<< "pid size..." << pid.size()<<endl;
		processIntervalsWithAffectedProteins(intervals, sai, lcp);
		cout<< "renaming files..."<<endl;
		string aux = "MR_SMR_AP_" + allFamilies[i].substr(78)+ ".csv";
		cout << "copying file: " << aux <<endl;
		char cstr[aux.size() + 1];
		strcpy(cstr, aux.c_str());
		if(rename("MR_SMR.csv", cstr) != 0){
			cout << "File renamed fail"<<endl;
		} else {
			cout << "File renamed successfully" <<endl;
		}
		string aux2 = "MR_NE_AP_" + allFamilies[i].substr(78)+ ".csv";
		cout << "copying file: " << aux2 <<endl;
		char cstr2[aux2.size() + 1];
		strcpy(cstr2, aux2.c_str());
		if(rename("MR_NE.csv", cstr2) != 0){
			cout << "File renamed fail"<<endl;
		} else {
			cout << "File renamed successfully" <<endl;
		}
		string aux3 = "MR_NN_AP_" + allFamilies[i].substr(78)+ ".csv"; 
		cout << "copying file: " << aux <<endl;
		char cstr3[aux3.size() + 1];
		strcpy(cstr3, aux3.c_str());
		if(rename("MR_NN.csv", cstr3) != 0){
			cout << "File renamed fail"<<endl;
		} else {
			cout << "File renamed successfully" <<endl;
		}
		cout << "process affected proteins ..." <<endl;
		processAffectedProteins(pid, sai, aux2);
		cout << "end family " << allFamilies[i] <<endl;
	}
	
}

void processAffectedProteins(vector< string> &pid, vector< int > &sa, string fileName){
	set< string > result;
	ifstream myfile(fileName);
	cout << "processing file: " << fileName << endl;
	//~ ifstream myfile("MR_NE_AP_ABCtran.csv");
	if (!myfile.good()) {
		cerr << "Error opening: " << fileName << " . You have failed." << endl;
	} 
	string ne;
	int ini, fin, l;
	map<string, pair<int,int>> toProcess;
	while (myfile >> ne) {
		myfile >> ini;
		myfile >> fin;
		myfile >> l;
		pair<int,int> aux = make_pair(ini, l);
		toProcess[ne] = aux;
	}
	myfile.close();
	cout << "finish reading file: " << fileName << endl;
	cout << "map size: " << toProcess.size() << endl;
	
	for (map<string, pair<int,int>>::iterator it=toProcess.begin(); it!=toProcess.end(); ++it){
		//~ cout << it->first << " => " << it->second.first << " , " << it->second.second << '\n';
		unsigned int cant = affectedProteins(it->second.first, it->second.second, sa, pid);
		string auxRes = "";
		auxRes = proteina.substr(sa[it->second.first],it->second.second) + " " + to_string(cant);
		//~ writeResultsHeatMap(fileName + ".out", auxRes);
		writeLineInFile(fileName + ".out", auxRes);
		//~ cout << auxRes <<endl;	
	}
	cout << "finish processing file: " << fileName << endl;

}

void mainComputeAllFamiliesNE() {
	vector< string > allFamilies = listDirsFromPath("/home/naciraa/Documents/tesis/Datos/canonicalAminoacidsProteins/familyDataset/");
	sort(allFamilies.begin(), allFamilies.end());
	
	for (int i = 0; i < allFamilies.size(); i++){
		cout << "computing family: " << allFamilies[i]<< endl;
		emptyFiles();
		vector< string > files = listFileFromPath(allFamilies[i] + "/");
		string auxProteinSet = concatenateProteins(files);
		proteina = "";
		//~ cout << "proteina: " << proteina << endl;
		proteina = auxProteinSet;
		
		vector< int > sai;
		vector< int > lcp;
		alfabeto.clear();
		//~ cout << "longitud alfabeto: " << alfabeto.size() <<endl;
		
		calculateSAIandLCPForSetWithStruct(proteina, sai, lcp);
		vector< pair< int, pair< int, int > > > intervals;
		findIntervalsPaper(sai, lcp, intervals);
		processIntervals(intervals, sai, lcp);
		string aux = "MR_SMR_" + allFamilies[i].substr(78)+ ".csv";
		cout << "copying file: " << aux <<endl;
		char cstr[aux.size() + 1];
		strcpy(cstr, aux.c_str());
		if(rename("MR_SMR.csv", cstr) != 0){
			cout << "File renamed fail"<<endl;
		} else {
			cout << "File renamed successfully" <<endl;
		}
		string aux2 = "MR_NE_" + allFamilies[i].substr(78)+ ".csv";
		cout << "copying file: " << aux2 <<endl;
		char cstr2[aux2.size() + 1];
		strcpy(cstr2, aux2.c_str());
		if(rename("MR_NE.csv", cstr2) != 0){
			cout << "File renamed fail"<<endl;
		} else {
			cout << "File renamed successfully" <<endl;
		}
		string aux3 = "MR_NN_" + allFamilies[i].substr(78)+ ".csv"; 
		cout << "copying file: " << aux <<endl;
		char cstr3[aux3.size() + 1];
		strcpy(cstr3, aux3.c_str());
		if(rename("MR_NN.csv", cstr3) != 0){
			cout << "File renamed fail"<<endl;
		} else {
			cout << "File renamed successfully" <<endl;
		}
		cout << "end family " << allFamilies[i] <<endl;
	}
	
}

void mainConcatProtein() {
	//~ vector< string > files = listFile();
    //~ cout << "files listed " <<endl;
    //~ string auxProteinSet = concatenateProteins(files);
    //~ cout << auxProteinSet <<endl;
    //~ proteina = auxProteinSet;
    
    //~ cout << "size: " << auxProteinSet.size() << "\n";
    //~ cout << "length: " << auxProteinSet.length() << "\n";
    //~ cout << "capacity: " << auxProteinSet.capacity() << "\n";
    //~ cout << "max_size: " << auxProteinSet.max_size() << "\n";
    
	vector< int > sai;
	vector< int > lcp;
	//~ vector< int > sai2;
	//~ vector< int > lcp2;
	//~ vector< int > maxRep;
	//~ proteina = "ACTCTCTGCTCT{";
	//~ proteina = auxProteinSet;
	//~ calculateSAIandLCP(proteina, sai, lcp);
	//~ for(int i=0; i<sai.size(); ++i){
		//~ cout << i << ": " << proteina.substr(sai[i]) <<endl;
	//~ }
	//~ printVectorContent(sai);
	//~ printVectorContent(lcp);
	//~ proteina = "ACTCTCTGCTCT{BANANA{ANANA{";
	cout << "calculando SA y LCP ..." <<endl;
	cout << "longitud alfabeto: " << alfabeto.size() <<endl;
	proteina = "";
	readWholeProtein(proteina);
	cout << proteina << endl;
	calculateSAIandLCPForSetWithStruct(proteina, sai, lcp);
	cout << "longitud alfabeto: " << alfabeto.size() <<endl;
	//~ for(int i=0; i<sai.size(); ++i){
		//~ cout << i << ": " << proteina.substr(sai[i]) <<endl;
	//~ }
	//~ printVectorContent(lcp);
	vector< pair< int, pair< int, int > > > intervals;
	cout << "buscando intervalos ..." <<endl;
	//~ findIntervals(sai, lcp, intervals);
	findIntervalsPaper(sai, lcp, intervals);
	//~ cout << "intervals size: " << intervals.size()<< endl;
	//~ for(int i=0; i<intervals.size(); ++i){
		//~ cout << "len: " << intervals[i].first << "[inicio: " << intervals[i].second.first << ", fin: " << intervals[i].second.second << "]" <<endl;
	//~ }
	cout << "procesando intervalos ..." <<endl;
	processIntervals(intervals, sai, lcp);
	cout << "calculando diferencia smr ..." <<endl;
	set< string > mySMRSet = processFileToSet("MR_SMR.csv");
	set< string > theirSMRSet = processFileToSet("./output/MR_SMR.csv");
	cout << "mySMRSet contains: " << mySMRSet.size() << endl;
	cout << "theirSMRSet contains: " << theirSMRSet.size() << endl;
	set < string > setRes1, setRes2;
	set_difference(mySMRSet.begin(), mySMRSet.end(), theirSMRSet.begin(), theirSMRSet.end(), inserter(setRes1, setRes1.end()));
	cout << "The set difference has " << setRes1.size() << " elements:\n";
	set_symmetric_difference(mySMRSet.begin(), mySMRSet.end(), theirSMRSet.begin(), theirSMRSet.end(), inserter(setRes2, setRes2.end()));
	cout << "The symmetric difference has " << setRes2.size() << " elements:\n";

	cout << "calculando diferencia NE ..." <<endl;
	set< string > myNESet = processFileToSet("MR_NE.csv");
	set< string > theirNESet = processFileToSet("./output/MR_NE.csv");
	cout << "myNESet contains: " << myNESet.size() << endl;
	cout << "theirNESet contains: " << theirNESet.size() << endl;
	set < string > setRes3, setRes4;
	set_difference(myNESet.begin(), myNESet.end(), theirNESet.begin(), theirNESet.end(), inserter(setRes3, setRes3.end()));
	cout << "The set difference has " << setRes3.size() << " elements:\n";
	set_symmetric_difference(myNESet.begin(), myNESet.end(), theirNESet.begin(), theirNESet.end(), inserter(setRes4, setRes4.end()));
	cout << "The symmetric difference has " << setRes4.size() << " elements:\n";

	cout << "calculando diferencia NN ..." <<endl;
	set< string > myNNSet = processFileToSet("MR_NN.csv");
	set< string > theirNNSet = processFileToSet("./output/MR_NN.csv");
	cout << "myNNSet contains: " << myNNSet.size() << endl;
	cout << "theirNNSet contains: " << theirNNSet.size() << endl;
	set < string > setRes5, setRes6;
	set_difference(myNNSet.begin(), myNNSet.end(), theirNNSet.begin(), theirNNSet.end(), inserter(setRes5, setRes5.end()));
	cout << "The set difference has " << setRes5.size() << " elements:\n";
	set_symmetric_difference(myNNSet.begin(), myNNSet.end(), theirNNSet.begin(), theirNNSet.end(), inserter(setRes6, setRes6.end()));
	cout << "The symmetric difference has " << setRes6.size() << " elements:\n";
	
}

void mainDiffProteinFromFamily(string path, string path2) {
	emptyFiles();
	vector< string > files = listFileFromPath(path);
    cout << "files listed " <<endl;
    string auxProteinSet = concatenateProteins(files);
    cout << auxProteinSet <<endl;
    proteina = "";
    proteina = auxProteinSet;
    
    cout << "size: " << auxProteinSet.size() << "\n";
    cout << "length: " << auxProteinSet.length() << "\n";
    cout << "capacity: " << auxProteinSet.capacity() << "\n";
    cout << "max_size: " << auxProteinSet.max_size() << "\n";
    
	vector< int > sai;
	vector< int > lcp;
	cout << "calculando SA y LCP ..." <<endl;
	cout << "longitud alfabeto: " << alfabeto.size() <<endl;
	
	cout << proteina << endl;
	calculateSAIandLCPForSetWithStruct(proteina, sai, lcp);
	cout << "longitud alfabeto: " << alfabeto.size() <<endl;
	
	vector< pair< int, pair< int, int > > > intervals;
	cout << "buscando intervalos ..." <<endl;
	findIntervalsPaper(sai, lcp, intervals);
	cout << "procesando intervalos ..." <<endl;
	processIntervals(intervals, sai, lcp);
	set< string > mySMRSet = processFileToSet("MR_SMR.csv");
	set< string > myNESet = processFileToSet("MR_NE.csv");
	set< string > myNNSet = processFileToSet("MR_NN.csv");
	
	emptyFiles();
	
	vector< string > files2 = listFileFromPath(path2);
    cout << "files2 listed " <<endl;
    string auxProteinSet2 = concatenateProteins(files2);
    cout << auxProteinSet2 <<endl;
    proteina = "";
    proteina = auxProteinSet2;
    
    cout << "size: " << auxProteinSet2.size() << "\n";
    cout << "length: " << auxProteinSet2.length() << "\n";
    cout << "capacity: " << auxProteinSet2.capacity() << "\n";
    cout << "max_size: " << auxProteinSet2.max_size() << "\n";
    
	vector< int > sai2;
	vector< int > lcp2;
	alfabeto.clear();
	cout << "calculando SA y LCP ..." <<endl;
	cout << "longitud alfabeto: " << alfabeto.size() <<endl;
	
	cout << proteina << endl;
	calculateSAIandLCPForSetWithStruct(proteina, sai2, lcp2);
	cout << "longitud alfabeto: " << alfabeto.size() <<endl;
	
	vector< pair< int, pair< int, int > > > intervals2;
	cout << "buscando intervalos ..." <<endl;
	findIntervalsPaper(sai2, lcp2, intervals2);
	cout << "procesando intervalos ..." <<endl;
	processIntervals(intervals2, sai2, lcp2);
	set< string > mySMRSet2 = processFileToSet("MR_SMR.csv");
	set< string > myNESet2 = processFileToSet("MR_NE.csv");
	set< string > myNNSet2 = processFileToSet("MR_NN.csv");
	
	//~ set< string > theirNNSet = processFileToSet("./output/MR_NN.csv");
	//~ cout << "myNNSet contains: " << myNNSet.size() << endl;
	//~ cout << "theirNNSet contains: " << theirNNSet.size() << endl;
	//~ set < string > setRes5, setRes6;
	//~ set_difference(myNNSet.begin(), myNNSet.end(), theirNNSet.begin(), theirNNSet.end(), inserter(setRes5, setRes5.end()));
	//~ cout << "The set difference has " << setRes5.size() << " elements:\n";
	//~ set_symmetric_difference(myNNSet.begin(), myNNSet.end(), theirNNSet.begin(), theirNNSet.end(), inserter(setRes6, setRes6.end()));
	//~ cout << "The symmetric difference has " << setRes6.size() << " elements:\n";
	
	
}

void mainCompare2Tries() {
	cout << "caso iguales: " << endl;
	trie * myTrie1 = new trie();
	trie * myTrie2 = new trie();
	
	insertString(myTrie1, "abcd");
	insertString(myTrie1, "abef");
	insertString(myTrie1, "bcd");
	insertString(myTrie1, "a");
	
	insertString(myTrie2, "abcd");
	insertString(myTrie2, "abef");
	insertString(myTrie2, "bcd");
	insertString(myTrie2, "a");
	
	compare2Tries(myTrie1, myTrie2);
	
	cout << "---------------------------- " << endl;
	cout << "caso distintos disjuntos: " << endl;
	trie * myTrie3 = new trie();
	trie * myTrie4 = new trie();
	
	insertString(myTrie3, "abcd");
	insertString(myTrie3, "abef");
	insertString(myTrie3, "bcd");
	insertString(myTrie3, "a");
	
	insertString(myTrie4, "tuvw");
	insertString(myTrie4, "tuyz");
	insertString(myTrie4, "wxy");
	insertString(myTrie4, "t");
	
	compare2Tries(myTrie3, myTrie4);
	
	cout << "---------------------------- " << endl;
	cout << "caso distintos con interseccion: " << endl;
	trie * myTrie5 = new trie();
	trie * myTrie6 = new trie();
	
	insertString(myTrie5, "abcd");
	insertString(myTrie5, "abef");
	insertString(myTrie5, "bcd");
	insertString(myTrie5, "a");
	
	insertString(myTrie6, "abcd");
	insertString(myTrie6, "tuyz");
	insertString(myTrie6, "bcd");
	insertString(myTrie6, "t");
	
	compare2Tries(myTrie5, myTrie6);
	
	
	cout << "---------------------------- " << endl;
	cout << "caso 2 distintos con interseccion: " << endl;
	trie * myTrie7 = new trie();
	trie * myTrie8 = new trie();
	
	insertString(myTrie7, "a");
	insertString(myTrie7, "bcde");
	insertString(myTrie7, "c");
	
	insertString(myTrie8, "a");
	insertString(myTrie8, "tuyz");
	insertString(myTrie8, "cd");
	
	compare2Tries(myTrie7, myTrie8);
	compare2Tries(myTrie8, myTrie7);
	double res =  (1.0*compare2Tries(myTrie8, myTrie7) / (countTree(myTrie7->ini)-1 + countTree(myTrie8->ini)-1));
	cout << res << endl;
	//~ countTree(myTrie8);
}

void mainCompare2Families(string path1, string path2) {
	cout << "comparing families: " << endl;
	cout << path1 << endl;
	cout << path2 << endl;
	emptyFiles();
	//~ vector< string > files = listFileFromPath(path1);
    //~ string auxProteinSet = concatenateProteins(files);
    //~ proteina = "";
    //cout << "proteina: " << proteina << endl;
    //~ proteina = auxProteinSet;
    
	//~ vector< int > sai;
	//~ vector< int > lcp;
	//~ alfabeto.clear();
	//cout << "longitud alfabeto: " << alfabeto.size() <<endl;
	
	//~ calculateSAIandLCPForSetWithStruct(proteina, sai, lcp);
	//~ vector< pair< int, pair< int, int > > > intervals;
	//~ findIntervalsPaper(sai, lcp, intervals);
	//~ processIntervals(intervals, sai, lcp);
	//~ set< string > mySMRSet = processFileToSet("MR_SMR.csv");
	//~ set< string > myNESet = processFileToSet("MR_NE.csv");
	//~ set< string > myNNSet = processFileToSet("MR_NN.csv");
	
	string filename = "MR-Families/MR_NE_" + path1.substr(78) + ".csv";
	cout << "filename mr ne: " << filename << endl;
	set< string > myNESet = processFileToSet(filename);
	
	trie * myTrie = new trie();
	vector< set< string > > dicc(10000000);
	createDicctionaryOfSmr(myNESet, dicc);
	
	for (int i = 1; i < 12; i++){
		createTrieOfSmr(dicc[i], myTrie);
		//~ createTrieOfSmrPref(dicc[i], myTrie);
		//~ createTrieOfSmr(myNESet, myTrie);
	}
	
	//~ cout << "trie ready... " << endl;
	//~ cout << "cant nodos " << countTree(myTrie->ini) <<endl;
	//Process path2 

	emptyFiles();
	//~ vector< string > files2 = listFileFromPath(path2);
    //~ string auxProteinSet2 = concatenateProteins(files2);
    //~ proteina = "";
    //cout << "proteina: " << proteina << endl;
    //~ proteina = auxProteinSet2;
    
    
	//~ vector< int > sai2;
	//~ vector< int > lcp2;
	//~ alfabeto.clear();
	//cout << "longitud alfabeto: " << alfabeto.size() <<endl;
	//~ calculateSAIandLCPForSetWithStruct(proteina, sai2, lcp2);
	//~ vector< pair< int, pair< int, int > > > intervals2;
	//~ findIntervalsPaper(sai2, lcp2, intervals2);
	//~ processIntervals(intervals2, sai2, lcp2);
	//~ set< string > mySMRSet2 = processFileToSet("MR_SMR.csv");
	//~ set< string > myNESet2 = processFileToSet("MR_NE.csv");
	//~ set< string > myNNSet2 = processFileToSet("MR_NN.csv");
	
	string filename2 = "MR-Families/MR_NE_" + path2.substr(78) + ".csv";
	cout << "filename mr ne: " << filename2 << endl;
	set< string > myNESet2 = processFileToSet(filename2);
	
	trie * myTrie2 = new trie();
	vector< set< string > > dicc2(10000000);
	createDicctionaryOfSmr(myNESet2, dicc2);
	
	for (int i = 1; i < 12; i++){
		createTrieOfSmr(dicc2[i], myTrie2);
		//~ createTrieOfSmrPref(dicc2[i], myTrie2);
		//~ createTrieOfSmr(myNESet2, myTrie2);
	}
	
	//~ cout << "trie ready... " << endl;
	//~ cout << "cant nodos " << countTree(myTrie2->ini) <<endl;
	//~ int metric = compare2TreeMetric(myTrie->ini, myTrie2->ini).first;
	//~ double res =  (1.0*metric / (countTree(myTrie->ini)-1 + countTree(myTrie2->ini)-1));
	//~ cout << res << endl;
	calculateMetrics (myTrie, myTrie2);
	validateTotalFootPrint(myTrie);
	validateTotalFootPrint(myTrie2);
	//~ deleteTrie(myTrie);
	//~ deleteTrie(myTrie2);
	
}

void mainCompare2FamiliesByLevel(string path1, string path2, int baseLevel, int maxLevel) {
	cout << "comparing families: " << endl;
	cout << path1 << endl;
	cout << path2 << endl;
	emptyFiles();
	
	string filename = "MR-Families/MR_NE_" + path1.substr(78) + ".csv";
	cout << "filename mr ne: " << filename << endl;
	set< string > myNESet = processFileToSet(filename);
	
	trie * myTrie = new trie();
	vector< set< string > > dicc(10000000);
	createDicctionaryOfSmr(myNESet, dicc);
	
	for (int i = 1; i < 12; i++){
		createTrieOfSmr(dicc[i], myTrie);
	}
	emptyFiles();
	string filename2 = "MR-Families/MR_NE_" + path2.substr(78) + ".csv";
	cout << "filename mr ne: " << filename2 << endl;
	set< string > myNESet2 = processFileToSet(filename2);
	
	trie * myTrie2 = new trie();
	vector< set< string > > dicc2(10000000);
	createDicctionaryOfSmr(myNESet2, dicc2);
	
	for (int i = 1; i < 12; i++){
		createTrieOfSmr(dicc2[i], myTrie2);
	}
	vector<string> files;
	files.push_back("results.csv");
	calculateMetricsByLevel (myTrie, myTrie2, baseLevel, maxLevel, files);
	validateTotalFootPrint(myTrie);
	validateTotalFootPrint(myTrie2);
	
}

void mainConcatProteinFromFamily(string path) {
	emptyFiles();
	//~ vector< string > files = listFileFromPath("/home/naciraa/Documents/tesis/Datos/canonicalAminoacidsProteins/familyDataset/Nebulin/");
	vector< string > files = listFileFromPath(path);
    cout << "files listed " <<endl;
    string auxProteinSet = concatenateProteins(files);
    cout << auxProteinSet <<endl;
    proteina = "";
    proteina = auxProteinSet;
    
    cout << "size: " << auxProteinSet.size() << "\n";
    cout << "length: " << auxProteinSet.length() << "\n";
    cout << "capacity: " << auxProteinSet.capacity() << "\n";
    cout << "max_size: " << auxProteinSet.max_size() << "\n";
    
	vector< int > sai;
	vector< int > lcp;
	cout << "calculando SA y LCP ..." <<endl;
	cout << "longitud alfabeto: " << alfabeto.size() <<endl;
	
	cout << proteina << endl;
	calculateSAIandLCPForSetWithStruct(proteina, sai, lcp);
	cout << "longitud alfabeto: " << alfabeto.size() <<endl;
	//~ for(int i=0; i<sai.size(); ++i){
		//~ cout << i << ": " << proteina.substr(sai[i]) <<endl;
	//~ }
	//~ printVectorContent(lcp);
	vector< pair< int, pair< int, int > > > intervals;
	cout << "buscando intervalos ..." <<endl;
	//~ findIntervals(sai, lcp, intervals);
	findIntervalsPaper(sai, lcp, intervals);
	cout << "procesando intervalos ..." <<endl;
	processIntervals(intervals, sai, lcp);
	//~ cout << "calculando diferencia smr ..." <<endl;
	set< string > mySMRSet = processFileToSet("MR_SMR.csv");
	//~ set< string > theirSMRSet = processFileToSet("./output/MR_SMR.csv");
	//~ cout << "mySMRSet contains: " << mySMRSet.size() << endl;
	//~ cout << "theirSMRSet contains: " << theirSMRSet.size() << endl;
	//~ set < string > setRes1, setRes2;
	//~ set_difference(mySMRSet.begin(), mySMRSet.end(), theirSMRSet.begin(), theirSMRSet.end(), inserter(setRes1, setRes1.end()));
	//~ cout << "The set difference has " << setRes1.size() << " elements:\n";
	//~ set_symmetric_difference(mySMRSet.begin(), mySMRSet.end(), theirSMRSet.begin(), theirSMRSet.end(), inserter(setRes2, setRes2.end()));
	//~ cout << "The symmetric difference has " << setRes2.size() << " elements:\n";

	//~ cout << "calculando diferencia NE ..." <<endl;
	set< string > myNESet = processFileToSet("MR_NE.csv");
	//~ set< string > theirNESet = processFileToSet("./output/MR_NE.csv");
	//~ cout << "myNESet contains: " << myNESet.size() << endl;
	//~ cout << "theirNESet contains: " << theirNESet.size() << endl;
	//~ set < string > setRes3, setRes4;
	//~ set_difference(myNESet.begin(), myNESet.end(), theirNESet.begin(), theirNESet.end(), inserter(setRes3, setRes3.end()));
	//~ cout << "The set difference has " << setRes3.size() << " elements:\n";
	//~ set_symmetric_difference(myNESet.begin(), myNESet.end(), theirNESet.begin(), theirNESet.end(), inserter(setRes4, setRes4.end()));
	//~ cout << "The symmetric difference has " << setRes4.size() << " elements:\n";

	//~ cout << "calculando diferencia NN ..." <<endl;
	set< string > myNNSet = processFileToSet("MR_NN.csv");
	//~ set< string > theirNNSet = processFileToSet("./output/MR_NN.csv");
	//~ cout << "myNNSet contains: " << myNNSet.size() << endl;
	//~ cout << "theirNNSet contains: " << theirNNSet.size() << endl;
	//~ set < string > setRes5, setRes6;
	//~ set_difference(myNNSet.begin(), myNNSet.end(), theirNNSet.begin(), theirNNSet.end(), inserter(setRes5, setRes5.end()));
	//~ cout << "The set difference has " << setRes5.size() << " elements:\n";
	//~ set_symmetric_difference(myNNSet.begin(), myNNSet.end(), theirNNSet.begin(), theirNNSet.end(), inserter(setRes6, setRes6.end()));
	//~ cout << "The symmetric difference has " << setRes6.size() << " elements:\n";
	
	trie * myTrie = new trie();
	vector< set< string > > dicc(10000000);
	cout << "creating dicctionary... " << endl; 
	createDicctionaryOfSmr(myNESet, dicc);
	cout << "dicctionary ready... dicc[3]: " << dicc[3].size()<< endl;
	cout << "creating trie... " << endl; 
	cout << "#NEXUS" <<endl;
	cout << "" <<endl;
	cout << "Begin trees;" <<endl;
	cout << "" <<endl;
	
	for (int i = 1; i < 12; i++){
		createTrieOfSmr(dicc[i], myTrie);
	}
	
	cout << "trie ready... " << endl;
	//~ printFootPrint (myTrie, 3);
	//~ printFootPrint (myTrie, 2, 3);
	//~ printTrieAsGraph (myTrie, 2, 3);
	printTrieAsGraphStr (myTrie, 3);
	cout << "cant nodos " << countTree(myTrie->ini) <<endl;
	//~ cout << "fin" <<endl;	
	//~ cout << "End;" <<endl;

}


void mainCoverageExample(){
	vector< int > sai;
	vector< int > lcp;
	//~ proteina = "ACTCTCTGCTCT{";
	//~ proteina = "ACTCTCTGCTCT{BANANA{ANANA{";
	proteina = "abcdeabcdfbcdebcd{";
	cout << "calculando SA y LCP ..." <<endl;
	cout << "longitud alfabeto: " << alfabeto.size() <<endl;
	calculateSAIandLCPForSetWithStruct(proteina, sai, lcp);
	cout << "longitud alfabeto: " << alfabeto.size() <<endl;
	vector< pair< int, pair< int, int > > > intervals;
	cout << "buscando intervalos ..." <<endl;
	findIntervalsPaper(sai, lcp, intervals);
	cout << "procesando intervalos ..." <<endl;
	processIntervals(intervals, sai, lcp);
	set< string > mySMRSet = processFileToSet("MR_SMR.csv");
	cout << "mySMRSet contains:";
	set<string>::iterator it;
	for (it=mySMRSet.begin(); it!=mySMRSet.end(); ++it){
		cout << ' ' << *it <<endl;
	}
	
	vector< set< string > > dicc(proteina.length());
	createDicctionaryOfSmr(mySMRSet, dicc);
	float familiarity = 0.0;
	//~ for(int i=1; i<dicc.size()-1; ++i){
		//~ familiarity += calculateCoverage(proteina, dicc[i]);
	//~ }
	//~ familiarity += ( 1 + calculateCoverage(proteina, dicc[dicc.size()-1]) )/2;
	ofstream familiarityFile;
	ofstream coverageFile;
	familiarityFile.open ("familiarity.csv", ofstream::app);
	coverageFile.open ("coverage.csv", ofstream::app);
	cout << "files ready.. calculating coverage.." <<endl;
	cout << "familiarity: " << familiarity <<endl;
	familiarity = 0.0;
	familiarity = calculateCoverageKMP(proteina, dicc, coverageFile);
	cout << "writing " << familiarity << " to familiarity file"<<endl;
	familiarityFile << familiarity << "\n";
	cout << "writing separator ; to familiarity file"<<endl;
	familiarityFile << ';' << "\n";
		
	//~ for(int i=1; i<dicc.size()-1; ++i){
		//~ familiarity += calculateCoverageKMP(proteina, dicc[i]);
	//~ }
	//~ familiarity += ( 1 + calculateCoverageKMP(proteina, dicc[dicc.size()-1]) )/2;
	
	//~ cout << "familiarity KMP: " << familiarity <<endl;
	cout << "fin" <<endl;
}

bool myComp(int i, int j){
	return proteina.substr(i) < proteina.substr(j);
}

void calculateSAandLCP(string &s, vector< string > &sa, vector< int > &lcp) {
	for(int i=0; i<s.length(); ++i){
		sa.push_back(s.substr(i));
	}
	sort(sa.begin(), sa.end());
	lcp.push_back(0);
	for(int i=0; i<sa.size()-1; ++i){
		int aux_min_len = min(sa[i].length(), sa[i+1].length());
		int aux_lcp = 0;
		for(int j=0; j<aux_min_len; ++j){
			if(sa[i][j] == sa[i+1][j]){
				aux_lcp++;
			} else {
				break;
			}
		}
		lcp.push_back(aux_lcp);
	}
}

void calculateSAIandLCP(string &s, vector< int > &sai, vector< int > &lcp) {
	alfabeto.clear();
	for(int i=0; i<s.length()-1; ++i){
		sai.push_back(i);
		alfabeto.insert(s[i]);
	}
	sort(sai.begin(), sai.end(), myComp);
	lcp.push_back(0);
	for(int i=0; i<sai.size()-1; ++i){
		int aux_min_len = min(s.substr(sai[i]).length(), s.substr(sai[i+1]).length());
		int aux_lcp = 0;
		//~ cout << "aux_min_len: " << aux_min_len << endl;
		//~ cout << s.substr(sai[i]) <<  " " << s.substr(sai[i+1])<<endl;
		for(int j=0; j<aux_min_len; ++j){
			if(s.substr(sai[i])[j] == s.substr(sai[i+1])[j]){
				aux_lcp++;
			} else {
				break;
			}
		}
		lcp.push_back(aux_lcp);
	}
}

bool myCompForSet(int i, int j){
	//~ if (proteina.substr(i)[0] == '+' && proteina.substr(j)[0] == '+') {
		//~ return false;
	//~ }
	return proteina.substr(i) < proteina.substr(j);
}

void calculateSAIandLCPForSet(string &s, vector< int > &sai, vector< int > &lcp) {
	alfabeto.clear();
	for(int i=0; i<s.length()-1; ++i){
		sai.push_back(i);
		alfabeto.insert(s[i]);
	}
	sort(sai.begin(), sai.end(), myCompForSet);
	lcp.push_back(0);
	for(int i=0; i<sai.size()-1; ++i){
		int aux_min_len = min(s.substr(sai[i]).length(), s.substr(sai[i+1]).length());
		int aux_lcp = 0;
		//~ cout << "aux_min_len: " << aux_min_len << endl;
		//~ cout << s.substr(sai[i]) <<  " " << s.substr(sai[i+1])<<endl;
		for(int j=0; j<aux_min_len; ++j){
			if(s.substr(sai[i])[j] == '{' && s.substr(sai[i+1])[j] == '{'){					
				aux_lcp = 0;
				break;
			}
			if(s.substr(sai[i])[j] == s.substr(sai[i+1])[j]){
				aux_lcp++;
			} else {
				break;
			}
		}
		lcp.push_back(aux_lcp);
	}
}

void calculateSAIandLCPForSetWithStruct(string &s, vector< int > &sai, vector< int > &lcp) {
	alfabeto.clear();
	for(int i=0; i<s.length()-1; ++i){
		alfabeto.insert(s[i]);
	}
	cout << "armando suffix array " << endl;
	SuffixArray suffix(s);
	sai = suffix.GetMiSuffix();
	cout << "suffix array listo, generando lcp.. " << endl;
	lcp.push_back(0);
	for(int i=0; i<sai.size()-1; ++i){
		//~ int aux_min_len = min(s.substr(sai[i]).length(), s.substr(sai[i+1]).length());
		int aux_min_len = min(s.size()-sai[i], s.size()-sai[i+1]);
		int aux_lcp = 0;
		//~ cout << "aux_min_len: " << aux_min_len << endl;
		//~ cout << s.substr(sai[i]) <<  " " << s.substr(sai[i+1])<<endl;
		for(int j=0; j<aux_min_len; ++j){
			if(s[sai[i] + j] == '{' && s[sai[i+1] + j] == '{'){					
				//~ aux_lcp = 0;
				break;
			}
			if(s[sai[i] +j] == s[sai[i+1] + j]){
				aux_lcp++;
			} else {
				break;
			}
		}
		lcp.push_back(aux_lcp);
	}
}


void findIntervals(vector< int > &sai, vector< int > &lcp, vector< pair< int, pair< int, int > > > &intervals){
	//~ cout << "proces Intervals" << endl;
	int fin, inicio;
	for(int len=1; len<proteina.length(); ++len){
		//~ cout << "len: " << len << endl;
		fin = -1;
		for(int i=0; i<lcp.size()-1; ++i){
			if (lcp[i] == len && lcp[i+1] < len) {
				//~ cout << "fin " << i << endl;
				fin = i;
				inicio = -1;
				bool isInterval = true;
				for(int j=0; j<fin; ++j){
					bool isPrefix = true;
					for(int k=0; k<len; ++k){
						//~ cout << "j: " << j << " k: " << k << " " << proteina[sai[j]+k] << " ? " << proteina[sai[fin]+k] <<endl;
						if (proteina[sai[j]+k] != proteina[sai[fin]+k]){
							isPrefix = false;
							break;
						}
					}
					//~ cout << "j: " << j << " isPrefix: " << isPrefix << " lcp[j]: " << lcp[j] << endl;
					if (isPrefix && lcp[j] < len){
						inicio = j;
					}
					//~ cout << "inicio: " << inicio << endl;
					if (inicio != -1) {
						if (inicio < j){
							if(lcp[j] < len) {
								isInterval = false;
							}
						}
					} else {
						isInterval = true;
					}
				}
				
				if (inicio != -1 && fin != -1 && isInterval) {
					//~ cout << "tengo par" << endl;
					//~ cout << "len: " << len << " inicio: " << inicio << " , fin: " << fin <<endl;
					pair< int, int > interval = make_pair (inicio, fin);
					pair<int, pair< int, int > > auxInterval = make_pair(len, interval);
					intervals.push_back(auxInterval);
				}
				
			}
		}
		
		
	}
	
}

void findIntervalsPaper(vector< int > &sai, vector< int > &lcp, vector< pair< int, pair< int, int > > > &intervals){
	stack<pair<int, pair<int, int>>> mystack;
	mystack.push(make_pair(0, make_pair(0, -1)));
	for(int i=1; i<lcp.size(); ++i){
		int leftBound = i-1;
		while(lcp[i] < mystack.top().first){
			mystack.top().second.second = i-1;
			pair<int, pair<int, int>> interval = mystack.top();
			mystack.pop();
			//~ cout << "report interval: lcp=" << interval.first << " lb=" << interval.second.first << " rb=" << interval.second.second <<endl; 
			intervals.push_back(interval);
			leftBound = interval.second.first;
		}
		if (lcp[i] > mystack.top().first){
			mystack.push(make_pair(lcp[i], make_pair(leftBound, -1)));
		}
		
	}
	
}

void processIntervals(vector< pair< int, pair< int, int > > > &intervals,
					vector< int > &sa,
					vector< int > &lcp){
					//~ vector< int > &lcp,
					//~ vector< pair< int, pair< int, int > > > &super,
					//~ vector< pair< int, pair< int, int > > > &local,
					//~ vector< int > &nested,
					//~ vector< int > &notNested){
	
	for(int i=0; i<intervals.size(); ++i){
		unordered_map < char, int > lctt;
		//~ vector < int > lctt (300, 0);
		unordered_map < char, int > rctt;
		//~ vector < int > rctt (300, 0);
		bool tmm = false;
		int ini = intervals[i].second.first;
		int fin = intervals[i].second.second;
		int l = intervals[i].first;
		char sigma = proteina[sa[ini]-1];
		//~ cout << "ini: " << ini << " fin: " << fin << " l: " << l << " sa[ini]: " << proteina.substr(sa[ini], l) << " sigma: " << sigma << endl;
		for(int k=ini; k<=fin; ++k){
			char sigmaLeft = proteina[sa[k]-1];
			char sigmaRight = proteina[sa[k]+l];
			//~ lctt[sigmaLeft] = lctt[sigmaLeft] + 1;
			//~ rctt[sigmaRight] = rctt[sigmaRight] +1;
			lctt[sigmaLeft]++;
			rctt[sigmaRight]++;
			if (sigma != sigmaLeft || sigma == '{') {
				tmm = true;
			}
			//~ cout << "k: " << k << " sa[k] " << sa[k] << " sigmaleft: " << sigmaLeft << " sigmaRight: " << sigmaRight << " lctt[sigmaLeft]: " << lctt[sigmaLeft] << " rctt[sigmaRight]: " << rctt[sigmaRight] << " tmm " << tmm <<endl;
		}
		lctt['{'] = 1;
		rctt['{'] = 1;
		//~ if (fin-ini+1 <= alfabeto.size()) {
			bool aux = true;
			unordered_set < char >::iterator it;
			for (it=alfabeto.begin(); it!=alfabeto.end(); ++it){
				if (lctt[*it] > 1 || rctt[*it] > 1){
					aux = false;
					break;
				}
			}
			//~ for(int i; i<lenAlfabeto; ++i){
				//~ if (lctt[alfabeto[i]] > 1 || rctt[alfabeto[i]] > 1){
					//~ aux = false;
					//~ break;
				//~ }
			//~ }
			if (aux){
				//~ cout << "super: " << proteina.substr(ini,fin-ini) << endl;
				//~ cout << "super: " << proteina.substr(ini,l) << endl;
				//~ pair< int, int > interval = make_pair (ini, fin);
				//~ pair<int, pair< int, int > > auxInterval = make_pair(l, interval);
				//~ super.push_back(auxInterval);
				processSuper(proteina.substr(sa[ini],l));
			} else {
		//~ }
				if (tmm == true) {
					//~ pair< int, int > interval = make_pair (ini, fin);
					//~ pair<int, pair< int, int > > auxInterval = make_pair(l, interval);
					//~ local.push_back(auxInterval);
					bool isNested = true;
					for(int k=ini; k<=fin; ++k){
						char sigmaLeft = proteina[sa[k]-1];
						char sigmaRight = proteina[sa[k]+l];
						//imprimir en el archivo desde k hasta k+l-1 inclusive
						//~ if (lctt[sigmaLeft] > 1 || rctt[sigmaRight] > 1) {
							//~ cout << "k: " << k << " nested: "<< proteina.substr(k,l) <<endl;
							//~ nested.push_back(k);
							//~ processNested(proteina.substr(sa[k],l));
						//~ } else {
							//~ cout << "k: " << k << " notNested: " << proteina.substr(k,l) <<endl;
							//~ notNested.push_back(k);
							//~ processNonNested(proteina.substr(sa[k],l));
						//~ }
						if (lctt[sigmaLeft] <= 1 && rctt[sigmaRight] <= 1){
							isNested = false;
						}
					}
					if (isNested) {
						processNested(proteina.substr(sa[ini],l));
					} else {
						processNonNested(proteina.substr(sa[ini],l));
					}	
				}
			}
		
		
	}
	
}


void processIntervalsWithAffectedProteins(vector< pair< int, pair< int, int > > > &intervals,
					vector< int > &sa,
					vector< int > &lcp){
					//~ vector< int > &lcp,
					//~ vector< pair< int, pair< int, int > > > &super,
					//~ vector< pair< int, pair< int, int > > > &local,
					//~ vector< int > &nested,
					//~ vector< int > &notNested){
	
	for(int i=0; i<intervals.size(); ++i){
		unordered_map < char, int > lctt;
		//~ vector < int > lctt (300, 0);
		unordered_map < char, int > rctt;
		//~ vector < int > rctt (300, 0);
		bool tmm = false;
		int ini = intervals[i].second.first;
		int fin = intervals[i].second.second;
		int l = intervals[i].first;
		char sigma = proteina[sa[ini]-1];
		//~ cout << "ini: " << ini << " fin: " << fin << " l: " << l << " sa[ini]: " << proteina.substr(sa[ini], l) << " sigma: " << sigma << endl;
		for(int k=ini; k<=fin; ++k){
			char sigmaLeft = proteina[sa[k]-1];
			char sigmaRight = proteina[sa[k]+l];
			//~ lctt[sigmaLeft] = lctt[sigmaLeft] + 1;
			//~ rctt[sigmaRight] = rctt[sigmaRight] +1;
			lctt[sigmaLeft]++;
			rctt[sigmaRight]++;
			if (sigma != sigmaLeft || sigma == '{') {
				tmm = true;
			}
			//~ cout << "k: " << k << " sa[k] " << sa[k] << " sigmaleft: " << sigmaLeft << " sigmaRight: " << sigmaRight << " lctt[sigmaLeft]: " << lctt[sigmaLeft] << " rctt[sigmaRight]: " << rctt[sigmaRight] << " tmm " << tmm <<endl;
		}
		lctt['{'] = 1;
		rctt['{'] = 1;
		//~ if (fin-ini+1 <= alfabeto.size()) {
			bool aux = true;
			unordered_set < char >::iterator it;
			for (it=alfabeto.begin(); it!=alfabeto.end(); ++it){
				if (lctt[*it] > 1 || rctt[*it] > 1){
					aux = false;
					break;
				}
			}
			//~ for(int i; i<lenAlfabeto; ++i){
				//~ if (lctt[alfabeto[i]] > 1 || rctt[alfabeto[i]] > 1){
					//~ aux = false;
					//~ break;
				//~ }
			//~ }
			if (aux){
				//~ cout << "super: " << proteina.substr(ini,fin-ini) << endl;
				//~ cout << "super: " << proteina.substr(ini,l) << endl;
				//~ pair< int, int > interval = make_pair (ini, fin);
				//~ pair<int, pair< int, int > > auxInterval = make_pair(l, interval);
				//~ super.push_back(auxInterval);
				//~ unsigned int cant = affectedProteins(ini, l, sa, pid);
				//~ string auxRes = proteina.substr(sa[ini],l) + " , " + to_string(cant);		
				string auxRes = proteina.substr(sa[ini],l) + " " + to_string(ini) + " " + to_string(fin) + " " + to_string(l);
				processSuper(auxRes);
			} else {
		//~ }
				if (tmm == true) {
					//~ pair< int, int > interval = make_pair (ini, fin);
					//~ pair<int, pair< int, int > > auxInterval = make_pair(l, interval);
					//~ local.push_back(auxInterval);
					bool isNested = true;
					for(int k=ini; k<=fin; ++k){
						char sigmaLeft = proteina[sa[k]-1];
						char sigmaRight = proteina[sa[k]+l];
						//imprimir en el archivo desde k hasta k+l-1 inclusive
						//~ if (lctt[sigmaLeft] > 1 || rctt[sigmaRight] > 1) {
							//~ cout << "k: " << k << " nested: "<< proteina.substr(k,l) <<endl;
							//~ nested.push_back(k);
							//~ processNested(proteina.substr(sa[k],l));
						//~ } else {
							//~ cout << "k: " << k << " notNested: " << proteina.substr(k,l) <<endl;
							//~ notNested.push_back(k);
							//~ processNonNested(proteina.substr(sa[k],l));
						//~ }
						if (lctt[sigmaLeft] <= 1 && rctt[sigmaRight] <= 1){
							isNested = false;
						}
					}
					if (isNested) {
						//~ unsigned int cant = affectedProteins(ini, l, sa, pid);
						//~ string auxRes = proteina.substr(sa[ini],l) + " , " + to_string(cant);
						string auxRes = proteina.substr(sa[ini],l) + " " + to_string(ini) + " " + to_string(fin) + " " + to_string(l);
						processNested(auxRes);
					} else {
						//~ unsigned int cant = affectedProteins(ini, l, sa, pid);
						//~ string auxRes = proteina.substr(sa[ini],l) + " , " + to_string(cant);
						string auxRes = proteina.substr(sa[ini],l) + " " + to_string(ini) + " " + to_string(fin) + " " + to_string(l);
						processNonNested(auxRes);
					}	
				}
			}
		
		
	}
	
}

void createDicctionaryOfSmrBySetPair(set< pair<string,int> > &smr, vector< set< pair<string,int> > > &dicc){
	set< pair<string,int> >::iterator it;
	cout << "set smr length: " << smr.size() <<endl;
	cout << "set dic length: " << dicc.size() <<endl;
	int contador =0 ;
	for (it=smr.begin(); it!=smr.end(); ++it){
		//~ cout << ' ' << *it << " it-len: " << it->length() << endl;
		int len = it->first.length();
		if(len <= 3)contador++;
		dicc[len].insert(*it);
	}
	cout << "contador: " << contador <<endl;
}

void createDicctionaryOfSmr(set< string > &smr, vector< set< string > > &dicc){
	set<string>::iterator it;
	cout << "set smr length: " << smr.size() <<endl;
	cout << "set dic length: " << dicc.size() <<endl;
	int contador =0 ;
	for (it=smr.begin(); it!=smr.end(); ++it){
		//~ cout << ' ' << *it << " it-len: " << it->length() << endl;
		int len = it->length();
		if(it->length() <= 3)contador++;
		dicc[len].insert(*it);
	}
	cout << "contador: " << contador <<endl;
}

void createTrieOfSmr(set< string > &smr, trie * smrTrie){
	set<string>::iterator it;
	for (it=smr.begin(); it!=smr.end(); ++it){
		//~ cout << "string ne to add to trie: " << *it << endl;
		insertString (smrTrie, *it);
	}
}

void createTrieOfSmrWithFP(set< pair<string,int> > &smr, trie * smrTrie){
	set< pair<string,int> >::iterator it;
	for (it=smr.begin(); it!=smr.end(); ++it){
		//~ cout << "string ne to add to trie: " << *it << endl;
		insertStringWithFP(smrTrie, *it);
	}
	calculateTotalFootPrint(smrTrie);
}


void createTrieOfSmrPref(set< string > &smr, trie * smrTrie){
	set<string>::iterator it;
	for (it=smr.begin(); it!=smr.end(); ++it){
		//~ cout << "string ne to add to trie: " << (*it).substr(0,4) << endl;
		insertString (smrTrie, (*it).substr(0,5));
	}
}


float calculateCoverage(string p, set< string > m){
	vector< bool > cov(p.length(), 0);
	set<string>::iterator it;
	for (it=m.begin(); it!=m.end(); ++it){
		//~ cout << ' ' << *it <<endl;
		size_t pos = p.find(*it, 0);
		while(pos != string::npos) {
			for(int j=0; j<it->length(); ++j){
				cov[pos+j] = 1;
			}
			pos = p.find(*it,pos+1);
		}
	}
	float res = 0;
	for(int i=0; i<cov.size(); ++i){
		res += cov[i];
	}
	cout << "res: " << res << " string-size: " << p.length() << endl;
	cout << "coverage: " << res/p.length() << endl;
	return res/p.length();	
}

float calculateCoverageWriteToFile(string &p, vector< set< string > > &dicc, ofstream &coverageFile){
	vector< bool > cov(p.length(), 0);
	float familiarity = 0.0;
	
	for(int i=10; 0<i; --i){
		set<string>::iterator it;
		
		
		for (it=dicc[i].begin(); it!=dicc[i].end(); ++it){
			//~ cout << ' ' << *it <<endl;
			size_t pos = p.find(*it, 0);
			while(pos != string::npos) {
				for(int j=0; j<it->length(); ++j){
					cov[pos+j] = 1;
				}
				pos = p.find(*it,pos+1);
			}
		}
		
		float res = 0;
		for(int i=0; i<cov.size(); ++i){
			res += cov[i];
		}
		res = res/p.length();
		if(i == p.length()-1){
			familiarity += (1 + res)/2;
		} else {
			familiarity += res;
		}
		
		//~ cout << "writing " << res << " to coverage file"<<endl;
		coverageFile << i << " , " << res << "\n";

		
	}
	coverageFile << 0 << " , " << 1 << "\n";
	
	cout << "familiarity: " << familiarity <<endl;
	return familiarity;
}

float calculateCoverageKMP(string &p, vector< set< string > > &dicc, ofstream &coverageFile){
	vector< int > cov(p.length(), 0);
	float familiarity = 0.0;
	
	for(int i=p.length(); 0<i; --i){
		//~ cout << "current length: " << i << endl;
		
		
		set<string>::iterator it;
		
		
		for (it=dicc[i].begin(); it!=dicc[i].end(); ++it){
			//~ cout << ' ' << *it <<endl;
			KMPSearch(*it, p, cov);
		}
		
		float res = 0;
		int acum = 0;
		for(int i=0; i<cov.size(); ++i){
			//~ cout << "cov[" << i <<"] " << cov[i] << endl;
			acum += cov[i];
			if (acum > 0){
				res += 1;
			}
		}
		//~ cout << "res: " << res << " string-size: " << p.length() << endl;
		//~ cout << "coverage: " << res/p.length() << endl;
		res = res/p.length();
		if(i == p.length()-1){
			familiarity += (1 + res)/2;
		} else {
			familiarity += res;
		}
		
		//~ cout << "writing " << res << " to coverage file"<<endl;
		coverageFile << i << " , " << res << "\n";
		//~ cout << "next"<<endl;
		
	}
	coverageFile << 0 << " , " << 1 << "\n";
	
	cout << "familiarity: " << familiarity <<endl;
	return familiarity;	
}


void calculateCoverageInProteins(){
	string proteinSet = "";
	//~ readWholeProtein(proteinSet);
	proteina = proteinSet;
	vector< string > files = listFile();
	proteinSet 	= concatenateProteins(files);
    //~ cout << proteinSet <<endl;
    //~ proteina = proteinSet;
	//~ vector< int > sai;
	//~ vector< int > lcp;
	//~ cout << "calculando SA y LCP ..." <<endl;
	//~ cout << "longitud alfabeto: " << alfabeto.size() <<endl;
	//~ calculateSAIandLCPForSetWithStruct(proteinSet, sai, lcp);
	//~ cout << "longitud alfabeto: " << alfabeto.size() <<endl;
	//~ vector< pair< int, pair< int, int > > > intervals;
	//~ cout << "buscando intervalos ..." <<endl;
	//~ findIntervalsPaper(sai, lcp, intervals);
	//~ cout << "procesando intervalos ..." <<endl;
	//~ processIntervals(intervals, sai, lcp);
	//~ cout << "calculando diferencia smr ..." <<endl;
	//~ set< string > mySMRSet = processFileToSet("MR_SMR.csv");
	set< string > mySMRSet = processFileToSet("MR_NE.csv");
	//~ set< string > mySMRSet = processFileToSet("MR_NN.csv");
	vector< set< string > > dicc(proteinSet.length());
	cout << "creating dicctionary... " << endl; 
	createDicctionaryOfSmr(mySMRSet, dicc);
	cout << "dicctionary ready... " << endl;
	for(int i=0; i<50; ++i){
		cout << "dicc[" << i << "] : " << dicc[i].size() <<endl; 
	}
	
	//~ cout << "files size : " << files.size() <<endl;
	ofstream familiarityFile;
	ofstream coverageFile;
	familiarityFile.open ("familiarity.csv", ofstream::app);
	coverageFile.open ("coverage.csv", ofstream::app);
	cout << "files ready.. calculating coverage.." <<endl;
	for(int i=0; i<1; ++i){
		//~ cout << "i: " << i << " files size: " << files.size()<<endl;
		ifstream myfile("/home/naciraa/Documents/materias/tesis/kapow/tesiskapow/codigo/NEWAnk/UniRef90_P25963.fasta");
		if (!myfile.good()) {
			cerr << "Error opening: " << files[i] << " . You have failed." << endl;
			//~ return -1;
		} 
		string line, id, protein;
		while (getline(myfile, line)) {
			if(line.empty()){
				continue;
			}
				
			if (line[0] == '>') {
				// output previous line before overwriting id
				// but ONLY if id actually contains something
				if(!id.empty()){
					cout << id << " : " << protein << endl;
				}
				id = line.substr(1);
				protein.clear();
			} else {
				//  if (line[0] != '>'){ // not needed because implicit
				protein += line;
			}
		}
		cout << protein <<endl;
		
		//~ float familiarity = 0.0;
		//~ for(int i=1; i<proteina.size()-1; ++i){
			//~ float aux = calculateCoverageKMP(proteina, dicc, i);
			//~ familiarity += aux;
			//~ cout << "writing " << aux << " to coverage file"<<endl;
			//~ coverageFile << aux << "\n";
		//~ }
		//~ cout << "writing separator ; to coverage file"<<endl;
		//~ coverageFile << ';' << "\n";
		//~ float familiarity = calculateCoverageWriteToFile(protein, dicc, coverageFile);
		float familiarity = calculateCoverageKMP(protein, dicc, coverageFile);
		cout << "writing " << familiarity << " to familiarity file"<<endl;
		familiarityFile << familiarity << "\n";
		cout << "writing separator ; to familiarity file"<<endl;
		familiarityFile << ';' << "\n";
	}
	
	familiarityFile.close();	
	coverageFile.close();	
	cout << "fin" <<endl;	
}

void calculateFootPrint(){
	string proteinSet = "";
	//~ readWholeProtein(proteinSet);
	//~ proteina = proteinSet;
	vector< string > files = listFile();
	proteinSet 	= concatenateProteins(files);
    //~ cout << proteinSet <<endl;
    //~ proteina = proteinSet;
	//~ vector< int > sai;
	//~ vector< int > lcp;
	//~ cout << "calculando SA y LCP ..." <<endl;
	//~ cout << "longitud alfabeto: " << alfabeto.size() <<endl;
	//~ calculateSAIandLCPForSetWithStruct(proteinSet, sai, lcp);
	//~ cout << "longitud alfabeto: " << alfabeto.size() <<endl;
	//~ vector< pair< int, pair< int, int > > > intervals;
	//~ cout << "buscando intervalos ..." <<endl;
	//~ findIntervalsPaper(sai, lcp, intervals);
	//~ cout << "procesando intervalos ..." <<endl;
	//~ processIntervals(intervals, sai, lcp);
	//~ cout << "calculando diferencia smr ..." <<endl;
	//~ set< string > mySMRSet = processFileToSet("MR_SMR.csv");
	set< string > mySMRSet = processFileToSet("MR_NE.csv");
	//~ set< string > mySMRSet = processFileToSet("MR_NN.csv");
	trie * myTrie = new trie();
	vector< set< string > > dicc(1000000);
	cout << "creating dicctionary... " << endl; 
	createDicctionaryOfSmr(mySMRSet, dicc);
	cout << "dicctionary ready... dicc[3]: " << dicc[3].size()<< endl;
	cout << "creating trie... " << endl; 
	cout << "#NEXUS" <<endl;
	cout << "" <<endl;
	cout << "Begin trees;" <<endl;
	cout << "" <<endl;
	
	for (int i = 1; i < 10; i++){
		createTrieOfSmr(dicc[i], myTrie);
	}
	
	cout << "trie ready... " << endl;
	//~ printFootPrint (myTrie, 3);
	printFootPrint (myTrie, 2, 3);
	printTrieAsGraph (myTrie, 2, 3);
	cout << "cant nodos " << countTree(myTrie->ini) <<endl;
	//~ cout << "fin" <<endl;	
	//~ cout << "End;" <<endl;
}
