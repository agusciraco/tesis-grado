#ifndef __UTILS_H_
#define __UTILS_H_

#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <iostream>
#include <string.h>
#include <fstream>
#include <sstream>
#include <dirent.h>
#include <vector>
#include <algorithm>
#include <set>

using namespace std;

vector< string > listFile();
vector< string > listFileFromPath(string dirPath);
vector< string > listDirsFromPath(string dirPath);
string concatenateProteinsOriginal(vector< string > &files);
string concatenateProteins(vector< string > &files);
//~ vector< string > concatenateProteinsPid(vector< string > &files);
pair<string, vector< string >> concatenateProteinsPid(vector< string > &files);
void printVectorContent(vector < int > &v);
void processMaxRepPRago(vector< int > &maxRep, string &s);
void emptyFiles();
void processSuper(string smr);
void processNonNested(string nn);
void processNested(string ne);
void readWholeProtein(string &proteina);
set< string > processFileToSet(string fileName);
set< pair<string,int> > processFileToSetPair(string fileName);
set< pair<string,int> > processFileToSetPair2(string fileName);
void computeLPSArray(string pat, int M, vector<int> &lps);
void KMPSearch(string pat, string &txt, vector< int > &cov);
void writeResultsHeatMap(string filename, string results);
void writeLineInFile(string filename, string line);
//~ unsigned int affectedProteins(int ini,int fin, vector< int > SA, string Pid);
unsigned int affectedProteins(int ini,int fin, vector< int > &SA, vector< string> &Pid);

#endif
